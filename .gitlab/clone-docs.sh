#!/bin/bash

# sparse-checkout remote CaaS docs folder inside ./sylva-core
git clone --branch "main" --depth=1 --no-checkout https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/sylva-projects/sylva-core.git
cd sylva-core
git sparse-checkout set ./docs
git checkout main
# oervide docs folder with remove content
cd ..
cp -r sylva-core/docs/. ./website/docs/
rm ./website/docs/placeholder.md
ls ./website/docs/
