import yaml
import os
import shutil
import requests
import hashlib
import tarfile
import subprocess

def download_content(url, fetchLocation):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            if os.path.dirname(fetchLocation) != '':
                os.makedirs(os.path.dirname(fetchLocation), exist_ok=True)
            with open(fetchLocation, 'wb') as f:
                f.write(response.content)
            print(f"Downloaded content from {url} to {fetchLocation}")
        else:
            print(f"Failed to download content from {url}. Status code: {response.status_code}")
    except Exception as e:
        print(f"Error downloading content from {url} to {fetchLocation}: {e}")

def download_archive(archive_url, fetchLocation):
    archive_name = f"{hashlib.md5(archive_url.encode('utf-8')).hexdigest()[:8]}.tar.gz"
    download_content(archive_url, archive_name)
    archive_file = tarfile.open(archive_name)
    archive_file.extractall()
    archive_root = archive_file.getmembers()[0].name
    archive_path = archive_url.split('=')[1]
    if os.path.dirname(fetchLocation) != '':
        os.makedirs(os.path.dirname(fetchLocation), exist_ok=True)
    try:
        shutil.move(os.path.join(archive_root, archive_path), fetchLocation)
    except Exception as e:
        print(f"Error: {e}")

def main():
    yaml_file = "remote-markdowns.yml"  # Change this to your YAML file path
    if not os.path.isfile(yaml_file):
        print(f"Error: {yaml_file} does not exist.")
        return
    
    with open(yaml_file, 'r') as f:
        try:
            data = yaml.safe_load(f)
            for item in data:
                if ('url' not in item or 'archive' not in item) and ('fetchLocation' not in item):
                    print("Invalid item format in YAML: ", item)
                # download folder as an archive
                elif 'archive' in item:
                    download_archive(item['archive'], item['fetchLocation'])
                # download markdown
                else:
                    download_content(item['url'], item['fetchLocation'])
                if 'patchName' in item and 'patchLocation' in item:
                    try:
                        os.makedirs(os.path.dirname(item['patchLocation']), exist_ok=True)
                        shutil.copy(item['patchName'], item['patchLocation'])
                    except Exception as e:
                        print(f"An error occurred: {e}")
        except yaml.YAMLError as e:
            print("Error parsing YAML:", e)

if __name__ == "__main__":
    main()
