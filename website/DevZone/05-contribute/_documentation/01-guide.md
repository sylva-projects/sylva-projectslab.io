---
sidebar_position: 1
---

# How to write Sylva documentation

## General workflow to create a new doc

1. Find where to create a new file, depending on the type of the documentations:
    - Release-Specific docs --> `docs`
    - Onboarding guide, tutorials, etc. --> `GetStarted`
    - Docs for developers (Onboarding, Tips, Dev/Test env.etc) --> `DevZone`
    - cf. README.md for the details repo structure
2. Create a draft Merge Request to target "main" branch, following the recommendations on:
    - File [naming conventions](#file-naming-convention) (**hyphen-separated** and **all-lowercase** markdown files)
    - Referenced [image locations](#adding-images) (`./img` `./img_src` for images included in the current document and their sources as ppt, drawio, etc.)
    - Adopt the [recommended styles](#style-references)
    - cf. details in respective sections
3. Preview the Merge Request using local or online preview
    - You can test and use [supported markdown syntax](/dev-zone/contribute/documentation/markdown-syntax) in your MR
    - Solve any spell check issue
    - Solve any bad links issue reported by the Gitlab pipeline
4. When the MR is ready, assign it to the docs reviewer to merge and publish the contribution.

Following sections describe the details of each specific topic on the documentation.

## Repo structure

Please refer to the Gitlab repo `README.md`.

## Recommendations on new files

### File naming convention

It is recommended to apply a consistent file naming convention since the filename is used to generate URL by default.
Therefore, we should avoid "spaces" in the filename, and decide on one **naming convention** to enforce.

Many online references
([link1](https://arcticicestudio.github.io/styleguide-markdown/rules/naming-conventions.html#file-naming)
or [link2](https://stackoverflow.com/questions/5362954/why-use-instead-off-in-url))
recommend **hyphen-separated** ("the-quick-brown-fox-jumps-over-the-lazy-dog") naming
(also called [spinal-case or kebab-case](https://en.wikipedia.org/wiki/Letter_case#Special_case_styles))
for better readability.

In summary:

- If you are modifying an existing file, the filename should remain the same (even if the naming convention is not applied).
- If you are creating a new folder or a new file, please respect the **hyphen-separated** and **all-lowercase** convention.

```python
./New_folder    --> Not OK
./New-folder    --> Not OK
./new-folder    --> OK

new markdown file.md  --> Not OK
new_markdown_file.md  --> Not OK
new-markdown-file.md  --> OK
```

Capitalization for proper nouns in the Markdown filename are OK, e.g. `G4R2`, `IDM`...

Note that the Markdown filename will define the **default** URL for the given document:
`new-markdown-file.md` will be rendered at the URL `http://<doc-portal-domain-name>/<path>/new-markdown-file`.

### Adding images

As a convention, we put image files in the `img` folder at the **same** location as the markdown document referencing it. 
Some site-wide generic images such as logo are placed in `/static/img/` folder. We normally do not need to add images there.

Then in the document, to add an image:

```
![Flavors](img/Flavors.png)             --> relative link to "./img/Flavors.png"
![logo](/static/img/logo.png)           --> link to "/static/img/logo.png"
```

#### How to make the image "clickable"?

By "nesting" image link in a document link: 
(`![Flavors](img/Flavors.png)` displays the image with an `alt` message, 
the additional `[   ](img/Flavors.png)` makes it as a clickable `<a>` link.

```
[![Flavors](img/Flavors.png)](img/Flavors.png)
```

## Markdown syntax and style

Here are some essential recommendations.
Detailed syntax guideline is [Markdown Syntax doc](/dev-zone/contribute/documentation/markdown-syntax).

### Document [Headers/Frontmatter](markdown-syntax#document-headers)

Always start a markdown file with `title` header, then use the heading from level 2 (`##`).

```markdown
---
title: New Document
---

## Heading 1

### Heading 2

... 
```

Other [header](markdown-syntax#document-headers) could be used.
For example, `sidebar_label` if the `title` is too long to fit in the sidebars.

### Document Links

Generally, to add links from a doc page to another, we have two cases depending the their locations.

In the first case, for most of links, the target page is in the same **section** as the source page, e.g. linking from a G5R0 operation page to G5R0 installation guide page.
(A section is defined as a root level subfolder in Git repo).
For these links, we use **relative file path** such as `relative_file_path_to/filename.md` instead of ~~relative URL path~~,
because relatives URL path can be easily broken, but the file path are processed during the build.

```markdown
[Link Test](../Relative-URL-Path)                         --> NOT OK
[Link Test](../other-folder/markdown.md#possible-anchor)  --> OK
```

In the second case, we have **cross-section** links, e.g. linking from a release doc to the general design section.
For these links, we should **NOT** use relative file path as above, because doc files location will change when a new release is available, so their relative path to the target doc will also change.
Therefore, for **cross-section** links, **Absolute URL path** should be used:  

```
[Detailed Roadmap](../../Lifecycle.md)            --> NOT OK (relative file path)
[Detailed Roadmap](../../Lifecycle)               --> NOT OK (relative URL path)
[Detailed Roadmap](/Lifecycle)                    --> OK (absolute URL path)
```

In summary, we use **relative file path** for in-section links and **Absolute URL path** for cross-section links.

:::note
There is a known issue as Docusaurus is unable to 
[detect any broken link with anchor](https://github.com/facebook/docusaurus/issues/3321)
as `[text](http//example.com#anchor)` or `[text](/path/markdown.md#anchor)`.  
We should double-check if anchors need to be added.
:::

### Code Blocks

Choose the right **Language** for the code block syntax highlighting, and use [title](https://v2.docusaurus.io/docs/markdown-features/code-blocks#code-title) and [line highlighting](https://v2.docusaurus.io/docs/markdown-features/code-blocks#line-highlighting) when possible.


    ```shell-session {2} title="test"
    root@m1-foremansible:oiaas-deployment-ansible# kubectl cluster-info
    Kubernetes master is running at https://127.0.0.1:6443
    ...
    ```

The most used languages in our context are `shell`, `shell-session`, `yaml`, `python`.
More details on code blocks can be found [here](/dev-zone/contribute/documentation/markdown-syntax#code-blocks).

### Callouts/Admonitions

When using [Callouts/admonitions](https://docusaurus.io/docs/markdown-features/admonitions)
(colored text block),
please check [their styles]((https://docusaurus.io/docs/markdown-features/admonitions),
and choose the label/color that suite you.

The callout itself should remain brief,  
add line breaks using `double space` if needed (just like in a normal markdown document)

```markdown
:::note Title
Label can be "note" "tip" "info" "caution" "danger"<space><space>
New line if necessary.
:::
```

### Style references

Some general rules are:

- Capitalize only the first word of the headings (except the title, which should be "Title Case");
- Adopt the same spelling for proper nouns (build and follow a Glossary).

Moreover, here are some good references on the document general styling.
Please try to adopt these best practices during your editing.

- [Markdown style guide](https://github.com/google/styleguide/blob/gh-pages/docguide/style.md)
- [Another style guide](https://arcticicestudio.github.io/styleguide-markdown/rules/)

## Preview

### Local preview

A local test website can be built and run locally. Commands are in the [README.md](/dev-zone/contribute/documentation/run-locally).

### Online preview throught preview URL

For each Merge Request, you can mannually trigger the "online preview" step, which will build the given branch to be hosted on the preview URL https://sylva-projects.gitlab.io/doc-preview/. 

Note that there is only one preview URL, and it is shared by all branches. Therefore, to be sure that the previewed content is your MR, you can check the branch name on the top announcement bar. If your branch preview is overridden by other branches, you can contact current preview's owner, and coordinate to rerun the pipeline on the aggreed branch. 
