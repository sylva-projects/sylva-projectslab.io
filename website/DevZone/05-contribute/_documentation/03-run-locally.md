---
sidebar_position: 3
---

# Run locally

## Using Yarn

### Pre-requisites

- Install [node](https://nodejs.org/en/learn/getting-started/how-to-install-nodejs)
- Install [nvm](https://github.com/nvm-sh/nvm?tab=readme-ov-file#installing-and-updating)
- Install [yarn](https://yarnpkg.com/getting-started/install)

The documentation portal uses docusaurus framework, the version used needs node version > v18.
You can use the stable v18 version

```shell
nvm install v18.18.2
nvm use v18.18.2
```

### Clone the documentation repository

```shell
git clone https://gitlab.com/sylva-projects/sylva-projects.gitlab.io.git
cd sylva-projects.gitlab.io/website
```

### Install dependencies and packages

```shell
yarn
```

### Run the documentation portal locally

```shell
yarn run start
```

By default, the webpage will be open on port [3000](http://localhost:3000).

## Using Docker (TO UPDATE)

### Build and start the local container

to build and test the documentation website locally

    docker compose up

This will build a docker image called `sylva-docusaurus`, install the dependencies and start a container named `sylva-local-preview` to render the local docs.

:warning: the "sylva-docusaurus" docker image has a large size (>2GB), because all the packages installed.

Some proxy configuration for `yarn` may be needed, to download/install packages from Internet repos.  

If you are behind a web proxy, the proxy address and port should be set in `.yarnrc` file. (an example file is provided as `.yarnrc-example`)

### Preview and edit the docs

The local server is accessible at http://localhost:3000.
It provides the **hot** preview for all markdown files in the `website` folder, i.e.
The changes in the markdown file are rendered immediately by the server without restart.

Note: To add new document categories in addition to `docs`, `GetStarted`, etc. 
You need also to modify `docusaurus.config.js`

### Clean up

If you want stop the container, you can run

    docker compose rm

or, since the container has a static name

    docker rm sylva-local-preview

If you have modified anything in `website/package.json`, and you want to rebuild the image

    docker compose up --build

If you want to remove everything and start over

    docker rm sylva-local-preview
    docker rmi sylva-docusaurus