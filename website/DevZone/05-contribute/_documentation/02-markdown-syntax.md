---
sidebar_position: 2
title: Markdown Syntax Examples
---

Docusaurus supports [standard markdown syntax](https://daringfireball.net/projects/markdown/syntax)
with some [extended syntax](https://www.markdownguide.org/extended-syntax/) such as code block and footnote.
Some "non-standard" [new features](https://v2.docusaurus.io/docs/markdown-features)
are also available to build simple and interactive documentations.

## Standard Markdown Syntax

There are many tutorials explaining the markdown syntax. Here are some of them:

- [Cheat sheet page](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- [Markdown syntax reference](https://waher.se/Markdown.md)

## Docusaurus new features

### Document headers

In Docusaurus, each markdown has the following
[header fields](https://docusaurus.io/docs/api/plugins/@docusaurus/plugin-content-docs#markdown-frontmatter):

- **sidebar_label** (default to **title**)
- **title** (default to **id**)
- **slug** (default to **id**)
- **id** (default to filename)
- (markdown page URL http://__(BaseURL)__/__(id)__)

Without headers set, everything will default to the filename. Therefore, we should:

- set `title` to have __Multi word labels__
- remove any space in the filename to avoid __THIS%20URL__

```diff
./Help/README.md
- # Release Note
+ ---
+ title: Release note
+ slug: /
+ ---
```

`slug` Header helps to reduce URL from http://__BaseURL__/Help/README to http://__BaseURL__/Help/

### Code blocks

#### Syntax highlighting

When the language is specified after the ` ``` ` will enable the syntax highlighting for this language.

    ```shell-session
    foo@bar:/$ cd ~
    foo@bar:~$ sudo -i
    [sudo] password for foo:
    root@bar:~# echo "hello!"
    hello!
    ```

will show

```shell-session
foo@bar:/$ cd ~
foo@bar:~$ sudo -i
[sudo] password for foo:
root@bar:~# echo "hello!"
hello!
```

Below is the list of supported languages. `shell/bash` and `shell-session` are often used in our context.

```
bash --> alias "shell"
shell-session
json
python
yaml
markdown
git

diff
markup
clike
c
cpp
css
"css-extras"
javascript
jsx
"js-extras"
coffeescript
go
graphql
handlebars
less
makefile
objectivec
ocaml
reason
sass
scss
sql
stylus
tsx
typescript
wasm
```

#### Block Title and Line Highlighting

Syntax Example:

    ```shell-session {4} title="test title"
    $ git checkout master
    Switched to branch 'master'
    Your branch is up-to-date with 'origin/master'.
    $ git push
    Everything up-to-date
    $ echo "Foo
    > Bar"
    Foo
    Bar
    ```

Results:

```shell-session {4} title="test title"
$ git checkout master
Switched to branch 'master'
Your branch is up-to-date with 'origin/master'.
$ git push
Everything up-to-date
$ echo "Foo
> Bar"
Foo
Bar
```

### Callouts/Admonitions

New callouts use `:::`

```diff
- !!! [info|failure|warning] Title
-     Description
+ :::[note|tip|info|caution|danger] Title
+ Description
+ :::
```

For more information, please refer to: 

- https://docusaurus.io/docs/markdown-features/admonitions
- https://github.com/elviswolcott/remark-admonitions#usage

To add a line-break inside the callout, just use the same ["double-spaces" syntax](https://daringfireball.net/projects/markdown/syntax#p) as in Markdown (two spaces at the end of the lines).
`<br />` tag can work as well as it is used in some table cells.


#### Other common pitfalls

Below are some errors observed during the migration:

React parses all files for HTML tags, and requires tag to be closed.
Therefore, `<` and `>` should be quoted or closed.

```diff title="Use < and > carefully"
- ... the remote IdM server(`ipaddr`:<IP>)
+ ... the remote IdM server(`ipaddr`:{"<"}IP{">"})
+ ... the remote IdM server(`ipaddr:<IP>`})
- <br>
+ <br />
```

```diff title="inline CSS format"
- <pre style="background: black">zzz</pre>
+ <pre style={{background: 'black'}}>zzz</pre>
```

```diff title="Markdown link url is mandatory"
- [text]()
+ [text]
+ or [text](link)
```

Please avoid using the `<u>` unarticulated annotation element especially in titles as it could be confused with a hyperlink!
