---
title: Contribution Guide
---

Sylva welcomes new technical contributions. Follow these steps to add new features to the Sylva stack.

## Step 1: Signing the CLA (Contributor License Agreement)

### For Companies

Before contributing, companies must sign the Corporate Contributor Agreement:

1. Access the [Sylva Project Corporate CLA signature page](https://organization.lfx.linuxfoundation.org/foundation/a092M00001KWzi6QAD/project/a092M00001L16ZLQAZ/cla).
2. Alternatively, visit the [main page](https://organization.lfx.linuxfoundation.org/), select **Linux Foundation Europe**, and then select **Sylva project**.

The initial CLA Manager of the organization **requires an LFID account and must belong to the contributing organization**. This can be managed in their Individual Dashboard profile [here](https://openprofile.dev/).

If issues arise, open a ticket with the Support Team [here](https://jira.linuxfoundation.org/plugins/servlet/desk/portal/4/create/143). For more details on the Corporate CLA process, refer to [this page](https://docs.linuxfoundation.org/lfx/easycla/v2-current/corporate-cla-managers/coordinate-signing-cla-and-become-initial-cla-manager).

:::note
Each developer must be registered on EasyCLA by their company's CLA manager before submitting a merge request to any Sylva project.
:::

### For Individuals

If you are not part of a company, or if your company has not signed the Corporate Contributor License Agreement, you can sign the Individual Contributor License Agreement by opening a ticket [here](https://jira.linuxfoundation.org/plugins/servlet/desk/portal/4/create/143).

## Step 2: Join Us on GitLab

### Create a GitLab Account

Sylva is hosted on [GitLab](https://gitlab.com/sylva-projects). Create a GitLab account to submit merge requests.

### Create Issues

If you find any issues in Sylva, ping the community on Slack (sylva-projects.slack.com) or directly open an issue. If unsure where to report the issue, use [sylva-core](https://gitlab.com/sylva-projects/sylva-core/-/issues) by default.

### Create Merge Requests from Your GitLab Account

Once signed in to GitLab, fork any Sylva project, work on your feature in a dedicated branch, and propose a merge request from your development branch.

### Propose Enhancements or Features

As an open-source community, we need to agree on significant enhancements or features. Discuss and review new features with the Sylva community to understand their technical impact.

Submit requests for enhancements and features using a merge request based on this [template document](/dev-zone/contribute/rfe/). The community will discuss the proposal, implementation, and testing during the review.

## Step 3: Joining the Community

Engage with the Sylva community through:

- **Slack**: Join us at sylva-projects.slack.com. Use the [invitation link](https://join.slack.com/t/sylva-projects/shared_invite/zt-1owzbom3p-MiSitCf~zJa8bFNoRjHKGQ) or [this link](https://sylva-projects.slack.com/ssb/redirect#/shared-invite/email).
- **GitLab Issues**: Create an issue in any [`sylva-projects`](https://gitlab.com/sylva-projects) repository to start a discussion.