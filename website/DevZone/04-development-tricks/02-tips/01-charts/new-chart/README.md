---
title: Add a New Helm Chart
sidebar_position: 1
---

:::info
A standard [template](./template) can be found here for the helm-chart related documentation
:::

## Steps to create a new Helm chart repository and add a new Helm chart to it.

### **Step 1: Create a new project and clone the repository**

Go to [sylva/elements/helm-charts](https://gitlab.com/sylva-projects/sylva-elements/helm-charts) and create a new project under this and later clone it. 

  ```sh
   git clone https://gitlab.com/your-username/my-project.git
   cd my-project
  ```

### **Step 2: Use Helm to Create a Chart**
     
1. Inside your **my-project** directory, create a new Helm chart:

  ```sh
   helm create my-new-chart
  ```
2. This will generate a directory structure for your new Helm chart:

```sh
my-new-chart/
├── Chart.yaml
├── charts
├── templates
└── values.yaml
```

### **Step 3: Customize Your Helm Chart**
1.  Edit **Chart.yaml** to provide metadata about your Helm chart:

```yaml
apiVersion: v2
name: my-new-chart
type: application
description: A Helm chart for sylva
version: 0.1.0
appVersion: "1.0"
```

* ***apiVersion:*** This denotes the chart API version. v2 is for Helm 3 and v1 is for previous versions.
* ***name:*** Denotes the name of the chart.
* ***description:*** Denotes the description of the helm chart.
* ***Type:*** The chart type can be either ‘application’ or ‘library’. Application charts are what you deploy on Kubernetes. Library charts are re-usable charts that can be used with other charts. A similar concept of libraries in programming.
* ***Version:*** This denotes the chart version. 
* ***appVersion:*** This denotes the version number of our application.

2. Add Kubernetes manifests in the **templates** directory as needed. These manifests can include various Kubernetes resources such as Pods, Deployments, Services, ConfigMaps, and more.

3. Customize **values.yaml** with default values for your Helm chart. The values.yaml file in a Helm chart provides a central place to define default configuration values for your templates. These values can be customized to fit the specific needs of your application and can be overridden by users during the installation or upgrade of the Helm chart.

    **Structure of values.yaml:**
    
    Here's an example of a values.yaml file with some commonly used default values:
    
    ```yaml
       replicaCount: 2

       image:
         repository: nginx
         tag: "1.16.0"
         pullPolicy: IfNotPresent

       service:
         name: nginx-service
         type: ClusterIP
         port: 80
         targetPort: 9000

       env:
         name: dev
    ```
4. **Add dependency charts**: This directory is used to store dependency charts under **Charts** directory. These are other Helm charts that your chart depends on. By placing dependency charts in this directory, Helm can package them together with your chart, ensuring that all dependencies are available when the chart is installed.

### **Step 4: GitLab CI/CD Configuration**

Setting up **.gitlab-ci.yml** file to integrate various tools including Gitleaks, Release Notes, Helm Tooling, Renovate, and Renovate Validator. Each tool is included as a template in the CI configuration. 

We can customize the configuration as needed by modifying the rules, adding more templates, or adjusting the versions of the included templates.

Here is the basic setup of **.gitlab-ci.yml** configuration:


```yaml
include:
  - project: "to-be-continuous/gitleaks"
    ref: "vx.x.x"
    file: "templates/gitlab-ci-gitleaks.yml"
  
  - project: 'renovate-bot/renovate-runner'
    ref: "vx.x.x"
    file: '/templates/renovate-config-validator.gitlab-ci.yml'
    rules:
      - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
        changes:
          paths:
            - .gitlab-ci.yml
            - renovate.json
  
  - project: 'sylva-projects/sylva-elements/renovate'
    ref: "vx.x.x"
    file: '/templates/renovate-dry-run.gitlab-ci.yml'
    rules:
      - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
        changes:
          paths:
            - renovate.json
  
  - project: 'sylva-projects/sylva-elements/ci-tooling/ci-templates'
    ref: "vx.x.x"
    file:
      - 'templates/release-notes.yml'
      - 'templates/helm-tooling.yml'
```

**Breakdown of Configuration**

* **Gitleaks**

```yaml
  - project: "to-be-continuous/gitleaks"
    ref: "vx.x.x"
    file: "templates/gitlab-ci-gitleaks.yml"
```

This includes the Gitleaks template from the to-be-continuous/gitleaks project at version. Gitleaks will scan your repository for hardcoded secrets.

* **Renovate Config Validator**

```yaml
  - project: 'renovate-bot/renovate-runner'
    ref: vx.x.x
    file: '/templates/renovate-config-validator.gitlab-ci.yml'
    rules:
      - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
        changes:
          paths:
            - .gitlab-ci.yml
            - renovate.json
```

This includes the Renovate Config Validator template from the renovate-bot/renovate-runner project at version x.x.x. The validator runs only if the pipeline is triggered by a merge request event and changes are detected in .gitlab-ci.yml or renovate.json.

*  **Renovate Dry Run**

```yaml
  - project: 'sylva-projects/sylva-elements/renovate'
    ref: vx.x.x
    file: '/templates/renovate-dry-run.gitlab-ci.yml'
    rules:
      - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
        changes:
          paths:
            - renovate.json
```

This includes the Renovate Dry Run template from the sylva-projects/sylva-elements/renovate project at version x.x.x. This job also runs only if the pipeline is triggered by a merge request event and changes are detected in the renovate.json file.

* **Release Notes and Helm Tooling**

```yaml
  - project: 'sylva-projects/sylva-elements/ci-tooling/ci-templates'
    ref: vx.x.x
    file:
      - 'templates/release-notes.yml'
      - 'templates/helm-tooling.yml'
```

This includes two templates from the sylva-projects/sylva-elements/ci-tooling/ci-templates project at version x.x.x:

1. release-notes.yml for generating release notes.
2. helm-tooling.yml for Helm chart management.

### **Step 5:  Tagging the Helm Chart in GitLab**

Once the helm chart is created, to mark a specific version or release of your Helm chart, create a Git tag in your GitLab repository either from the GitLab UI or using the Git command line.

**From GitLab UI:**

1. Go to your project.
2. Navigate to Repository > Tags.
3. Click on "New tag" and fill in the details.

**Using Git Command Line:**

```shell
git tag <version>
git push origin <version>
```

### **Step 6:  Consume helmchart from sylva-core**

* After the tag is created,  under sylva-core repository in a template **charts/sylva-units/values.yaml**, define name of your GitRepository  "my-project" that points to the tag (version) created in previous step.

```yaml
my-project:
    kind: GitRepository
    spec:
      url: https://gitlab.com/your-username/my-project.git
      ref:
        tag: <version>
```

### **Step 7:** Add a **README.md** 

* Each helmchart project should have a README.md mentioning all instructions on installing, configuring, and accessing the deployed application. It outlines configuration options and usage examples, including troubleshooting guidance. A [template](./template) can be found here.
