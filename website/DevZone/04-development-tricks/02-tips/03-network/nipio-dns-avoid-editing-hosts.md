---
title: nip.io DNS to avoid editing hosts
---

## Using nip.io dns records in order to avoid editing hosts file

The following trick can be used to generate a dynamic cluster domain, avoiding to edit the system hosts file manually:

```yaml
cluster_external_domain: '{{ .Values.display_external_ip }}.nip.io'
```

As result you got something like:

```txt
🌱 You can access following UIs
NAMESPACE       NAME                      CLASS    HOSTS                            ADDRESS                                                          PORTS     AGE
cattle-system   rancher                   nginx    rancher.172.49.112.148.nip.io    192.168.128.120,192.168.128.121,192.168.128.49,192.168.129.244   80, 443   15m
flux-system     flux-webui-weave-gitops   nginx    flux.172.49.112.148.nip.io       192.168.128.120,192.168.128.121,192.168.128.49,192.168.129.244   80, 443   11m
keycloak        keycloak-ingress          <none>   keycloak.172.49.112.148.nip.io   192.168.128.120,192.168.128.121,192.168.128.49,192.168.129.244   80, 443   33m
vault           vault                     <none>   vault.172.49.112.148.nip.io      192.168.128.120,192.168.128.121,192.168.128.49,192.168.129.244   80, 443   33m

🎉 All done
```

and you should be able to directly access UIS as `https://rancher.172.49.112.148.nip.io` in your browser

Notes:

- If your are behind a proxy, you need to be able to resolve Internet hostnames.
- Depending of your proxy config, you also may need to add `.nip.io` in `proxy.no_proxy`
