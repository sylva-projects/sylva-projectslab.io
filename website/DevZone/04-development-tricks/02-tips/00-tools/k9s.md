---
title: K9s
---

# Sylva Project: Tips for Using k9s

## Introduction
This document provides specific tips for using k9s to manage and monitor the Sylva project. k9s is a powerful terminal-based UI to interact with your Kubernetes clusters.

## Prerequisites
- Ensure you have k9s installed. You can follow the installation guide [here](https://k9scli.io/topics/install/).
- Access to the Kubernetes cluster where the Sylva project is deployed.

## Tips for Using k9s with Sylva

### 1. Launch k9s
To start k9s, simply open your terminal and type:
```sh
k9s
```
This will launch the k9s interface.

### 2. Navigate to the Sylva Namespace
If the Sylva project is deployed in a specific namespace, switch to that namespace using the `:ns` command:
```sh
:ns sylva-namespace
```
Replace `sylva-namespace` with the actual namespace name.

### 3. Viewing Pods
To view all pods in the Sylva namespace, use the `:po` command:
```sh
:po
```
This will list all the pods, their statuses, and other relevant information.

### 4. Inspecting Pod Logs
To inspect logs for a specific pod, navigate to the pod using the arrow keys and press `l`. This will open the logs for the selected pod. You can filter logs by typing `/` followed by the search term.

### 5. Executing Commands in a Pod
To execute a command inside a pod, select the pod and press `s`. This will open a shell session inside the pod.

### 6. Viewing Deployments
To view deployments, use the `:deploy` command:
```sh
:deploy
```
This will list all deployments in the Sylva namespace.

### 7. Monitoring Resource Usage
To monitor resource usage, use the `:top` command:
```sh
:top
```
This will display resource usage metrics for pods, nodes, and containers.

### 8. Describing Resources
To get detailed information about a resource, select the resource and press `d`. This will show the YAML description of the resource.

### 9. Port Forwarding
To set up port forwarding, select the pod and press `Shift+f`. Follow the prompts to set up the port forwarding.

### 10. Exiting k9s
To exit k9s, press `Ctrl+c`.

## Conclusion
Using k9s can significantly enhance your productivity when managing the Sylva project on Kubernetes. These tips should help you get started and make the most out of k9s.

For more detailed information, refer to the [k9s documentation](https://k9scli.io/).
