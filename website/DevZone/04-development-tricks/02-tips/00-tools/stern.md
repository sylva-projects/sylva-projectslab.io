---
title: Stern
---

# Using Stern for Multi-Pod Logging

## Introduction

Stern is a powerful tool for tailing logs from multiple Kubernetes pods and containers. It allows you to aggregate logs from multiple pods in real-time, making it easier to monitor and debug your applications.

## Installation

To install Stern, you can use the following command:

### macOS

```sh
brew install stern
```

### Linux

Download the latest release from the [Stern GitHub releases page](https://github.com/stern/stern/releases) and install it:

```sh
wget https://github.com/stern/stern/releases/download/v1.11.0/stern_linux_amd64
chmod +x stern_linux_amd64
sudo mv stern_linux_amd64 /usr/local/bin/stern
```

### Windows

Download the latest release from the [Stern GitHub releases page](https://github.com/stern/stern/releases) and add it to your PATH.

## Usage

### Basic Usage

To tail logs from all pods in a specific namespace, use the following command:

```sh
stern -n <namespace> .
```

Replace `<namespace>` with the name of your namespace.

### Filtering by Pod Name

You can filter logs by pod name using a regular expression. For example, to tail logs from all pods with names starting with "app":

```sh
stern -n <namespace> ^app
```

### Filtering by Container Name

If you want to filter logs by container name, use the `-c` option:

```sh
stern -n <namespace> -c <container-name> .
```

Replace `<container-name>` with the name of your container.

### Combining Filters

You can combine pod name and container name filters:

```sh
stern -n <namespace> ^app -c <container-name>
```

### Customizing Output

Stern allows you to customize the output format. For example, to include timestamps in the log output:

```sh
stern -n <namespace> . --timestamps
```

### Tail Specific Number of Lines

To tail a specific number of lines from the logs:

```sh
stern -n <namespace> . --tail <number-of-lines>
```

Replace `<number-of-lines>` with the number of lines you want to tail.

## Example

Here is an example of using Stern to tail logs from all pods in the "default" namespace, filtering by pod names starting with "web" and container name "nginx":

```sh
stern -n default ^web -c nginx
```

## Documentation

For more detailed information and advanced usage, refer to the [Stern GitHub repository](https://github.com/stern/stern).
