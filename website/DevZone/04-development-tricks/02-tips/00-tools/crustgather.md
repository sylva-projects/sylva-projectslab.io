---
title: CrustGather
---

# Sylva Project: Tips for Using crustgather

## Introduction

This document provides specific tips for using `crust-gather` to manage and monitor the Sylva project. `crust-gather` is a `kubectl` plugin that provides functionality, which allows to collect full or partial cluster state and serve it via an api server.

## Prerequisites

Ensure you have `crust-gather` installed. You can follow the installation guide [here](https://github.com/crust-gather/crust-gather?tab=readme-ov-file#prerequisites).

## Tips to collect Sylva cluster state

### Using `debug-on-exit.sh`

When running `debug-on-exit.sh`, `crust-gather` is automatically executed to collect data from all related clusters.
Data will be located, in `crust-gather` folder, one folder per cluster.

`crust-gather` logs will be located in same folder.

### Using `crust-gather` directly

You can also collect `crust-gather` data using `crust-gather` cli (or `kubectl` plugin)

```sh
crustgather collect --exclude-group="catalog.cattle.io" --exclude-kind=Secret -f crust-gather/my-cluster"
```

`crust-gather` is using standard way to access kubernetes clusters. You may need to set your context or export `KUBECONFIG` variable accordingly.

## Tips to serve previsouly collected cluster state

### 1. Serve

`crust-gather` will serve all collected data in one go. You have to provide the folder containing previously collected data.

```sh
crustgather serve -a crust-gather
```

### 2. Consume

You can now consume in another terminal data served by `crust-gather` as standard kubernetes API using `kubectl` or [`k9s`](./k9s.md)

## Tips to serve cluster state collected from CI job

Deployment Jobs run automatically `debug-on-exit.sh` and store `crust-gather` collected data as an artifact.

A shell script `serve-crustgather-artifact.sh` simplify downloading and serving `crust-gather` data.

From `sylva-core` folder, you can run :

```sh
./tools/serve-crustgather-artifact.sh -j <CI_JOB_ID> [-b]
```

`-b` is optional. It runs `crust-gather` in background, allowing to use same current terminal to explore clusters state.
