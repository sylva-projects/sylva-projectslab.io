---
title: Troubleshooting Guide
---

As the stack is highly relying on flux, it is the main entry point to start with when something goes wrong. As all units are managed by kustomizations, this is the first thing to look at:

```shell
kubectl get kustomizations
```

You can also use [Flux CLI](https://fluxcd.io/flux/installation/#install-the-flux-cli) to watch these resources. This tool will be downloaded automatically alongside with other utilities during bootstrap.

In order to make them accessible in current shell, it is recommended to use provided env file:

```shell
# this puts sylva-core/bin in the $PATH, sets up completion for all tools and sets up FLUX_SYSTEM_NAMESPACE:
# (run this from sylva-core directory)
source bin/env

# alternative: export FLUX_SYSTEM_NAMESPACE=sylva-system
```

After setting this environment variable, you'll be able to issue flux commands without having to provide the namespace each time.
With flux cli, the equivalent of previous command would be:

```shell
flux get kustomizations
```

If you don't have any kustomization in your cluster, it means that the sylva-units chart has not been properly instantiated. In that case you should have a look at the resources that are managing that chart:

```shell
kubectl get gitrepositories.source.toolkit.fluxcd.io sylva-units
kubectl get helmcharts.source.toolkit.fluxcd.io default-sylva-units
kubectl get helmreleases.helm.toolkit.fluxcd.io sylva-units
```

If your management cluster is not properly deploying you should have a look at cluster-api resources:

```shell
kubectl get cluster.cluster
kubectl get machine
kubectl get openstackmachine
```

If you don't have enough info in the status of these resources, you can also have a look at the logs of capi infrastructure & bootstrap providers:

```shell
kubetail -n capo-system -s 12h
```

You can also install and use the [clusterctl tool](https://github.com/kubernetes-sigs/cluster-api/releases), it can be used to have a summary of the cluster deployment status:

```shell
clusterctl describe cluster management-cluster --show-conditions all
```

If you need to check the values set to the sylva-units HelmRelease(s) deployed, possibly for debug purposes, you could get them with:

```shell
kubectl get secret sylva-units-values -o template='{{ .data.values }}' | base64 -d
```

For example, if you want to get the final values passed to a specific unit, such as ones set to the cluster unit (instantiation of sylva-capi-cluster Helm chart),you could use:

```shell
kubectl get secret sylva-units-values -o template='{{ .data.values }}' | base64 -d | yq .units.cluster.helmrelease_spec.values
```
