---
title: OS Images
---

# Using OS Images Built in Sylva

## Available Images

In Sylva, we build OS images using [diskimage-builder](https://gitlab.com/sylva-projects/sylva-elements/diskimage-builder) based on Ubuntu and openSUSE. These images include the necessary packages to deploy Kubeadm or RKE2 clusters in air-gapped environments and are exposed as [OCI artifacts](https://gitlab.com/sylva-projects/sylva-elements/diskimage-builder/container_registry). The images are available as *plain* cloud images or *hardened* according to CIS standards. The **sylva-units** value `sylva_diskimagebuilder_images` contains all available options, with the Ubuntu plain flavor and latest supported Kubernetes version set as default. The name of the image is composed of "OS distro"-"OS version"-"plain/hardened"-"k8s distro"-"k8s version".

```yaml
sylva_diskimagebuilder_images:
  ubuntu-jammy-plain-rke2-1-27-6:
    default_enabled: true
  ubuntu-jammy-plain-rke2-1-26-9: {}
  ubuntu-jammy-plain-rke2-1-25-14: {}
  ubuntu-jammy-hardened-rke2-1-27-6: {}
  ubuntu-jammy-hardened-rke2-1-26-9: {}
  ubuntu-jammy-hardened-rke2-1-25-14: {}
  opensuse-15-5-plain-rke2-1-27-6: {}
  opensuse-15-5-plain-rke2-1-26-9: {}
  opensuse-15-5-plain-rke2-1-25-14: {}
  opensuse-15-5-hardened-rke2-1-27-6: {}
  opensuse-15-5-hardened-rke2-1-26-9: {}
  opensuse-15-5-hardened-rke2-1-25-14: {}
```

The version of [Sylva diskimage-builder](https://gitlab.com/sylva-projects/sylva-elements/diskimage-builder) used for building the images is specified as:

```yaml
sylva_diskimagebuilder_version: 0.1.7
```

## Using Custom Images

Additional images can be created by forking the [Sylva diskimage-builder](https://gitlab.com/sylva-projects/sylva-elements/diskimage-builder) repository, producing the desired images, and then pushing them as OCI artifacts. These can then be used by specifying the OCI repository and adding the image key under `sylva_diskimagebuilder_images`.

```yaml
os_images_oci_registries:
  my-custom-repo:
    url: oci://registry.gitlab.com/my-custom-project
    tag: 0.1.1

sylva_diskimagebuilder_images:
  my-custom-image-rke2:
    os_images_oci_registry: my-custom-repo
    enabled: true
```

## Simplifying the Deployment Process

The available keys for each image are `default_enabled` and `enabled`, both of boolean type. To use an image, it must first be enabled, unless it's the `default_enabled` one.

**Tip**: The `default_enabled` image will always be downloaded and available for usage unless specifically disabled. To save space and time, disable it if you don't intend to use it. For example, if you want to deploy using Ubuntu plain and Kubernetes version 1.26:

```yaml
sylva_diskimagebuilder_images:
  ubuntu-jammy-plain-rke2-1-27-6:
    default_enabled: false
  ubuntu-jammy-plain-rke2-1-26-9:
    enabled: true
```

The values set in `sylva_diskimagebuilder_images` (and other similar OS image settings, like the one described [here](#bootstrap-optimization)) are parsed and made available to various units of the Sylva stack by a dedicated unit named `os-images-info`. In the management cluster, this unit creates a **ConfigMap/os-images-info** containing an **os_images** dictionary with entries for each enabled OS image and various details read from the OCI artifact metadata.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: os-images-info
  namespace: sylva-system
data:
  values.yaml: |
    os_images:
      ubuntu-jammy-plain-kubeadm-1-28-9:
        archive-size: "1387746656"
        commit-id: bfe8095e7e7e7104e790adf4f06112087e5981dc
        commit-tag: 0.2.10
        compression: gz
        filename: ubuntu-jammy-plain-kubeadm-1-28-9.raw
        flavor: plain
        hardened: "false"
        image-format: raw
        k8s-flavor: kubeadm
        k8s-version: 1.28.9
        md5: ba1736f210dd0c35eb701f83f02d3acc
        os: ubuntu
        os-release: jammy
        sha256: 58e259811ef065579311fa8637e8273229210c456a271e40a81feee87313ed82
        size: "22000000000"
        uri: oci://registry.gitlab.com/sylva-projects/sylva-elements/diskimage-builder/ubuntu-jammy-plain-kubeadm-1-28-9:0.2.10
```

This `os-images-info` ConfigMap is then used by other units:

- `get-openstack-images`: Pushes the enabled images into OpenStack Glance for **CAPO** deployment consumption.
- `os-image-server`: Exposes images for **CAPM3** deployment consumption. Its [`os-image-server`](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/os-image-server) Helm chart expects `os_images` as a top-level value.
- `cluster`: Defines the image fields in **OpenStackMachineTemplate** or **Metal3MachineTemplate** CAPI objects. Its [`sylva-capi-cluster`](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster) Helm chart expects `os_images` as a top-level value.

Using `sylva_diskimagebuilder_images` allows for computing the necessary values to consume **CAPO** & **CAPM3** deployment images automatically by **sylva-capi-cluster**. The image serving part has different implications for CAPO versus CAPM3:

- **For CAPO**: Enabling the desired image(s) will make the `get-openstack-images` unit check the existence of the image(s) in Glance (based on the MD5 sum contained in the OCI artifact annotations). If an image is not found, the artifact is downloaded and the image is pushed automatically.

```yaml
cluster:
  capo:
    image_key: ubuntu-jammy-plain-rke2-1-27-6
  control_plane:
    capo:
      image_key: ubuntu-jammy-hardened-rke2-1-27-6
```

- **For CAPM3**: The enabled images are automatically pulled and served by `os-image-server`, and the necessary values used to consume these images are automatically computed by [`sylva-capi-cluster`](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster).

```yaml
cluster:
  capm3:
    image_key: ubuntu-jammy-plain-rke2-1-27-6
  control-plane:
    capm3:
      image_key: ubuntu-jammy-hardened-rke2-1-27-6
```

**Note** on `sylva-capi-cluster` values:

- For CAPO, `image_name` is still available but not recommended. Either `image_key` or `image_name` can be specified under the same `capo` key, not both.
- For CAPM3, the same applies for `machine_image_*`. Either `image_key` or `machine_image_url` can be specified under the same `capm3` key, not both.

## Using Custom Images Distributed Over HTTP (Legacy)

Custom images provided using a simple web server can be used by specifying them under the `os_images` key:

```yaml
os_images:
  my-custom-image:
    uri: http://192.168.130.1:8080/custom-image.raw
    filename: custom-image.raw
    md5: 4f9f1ef17cf8ecd580a682f8bb4577fa
    sha256: 916afbbf52a177fec996ee20d980bcac7c62e475f389f172dd0b5d56b9da5c77
    image-format: raw
```

**Note**:

- `uri` and `filename` are always required fields.
- `md5` is required for CAPO.
- `sha256` and `image-format` are required for CAPM3.

## Bootstrap Optimization

To save time and space during bootstrap deployment, we introduced a new optional value **bootstrap_os_images_override_enabled**.

This optional list enables only the **OS images** required for management cluster deployment. All image_key(s) declared for the management cluster will be automatically added to this list:

- `cluster.capo.image_key` or `cluster.capm3.image_key`
- `cluster.control_plane.capo.image_key` or `cluster.control_plane.capm3.image_key` if defined
- `cluster.machine_deployment_default.capo.image_key` or `cluster.machine_deployment_default.capm3.image_key` if defined
- Every `cluster.machine_deployments.*.capo.image_key` or `cluster.machine_deployments.*.capm3.image_key` if defined

The objective is to save time and disk space during the bootstrap deployment and avoid unnecessary image downloads.

In most cases, you don't have to set the following list, but you can if you want to force the download of OS images.

Example of settings:

```yaml
bootstrap_os_images_override_enabled:
  - ubuntu-jammy-plain-rke2-1-27-10
  - ubuntu-jammy-plain-rke2-1-26-9
```

Items in this array are the same as those used in **`sylva_diskimagebuilder_images`** and **`os_images`**.

If you specify unknown images, an error message will be displayed.

## Updating Workload Cluster OS Images

If the OS image for a workload cluster deployment differs from the one used for the management cluster deployment (e.g., a different Kubernetes version), update the image serving services (external OpenStack Glance for CAPO and `os-image-server` unit deployed on the management cluster for CAPM3) before deploying the `cluster` unit.

### CAPO Deployments

Each CAPO deployment (management or workload cluster) has its own `get-openstack-images` unit responsible for uploading the desired OS image to OpenStack Glance. After updating the OS image in your workload cluster's `sylva_diskimagebuilder_images` values.yaml, execute the following command:

```shell
./apply-workload-cluster.sh <workload cluster ENV file>
```

The CAPO workload cluster Sylva-units release has a dedicated `os-images-info` unit instance to accompany the `get-openstack-images` unit. The **os-images-info ConfigMap** is independently generated/updated by `os-images-info` in each workload cluster namespace.

### CAPM3 Deployments

For CAPM3 workload cluster deployments, the OS image must be uploaded to and served by the management cluster's [`os-image-server`](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/os-image-server) unit. Follow these steps:

1. Edit your management cluster values.yaml to enable the desired OS image under `sylva_diskimagebuilder_images`.
2. Update the management cluster with:

    ```shell
    ./apply.sh <your-management-cluster-env>
    ```

3. Wait for the process to complete. A new ingress/service/pod should then serve the new OS image.
4. Update your workload cluster values.yaml (`image_key` under `.capm3`/`.control_plane.capm3`/`.machine_deployment_default.capm3`/`.machine_deployments.X.capm3`) to use the same image set in step 1.
5. Install/upgrade the workload cluster with:

    ```shell
    ./apply-workload-cluster.sh <your-workload-cluster-env>
    ```

Once the management cluster is updated, the `os-images-info` unit will update its output ConfigMap in the management cluster. The **ConfigMap/os-images-info** is cloned by Kyverno into a **ConfigMap/kyverno-cloned-os-images-info-capm3** (or its content is updated) in each workload cluster namespace. This ConfigMap is then used for `cluster` unit (`os_images` value) consumption.
