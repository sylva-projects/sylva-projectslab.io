---
title: Metal3
---

# Developer Perspective: Metal3

**A Deep-Dive has been written for this component** [here](/dev-zone/development-tricks/deep-dives/metal3).

## Use upstream IPA Downloader container in Metal3 chart

By default the chart `metal3` uses our custom IPA Downloader container which directly embeds the IPA ramdisk image.

However if a more recent IPA ramdisk image is required, it's possible to override our custom container to use an upstream one which retreives from Internet the IPA ramdisk image. In certain circumstances, to be able to reach it, the container must be configured with proxy settings like this:

```yaml
units:
  metal3:
    helmrelease_spec:
      values:
        images:
          ironicIPADownloader:
            repository: <upstream_image>
        httpProxy: <upstream http proxy, value set under proxies.http_proxy>
        httpsProxy: <upstream http proxy, value set under proxies.https_proxy>
```
