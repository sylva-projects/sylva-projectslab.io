---
title: KubeVirt
sidebar_position: 3
---

:::info
To effectively manage KubeVirt virtual machines within Sylva, a set of concise procedures has been developed. These procedures provide step-by-step commands for common tasks such as creating, starting, stopping, and accessing VMs. By following these procedures, users can efficiently handle various use cases and ensure smooth operation of their virtualized environments.

For detailed instructions and commands, refer to the [KubeVirt Procedures](/docs/Operations/special-operations/kubevirt-procedures) document.
:::

# KubeVirt Overview

KubeVirt extends Kubernetes to manage virtual machine workloads, allowing you to run and manage application containers and VMs side-by-side. It comprises custom resource definitions (CRDs) and controllers.

## Why Use KubeVirt?

- **Unified Orchestration Platform**: Running both VMs and containerized applications within a Kubernetes cluster simplifies orchestration needs, providing a unified platform for VNF and CNF.

- **Application Modernization**: KubeVirt helps reduce hurdles by allowing legacy and new applications to run on the same platform using both VMs and containers.

- **VNF Modernization**: Moving NFV workloads into VMs with KubeVirt allows you to host NFV VMs alongside containerized applications on Kubernetes.

## KubeVirt Networking

By default, VMs created with KubeVirt use the native networking configured in the pod. Typically, this means the bridge option is selected, and your VM has the pod's IP address. This approach enables interoperability, allowing the VM to integrate with sidecar containers and pod masquerading.

### KubeVirt with Multus

Multus is a secondary network that uses Multus-CNI, allowing multiple network interfaces to be attached to pods in Kubernetes. Ensure Multus is installed across your cluster and that you have created a NetworkAttachmentDefinition CRD.

Enable Multus via the environment's `values.yaml`:

```yaml
units:
  multus:
    enabled: true

```

### KubeVirt with SR-IOV

KubeVirt with SR-IOV enhances performance and flexibility in virtualized environments. SR-IOV can be enabled in Sylva with or without KubeVirt.

Enable SR-IOV via the environment's `values.yaml`:

```yaml
units:
  sriov:
    enabled: true
  sriov-resources:
    enabled: true

sriov:
  node_policies:
    dl360-sriov:
      resourceName: sriov
      numVfs: 16
      deviceType: "vfio-pci"
      nicSelector:
        pfNames:
          - ens2f0

cluster:
  kubelet_extra_args:
    max-pods: "228"
    feature-gates: "TopologyManager=true,CPUManager=true"
    topology-manager-policy: "best-effort"
    cpu-manager-policy: "static"
    system-reserved: "cpu=100m"
    kube-reserved: "cpu=100m"
```

### KubeVirt with Feature Gates

Sylva supports optional KubeVirt feature gates such as NUMA, CPUManager, Snapshot and ExpandDisks. These can be set as required.

## Units

### KubeVirt

`kubevirt` is an optional unit that can be enabled from environment values. This is main unit which will setup the related components required to create KubeVirt VMs.

Enable KubeVirt via the environment's `values.yaml`:

```yaml
units:
  kubevirt:
    enabled: true
```

### KubeVirt Test VMs

The `kubevirt-test-vms` unit is optional and dependent on the KubeVirt and Multus units.

Examples:
- [KubeVirt VM with Cirros Image](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/kustomize-units/kubevirt-test-vms/cirros-vm.yaml)
- [KubeVirt CentOS VM with Multus](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/kustomize-units/kubevirt-test-vms/multus-vm.yaml)

Enable KubeVirt Test VMs via the environment's `values.yaml`:

```yaml
units:
  kubevirt-test-vms:
    enabled: true
```

Example inside `kubevirt-test-vms` unit is creating VM with multus, so need to enable `multus` unit as well.

[How to enable multus](#kubevirt-with-multus)

### KubeVirt Dashboard

The `kubevirt-manager` unit is an optional component that provides a dashboard for managing KubeVirt VMs. It works in conjunction with the `kubevirt` unit.

With the `kubevirt-manager`, VMs can be managed through a graphical user interface (GUI), allowing users to:

- Create new VMs
- Delete existing VMs
- Modify VM configurations
- Access VMs remotely

The images required to create VMs can be accessed from various sources, including:

- Docker repositories
- HTTP/HTTPS URLs
- S3 buckets
- Container registries
- PersistentVolumeClaims (PVCs)

### Containerized Data Importer

The Containerized Data Importer (CDI) is a key component when working with KubeVirt. It works in conjunction with the `kubevirt` unit and specially required with kubevirt dashboard.

The `kubevirt-cdi` unit is an optional component which is enabled by default with `kubevirt` unit.

When deploying KubeVirt in a Kubernetes cluster, CDI is often included as an extension that enables several capabilities related to the management of VM disk images.

CDI can help automate tasks like importing VMs from different sources, cloning persistent volumes, and uploading virtual disk images (such as QCOW2, RAW, or VMDK).

[How to create VMs with DataVolume](/docs/Operations/special-operations/kubevirt-procedures#how-to-create-vm-with-datavolume)

## Using KubeVirt

A KubeVirt virtual machine is a Pod running a KVM instance in a container. Associated with the Pod is a VirtualMachineInstance, linking the Pod to a VirtualMachine and providing endpoints for advanced operations like migration and disk hotplug.

The `kubevirt-test-vms` unit creates VMs on management and workload clusters, typically aimed at Baremetal Workload clusters for performance reasons.

## How images used with kubevirt

When dealing with KubeVirt, you typically interact with different types of images that serve various purposes within the virtual machine lifecycle. Here are the main types of images you might work with:

### VM Disk Images

Operating System Images: These are disk images that contain the operating system to be run inside the virtual machine. Common formats include:

- QCOW2 (QEMU Copy-On-Write version 2)
- RAW

Examples: Fedora Cloud images, Ubuntu Cloud images, CentOS images.

### Container Images

VM Launcher: Container images that KubeVirt uses to launch and manage virtual machines. These containers run QEMU/KVM processes to provide virtualization capabilities.
Infrastructure Components: Container images for various KubeVirt components like the virt-controller, virt-api, and virt-handler which manage the lifecycle of virtual machines within Kubernetes.

### Cloud-Init Configurations

Cloud-Init NoCloud Images: These are images containing cloud-init configuration data. They are used to automate the initialization process of VMs by providing scripts and metadata to be run on first boot.

Example:

[Creating a VM from an OS Image](/docs/Operations/special-operations/kubevirt-procedures#how-to-create-a-vm)
