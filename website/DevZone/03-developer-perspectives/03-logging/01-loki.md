---
title: "Logging with Loki"
---

# Developer Perspective: Logging with Loki

Loki documentation is available [here](https://grafana.com/docs/loki/latest/)

## Log retention

By default in Loki, the log retention is disabled. The logs live forever in Loki.

When deploying Loki through `sylva-unit`, we **enable** Log retention by default with the following configuration for Loki [Compactor](https://grafana.com/docs/loki/latest/operations/storage/retention/#compactor):

```yaml
# charts/sylva-units/values.yaml
  loki:
    compactor:
      compaction_interval: 1h
      retention_enabled: true               # to enable log retention
      retention_delete_delay: 2h            # delay before deleting marked chunks
      retention_delete_worker_count: 150
      delete_request_store: s3
    limits_config:
      retention_period: 72h                 # global retention delay for all logs
```

More details can be found in [Loki documentation v3.2.x](https://grafana.com/docs/loki/latest/operations/storage/retention/)
