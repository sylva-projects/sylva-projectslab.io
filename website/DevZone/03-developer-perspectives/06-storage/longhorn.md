---
title: "Developer Perspective: Longhorn"
sidebar_label: "Longhorn"
---

## Longhorn, Sylva's storage solution for BareMetal

To have the implementation picture in mind, we propagate this individual BMH metadata (annotation `sylvaproject.org/default-longhorn-disks-config=<customized default disks>`) to consumer CAPI machine (to achieve per-node granularity), to be further used for both disk configuration/mounting and setting annotations and label inside cloud-init. <br/>
To achieve this propagation, we need to rely on the "BMH -\> Metal3 metadata -\> ds.metadata" channel and set the node annotation based on the said BMH annotation running a `kubectl annotate $(hostname) node.longhorn.io/default-disks-config=<customized default disks>` command on each node. <br/>
This workflow is:

- use the following `sylva-units` values to annotate the BMH:

```yaml

cluster:
  baremetal_hosts:
    my-bmh-foo:
      longhorn_disk_config:
        - path: "/var/longhorn/disks/disk_by-path_pci-0000:00:0a.0"
          storageReserved: 0
          allowScheduling: true
          tags:
            - vhdd
            - fast
        - path: "/var/longhorn/disks/disk_by-path_pci-0000:00:0b.0"
          storageReserved: 0
          allowScheduling: true
          tags:
            - vhdd
            - fast

```

or 

```yaml

baremetal_hosts:
  my-bmh-foo:
    bmh_metadata:
      annotations:
        sylvaproject.org/default-longhorn-disks-config: >-
          [{"path":"/var/longhorn/disks/disk_by-path_pci-0000:00:0a.0", "storageReserved":0, "allowScheduling":true, "tags":[ "vhdd", "fast"] },
           {"path":"/var/longhorn/disks/disk_by-path_pci-0000:00:0b.0", "storageReserved":0, "allowScheduling":true, "tags":[ "vhdd", "fast"] }]

```

- if either way is provided, then BMH will be annotated using the available contents
- but if both are provided, then the contents must be same for both the `longhorn_disk_config` and `sylvaproject.org/default-longhorn-disks-config` annotation, otherwise a helm templating error will be returned


```yaml

apiVersion: v1
items:
- apiVersion: metal3.io/v1alpha1
  kind: BareMetalHost
  metadata:
    annotations:
      meta.helm.sh/release-name: cluster-bmh
      meta.helm.sh/release-namespace: sylva-system
      sylvaproject.org/baremetal-host-name: management-cp
      sylvaproject.org/cluster-name: management-cluster
      sylvaproject.org/default-longhorn-disks-config: W3siYWxsb3dTY2hlZHVsaW5nIjp0cnVlLCJwYXRoIjoiL3Zhci9sb25naG9ybi9kaXNrcy92ZGEiLCJzdG9yYWdlUmVzZXJ2ZWQiOjAsInRhZ3MiOlsiaGRkIiwic2xvdyJdfSx7ImFsbG93U2NoZWR1bGluZyI6dHJ1ZSwicGF0aCI6Ii92YXIvbG9uZ2hvcm4vZGlza3MvdmRiIiwic3RvcmFnZVJlc2VydmVkIjowLCJ0YWdzIjpbImhkZCIsInNsb3ciXX1d

```

- which allows `Metal3DataTemplate.spec.metaData.fromAnnotations` to read it based on:

```yaml

metaData:
  :
  fromAnnotations:
  - key: sylva_longhorn_disks
    object: baremetalhost
    annotation: sylvaproject.org/default-longhorn-disks-config
  {{- end }}

```

- to further use it inside cloud-init for both CP and MD nodes (facilitated by [`sylva-capi-cluster`](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster)'s named template [`shell-longhorn-node-metadata`](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster/-/blob/0.1.31/templates/_helpers.tpl?ref_type=tags#L378-462)), with something like:

```yaml

postRKE2Commands:
  :
  - /var/lib/rancher/rke2/bin/kubectl --kubeconfig /etc/rancher/rke2/rke2.yaml annotate node $(hostname) {{ printf "%s=%s" "node.longhorn.io/default-disks-config" `{{ ds.meta_data.sylva_longhorn_disks }}` }}
  - /var/lib/rancher/rke2/bin/kubectl --kubeconfig /etc/rancher/rke2/rke2.yaml label node $(hostname) node.longhorn.io/create-default-disk=config
```

- which will result in a bootstrapped node like:

```yaml

apiVersion: v1
kind: Node
metadata:
  annotations:
    node.longhorn.io/default-disks-config: '[{"path":"/var/longhorn/disks/disk_by-path_pci-0000:18:00.0-scsi-0:3:111:0",
      "storageReserved":0, "allowScheduling":true, "tags":[ "ssd", "fast" ]}, {"path":"/var/longhorn/disks/sde", "storageReserved":0, "allowScheduling":true, "tags":[ "hdd", "slow" ]}]'
    :
    rke2.io/node-args: '["server","--cluster-cidr","100.72.0.0/16","--cni","calico","--kubelet-arg","anonymous-auth=false","--kubelet-arg","provider-id=metal3://sylva-system/mgmt-cluster-my-bmh-foo/mgmt-cluster-cp-056108e4c3-5b9sj","--node-label","--node-label","node.longhorn.io/create-default-disk=config","--profile","cis-1.23","--service-cidr","100.73.0.0/16","--tls-san","172.18.0.2","--tls-san","192.168.100.2","--token","********"]'
  labels:
    :
    node.longhorn.io/create-default-disk: config

```

```shell

# checking the mounted disks
root@gmt-cluster-my-bmh-foo:/# lsblk
NAME  MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
vda   252:0    0   50G  0 disk
├─vda1
│     252:1    0  550M  0 part /boot/efi
├─vda2
│     252:2    0    8M  0 part
├─vda3
│     252:3    0 49.4G  0 part /
└─vda4
      252:4    0   65M  0 part
vdb   252:16   0   50G  0 disk
vdc   252:32   0   50G  0 disk /var/longhorn/disks/disk_by-path_pci-0000:18:00.0-scsi-0:3:111:0
root@gmt-cluster-my-bmh-foo:/#

```

```shell

# checking the PVs mounted on the node
$ kubectl get node management-cluster-md0-hnjtc-6j49q -o yaml | yq .status.volumesInUse
- kubernetes.io/csi/driver.longhorn.io^pvc-17e5863f-4d15-419c-bf20-48e2547d64fc
- kubernetes.io/csi/driver.longhorn.io^pvc-1b91ddb0-15a4-4c50-af90-c33280e7cdfa
- kubernetes.io/csi/driver.longhorn.io^pvc-1cb0e490-d030-42ea-a16f-60a9b19f1e3a
- kubernetes.io/csi/driver.longhorn.io^pvc-1dc68632-3587-48a5-b481-d0f5f5b5dd43
- kubernetes.io/csi/driver.longhorn.io^pvc-68dc4c38-ecef-48d2-a02b-fe69d4ff1e26
- kubernetes.io/csi/driver.longhorn.io^pvc-7f0ec91b-dec6-4f53-a277-4d6c62279760
- kubernetes.io/csi/driver.longhorn.io^pvc-9c103ae6-c68f-42a2-8e88-dfe41f6c4c25
- kubernetes.io/csi/driver.longhorn.io^pvc-c3095a2c-ba1f-49a7-8796-7dc7dfc5f2de

```

When passing the value of this annotation to the BMH, we base64 encode it to avoid issues with parsing the complex JSON structure during cloud-init init phase. We base64 decode it during cloud-init commands injection.

```terminal

$ kubectl get secrets sylva-units-values -o yaml | yq .data.values | base64 -d | yq '.cluster.baremetal_hosts."management-cp".longhorn_disk_config'
- path: "/var/longhorn/disks/sde"
  storageReserved: 0
  allowScheduling: true
  tags:
    - hdd
    - slow
- path: "/var/longhorn/disks/disk_by-path_pci-0000:18:00.0-scsi-0:3:111:0"
  storageReserved: 0
  allowScheduling: true
  tags:
    - ssd
    - fast
$
$ kubectl get bmh mgmt-cluster-management-cp -o yaml | yq .metadata.annotations
meta.helm.sh/release-name: cluster
meta.helm.sh/release-namespace: sylva-system
sylvaproject.org/default-longhorn-disks-config: W3sicGF0aCI6Ii92YXIvbG9uZ2hvcm4vZGlza3MvZGlza19ieS1wYXRoX3BjaS0wMDAwOjAwOjBiLjAiLCAic3RvcmFnZVJlc2VydmVkIjowLCAiYWxsb3dTY2hlZHVsaW5nIjp0cnVlLCAidGFncyI6WyAic3NkIiwgImZhc3QiIF19XQ==
$

```
