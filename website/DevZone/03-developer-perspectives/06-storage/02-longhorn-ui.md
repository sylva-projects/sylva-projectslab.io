---
title: Longhorn User Interface
sidebar_label: Longhorn UI
---

The Longhorn User Interface can be an useful tool for monitoring and troubleshooting.

:::note
Longhorn CSI is available for baremetal clusters and the disks for each node can be configured through `values.yaml`, more details [here](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/docs/longhorn.md#how-to-define-baremetal-node-drives-as-longhorn-disks-in-valuesyaml).
:::

:::info Warning
The UI also allows some storage operations, but important operations (e.g. scaling) must not be performed through the UI, but through the main LCM tools, by changing the cluster definition files.
:::

## Access to the UI

The Longhorn User Interface is available through Rancher UI.

In Rancher, navigate to the Kubernetes cluster holding the Longhorn instance you want to see, and find the Longhorn item in the left column.

![screenshot](img/installed-longhorn.png)

Then click on the Longhorn link on the right.


## Longhorn dashboard

The main page will be the dashboard, showing a global view on volumes, storage, and nodes. Node and Volume tabs will provide detail informations and operation options.

![dashboard](img/longhorn-ui.png)

The central graph provides overall storage capacity information

- `Schedulable`: the actual space that can be used for Longhorn volume scheduling
- `Reserved`: the space reserved for other applications and system
- `Used`: the actual space that has been used by Longhorn, system, and other applications
- `Disabled`: the total space of the disks/nodes on which Longhorn volumes are not allowed for scheduling
