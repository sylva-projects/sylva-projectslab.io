---
title: Longhorn
sidebar_label: Longhorn
---

# Longhorn in Sylva

## Overview

[Longhorn](https://longhorn.io/) is a distributed block storage system for Kubernetes. In Sylva, you can specify which disks are used for Longhorn on a per-server basis for baremetal servers, as servers may have different hardware topologies.

You can access the operationnal guide [here](/docs/Operations/storage-operations/longhorn).

## Declarative Longhorn Settings and Sylva Context

According to the [documentation](https://longhorn.io/docs/1.5.3/advanced-resources/default-disk-and-node-config/#customizing-default-disks-for-new-nodes), depending on whether a node’s label `node.longhorn.io/create-default-disk: 'config'` is present, Longhorn will check for the `node.longhorn.io/default-disks-config` annotation (which allows a [rich set of settings](https://longhorn.io/docs/1.5.3/advanced-resources/default-disk-and-node-config/#customizing-default-disks-for-new-nodes)) and create default disks according to it. <br/>
So in order to configure disks for Longhorn from the cluster creation time, you need to:

1. Label (and annotate) Kubernetes Nodes declaratively.
2. Mount the disks appropriately. Longhorn does not take of that _at all_.

Node labels and annotations are set using CAPI post-bootstrap `kubectl annotate node $(hostname)` and `kubectl label node $(hostname)` cloud-init (`/var/lib/cloud/instance/scripts/runcmd`) commands, provided by [`sylva-capi-cluster`](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster).

Similarly, in pre-bootstrap cloud-init commands (also provided by [`sylva-capi-cluster`](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster)) we handle the disk mounting part, where the convention is to use as mount point the value of of the `"path"` key inside `longhorn_disk_config` list of dicts`. _See below_

## Disk Mounting

Disks are mounted using the value of the `"path"` key inside `longhorn_disk_config` list of dicts. For example:

```yaml
baremetal_hosts:
  my-bmh-foo:
    bmh_metadata:
      annotations:
        sylvaproject.org/default-longhorn-disks-config: >-
          [{"path":"/var/longhorn/disks/sde", "storageReserved":0, "allowScheduling":true, "tags":[ "ssd", "fast"] },
           {"path":"/var/longhorn/disks/disk_by-path_pci-0000:00:0b.0", "storageReserved":0, "allowScheduling":true, "tags":[ "vhdd", "fast"] }]
    longhorn_disk_config:
      - path: "/var/longhorn/disks/sde"
        storageReserved: 0
        allowScheduling: true
        tags:
          - ssd
          - fast
      - path: "/var/longhorn/disks/disk_by-path_pci-0000:00:0b.0"
        storageReserved: 0
        allowScheduling: true
        tags:
          - vhdd
          - fast
```

This configuration will:

- Mount `/dev/sde` at `/var/longhorn/disks/sde`.
- Mount `/dev/disk/by-path/pci-0000:00:0b.0` at `/var/longhorn/disks/disk_by-path_pci-0000:00:0b.0`.

:::info
We needed the ability to specify which disks are used for Longhorn _on a per-server basis for baremetal servers_. This per-node granularity is critical to have, because experience has proven that it's not reasonable to expect all nodes in a given group (control nodes, or workers of a given MachineDeployment) would have same disk hardware or PCI topology. <br/>

Regardless of how the Longhorn disk configuration is passed through sylva-units values, be it `.cluster.baremetal_hosts.X.bmh_metadata."sylvaproject.org/default-longhorn-disks-config"` or `.cluster.baremetal_hosts.X.longhorn_disk_config`,
their contents will become the value for special annotation `sylvaproject.org/default-longhorn-disks-config=<customized default disks>` set **on BareMetalHost resources**. With this single BMH annotation we're:

1) mounting the disks for Longhorn

2) annotating the node created by the Machine consuming the BareMetalHost with `node.longhorn.io/default-disks-config=<customized default disks>`.

3) label the node created by the Machine consuming the BareMetalHost with `node.longhorn.io/create-default-disk: 'config'`.

**Please see [here](/dev-zone/tools/devtips/storage/longhorn) how this is achieved.**

The rest falls to the `longhorn` unit responsibility.
:::
