---
sidebar_position: 8
title: Cosign
---

# Image signatures

This document explains how `cosign` is used to protect the artifacts of this project.

## Generate Keys

The first step is to generate the cosign keypair, i.e. the signing material. In the context of Sylva developments, we can use the GitLab provider for that and the signing material shall be stored as CI/CD variables. 

> **_SECURITY NOTICE_**: in production, it is recommend to store the signing material in an external Key Management Service (KMS), e.g. Vault, instead the CI/CD variables, with a fine-grained access control to these material (in other words, with a fine-grained control on who is allowed to sign the artifacts).
>
> For example, if using Vault: when the signing is handled by a CI, the signing job shall authenticate against Vault with the JWT method (see https://docs.gitlab.com/ee/ci/secrets/) to fetch the signing keys. On the Vault side, a role is configured to grant only the Job's JWT with the appropriate bound claims, e.g. a set of GitLab user ID, to access the secret path with the signing keys.


```shell
cosign generate-key-pair gitlab://<project_id>
```

Before creating the keypair with the GitLab provider, export an environment variable `GITLAB_TOKEN` with rights to create CI/CD variables:

```shell
export GITLAB_TOKEN=glpat-Z7Dj....
```

Then generate the cosign key pair with the GitLab provider:

```shell
$ cosign generate-key-pair gitlab://42451983
Enter password for private key: 
Enter password for private key again: 
Password written to "COSIGN_PASSWORD" variable
Private key written to "COSIGN_PRIVATE_KEY" variable
Public key written to "COSIGN_PUBLIC_KEY" variable
Public key also written to cosign.pub
```

On success, three CI/CD variables are added to the project:

> **_SECURITY NOTICE_**: it is recommended to put the cosign CI/CD variables in protected mode.

![variable](./img/cosign-gitlab-variables.png)

The public key can be retrieved with (assuming a `GITLAB_TOKEN` environment variable with read privilege on CI/CD variables):

```shell
$ cosign public-key --key gitlab://42451983
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEEN6LNycNA/OB8/dtqTPZcPDuLnxW
hR0Rskmno7Lx1WqBl2ylN/sfkLEAPfCkizceHiu/fw8lnsPq9uSGlAICeQ==
-----END PUBLIC KEY-----
```

## OCI Artifacts Signature

### Signing

The OCI artifacts are automaticaly signed by scripts located in `sylva-core/tools/oci`. However, the following describes a manual process for **the sake of understanding**.

Signing the artifact identified by a tag can lead to sign a different image than the intended one. It is better to identify the artifact with a digest rather than tag. The manifest digest can be obtained via the GitLab API or, better, by using `crane`. For example, considering the Harbor Helm chart stored in the Sylva OCI registry:

```shell
$ crane digest registry.gitlab.com/sylva-projects/sylva-core/harbor:1.14.0
sha256:7dbce8f79f54de305f9f06b60e03f4a7f7cd5ae6e9061036325e7a22aa7e25c4

$ curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v4/projects/42451983/registry/repositories/5829972/tags/1.14.0 | jq '.digest'
"sha256:7dbce8f79f54de305f9f06b60e03f4a7f7cd5ae6e9061036325e7a22aa7e25c4"
```

Note that the signer must authenticate to the registry before signing because the signature shall be stored in the OCI repository as a new tag:

```shell
docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD registry.gitlab.com
```

Then the artifact can be signed. In the CI, we can sign by refering the environment variable `COSIGN_PRIVATE_KEY`:

> **_NOTE_**: when refering  `COSIGN_PRIVATE_KEY`, the environment variable `COSIGN_PASSWORD` must be set, so that `cosign` can decrypt the private key

```shell
cosign sign --key env://COSIGN_PRIVATE_KEY registry.gitlab.com/sylva-projects/sylva-core/harbor@sha256:7dbce8f79f54de305f9f06b60e03f4a7f7cd5ae6e9061036325e7a22aa7e25c4

    The sigstore service, hosted by sigstore a Series of LF Projects, LLC, is provided pursuant to the Hosted Project Tools Terms of Use, available at https://lfprojects.org/policies/hosted-project-tools-terms-of-use/.
    Note that if your submission includes personal data associated with this signed artifact, it will be part of an immutable record.
    This may include the email address associated with the account with which you authenticate your contractual Agreement.
    This information will be used for signing this artifact and will be stored in public transparency logs and cannot be removed later, and is subject to the Immutable Record notice at https://lfprojects.org/policies/hosted-project-tools-immutable-records/.

By typing 'y', you attest that (1) you are not submitting the personal data of any other person; and (2) you understand and agree to the statement and the Agreement terms at the URLs listed above.
tlog entry created with index: .........
Pushing signature to: registry.gitlab.com/sylva-projects/sylva-core/harbor
```

Now, you should see the new tag `sha256-7dbce8f79f54de305f9f06b60e03f4a7f7cd5ae6e9061036325e7a22aa7e25c4.sig` for the `harbor` artifact:

![oci-signature](./img/cosign-oci-signature.png)

It is also possible to sign by fetching the private key from GitLab (assuming `GITLAB_TOKEN` with read access on project variables):

```shell
cosign sign --key gitlab://42451983 registry.gitlab.com/sylva-projects/sylva-core/harbor@sha256:7dbce8f79f54de305f9f06b60e03f4a7f7cd5ae6e9061036325e7a22aa7e25c4
```

**Important Notice:**   By default, **artifact signatures are uploaded to [Rekor](https://github.com/sigstore/rekor)**. Quoting https://blog.sigstore.dev/cosign-2-0-released/:

> To not upload to Rekor, include `--tlog-upload=false`.
>
>* You must also include `--insecure-ignore-tlog=true` when verifying an artifact that was not uploaded to Rekor.
>* Examples of when you may want to skip uploading to the transparency log are if you have a private Sigstore deployment that does not use >transparency or a private artifact.
>* We (Sigstore) strongly encourage all other use-cases to upload artifact signatures to Rekor. Transparency is a critical component of supply chain security, to allow artifact maintainers and consumers to monitor a public log for their artifacts and signing identities.

### Verifying

The signature is verified against the public key fetched from an environment variable or from the GitLab project hosting the registry:

```shell
cosign verify --key env://COSIGN_PUBLIC_KEY registry.gitlab.com/sylva-projects/sylva-core/harbor:1.14.0

cosign verify --key gitlab://<project ID> registry.gitlab.com/sylva-projects/sylva-core/harbor:1.14.0
```

You can verify manually the signature of the OCI artifacts by using the cosign public key available in the project, i.e. `./tools/oci/cosign.pub`:

```shell
$ cosign verify --key ./tools/oci/cosign.pub  registry.gitlab.com/sylva-projects/sylva-core/harbor:1.14.0 --insecure-ignore-tlog=true
WARNING: Skipping tlog verification is an insecure practice that lacks of transparency and auditability verification for the signature.

Verification for registry.gitlab.com/sylva-projects/sylva-core/harbor:1.14.0 --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - The signatures were verified against the specified public key

[{"critical":{"identity":{"docker-reference":"registry.gitlab.com/sylva-projects/sylva-core/harbor"},"image":{"docker-manifest-digest":"sha256:7dbce8f79f54de305f9f06b60e03f4a7f7cd5ae6e9061036325e7a22aa7e25c4"},"type":"cosign container image signature"},"optional":null}]
```

## Upstream Images Signatures

### Signing

Once an upstream Docker image is considered safe after a validation process (this process is out the scope of this document and is user specific), it can be signed so that it can be trusted.

Let's imagine we want to sign the image `quay.io/keycloak/keycloak:21.1.2` (image arbitrarily chosen). Signing the artifact identified by a tag can lead to sign a different image than the intended one. It is better to identify the artifact with a digest rather than tag. The digest can be obtained by using crane:

```shell
crane digest quay.io/keycloak/keycloak:21.1.2
sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0
```

By default a signature is a new tag added to the image repository but, obviously, we do not have the right privileges on the upstream registries, so the signatures must be stored on a different OCI registry. In such a case, the location of the signature repository can be specified by the  environment variable `COSIGN_REPOSITORY` (see https://github.com/sigstore/cosign#specifying-registry).

> **_NOTE_**: the Sylva project may sign some upstream images after processing some basic vulnerabilities checking and evaluation. If so, the signatures are stored in the repository `registry.gitlab.com/sylva-projects/sylva-core/signatures`.

Then, authenticate to the OCI registry expected to store the signature (the signatures shall be stored as new tags) and sign the image:

```shell
docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD registry.gitlab.com
```

Sign the image while indicatint to store the signatures in a separate repository, for example registry `registry.gitlab.com/sylva-projects/sylva-core/signatures`, with the following command (assumint the Cosign private key available from the environment variable):

```shell
$ COSIGN_REPOSITORY=registry registry.gitlab.com/sylva-projects/sylva-core/signatures cosign sign --key env://COSIGN_PRIVATE_KEY quay.io/keycloak/keycloak@sha256:3408c186dde4a95c2b99ef1290721bf1d253d64ba3a1de0a46c667b8288051f0

  The sigstore service, hosted by sigstore a Series of LF Projects, LLC, is provided pursuant to the Hosted Project Tools Terms of Use, available at https://lfprojects.org/policies/hosted-project-tools-terms-of-use/.
  Note that if your submission includes personal data associated with this signed artifact, it will be part of an immutable record.
  This may include the email address associated with the account with which you authenticate your contractual Agreement.
  This information will be used for signing this artifact and will be stored in public transparency logs and cannot be removed later, and is subject to the Immutable Record notice at https://lfprojects.org/policies/hosted-project-tools-immutable-records/.

By typing 'y', you attest that (1) you are not submitting the personal data of any other person; and (2) you understand and agree to the statement and the Agreement terms at the URLs listed above.
Are you sure you would like to continue? [y/N] y
Pushing signature to: registry.gitlab.com/sylva-projects/sylva-core/signatures
```

> To not upload to Rekor, add the flag `--tlog-upload=false`. 
> Then, you must also include `--insecure-ignore-tlog=true` when verifying an artifact that was not uploaded to Rekor.

All signatures of the upstream images appears as tags in the repository `registry.gitlab.com/sylva-projects/sylva-core/signatures`:

![signatures](./img/cosign-signatures.png)

### Verifying

The signature of an upstream image can be verified by specifying the location of the signatures and by using the public key, aka `cosign.pub`, stored in `sylva-core/tools/oci/`

```shell
COSIGN_REPOSITORY=registry.gitlab.com/sylva-projects/sylva-core/signatures cosign verify --key ./tools/oci/cosign.pub --insecure-ignore-tlog quay.io/keycloak/keycloak:21.1.2
```

The **Sylva** stack can implement a Kyverno policy that verifies the signature of an upstream image before running the corresponding Pod. To enable this feature, fix the following fields in the file `values.yaml`:

* `.Values.security.oci_artifacts_cosign_pubkey`: the public key in PEM format to be used to verify the sylva OCI artifacts.
* `.Values.security.upstream_images_signature.upstream_images_verify`: boolean enabling the verification of the signature for the upstream images.
* `.Values.security.upstream_images_signature.policy_action`: action taken by kyverno when validation fails. It can be editing a report (set Audit) or blocking the creation of the pod (set Enforce).
* `.Values.security.upstream_images_signature.upstream_images_signature_repository`: the repository storing the signatures of the upstream images, e.g. `registry.gitlab.com/sylva-projects/sylva-core/signatures`.
* `.Values.security.upstream_images_signature.cosign_public_key`: the public key that kyverno must use to verify the signatures of the trusted upstream images. By default this value is set to `.Values.security.oci_artifacts_cosign_pubkey`, meaning that Sylva artifacts and Upstream images are trusted by the same cosign keypair. However, the upstream images can be trusted by a different key. Indeed in a **zero-trust approach**, the user shall likey run its own image validation process an sign the upstream images with its own `cosign` keypair.
* `.Values.security.upstream_images_signature.upstream_images_list`: the list of trusted images to verify, wildcard is allowed (e.g. `hashicorp/vault*`).

For example, the setting can be:

```yaml
security:
  oci_artifacts_cosign_pubkey: |
     -----BEGIN PUBLIC KEY-----
     MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEEN6LNycNA/OB8/dtqTPZcPDuLnxW
     hR0Rskmno7Lx1WqBl2ylN/sfkLEAPfCkizceHiu/fw8lnsPq9uSGlAICeQ==
     -----END PUBLIC KEY-----
  upstream_images_signature:
    upstream_images_verify: true
    policy_action: Audit
    upstream_images_signature_repository: '{{ .Values.sylva_base_oci_registry }}/sylva-core/signatures'
    cosign_public_key: '{{ .Values.security.oci_artifacts_cosign_pubkey }}'
    upstream_images_list:
      - quay.io/keycloak/keycloak*
      - hashicorp/vault*
```
