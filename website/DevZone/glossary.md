---
title: Glossary
sidebar_position: 6
---

:::info
Having a clear glossary of the terminology we use in Sylva will be useful in a gen-AI context for rapidly retrieving sense in generated outputs.
:::

Here is a glossary of key terms related to the Sylva project and the broader telecom ecosystem:

| Term                      | Definition |
|---------------------------|------------|
| **5G**                    | The fifth generation of mobile network technology, offering faster speeds and more reliable connections on mobile devices. |
| **Bare Metal**            | Physical servers without hypervisors, offering direct access to hardware resources. |
| **CaaS (Container as a Service)** | A cloud service model that allows users to manage and deploy containers with less overhead. |
| **Cloud Native**          | A set of practices that empowers an organization to build and manage scalable applications in modern, dynamic environments such as public, private, and hybrid clouds. |
| **CNF (Cloud Native Network Function)** | A network function designed to run in a cloud-native environment, leveraging containerization and microservices architecture. |
| **Containerization**      | The use of Linux containers to deploy applications in a fast, efficient, and portable manner. |
| **DPDK (Data Plane Development Kit)** | A set of libraries and drivers for fast packet processing in software-defined networking (SDN) applications. |
| **Edge Computing**        | A distributed computing paradigm that brings computation and data storage closer to the sources of data. |
| **EUCS (European Union Cloud Strategy)** | The European Union's framework for promoting cloud adoption and ensuring data security and sovereignty within the EU. |
| **Kubernetes**            | An open-source platform for automating the deployment, scaling, and operations of application containers across clusters of hosts. |
| **O-RAN (Open Radio Access Network)** | An industry consortium fostering a more open and intelligent RAN architecture. |
| **OpenStack**             | An open-source cloud computing platform for building and managing public and private clouds. |
| **PTP (Precision Time Protocol)** | A protocol used to synchronize clocks throughout a computer network. |
| **Units**                 | A single software dependency or component used in Sylva. Some _units_ can be optional. It has been used to differ from the Kustomize "components", also used in Sylva. |
| **RAN (Radio Access Network)** | Part of a mobile telecommunication system that connects individual devices to other parts of a network through radio connections. |
| **SDN (Software Defined Networking)** | An approach to networking that uses software-based controllers or application programming interfaces (APIs) to direct traffic on the network and communicate with the underlying hardware infrastructure. |
| **SR-IOV (Single Root Input/Output Virtualization)** | A technology that allows a single physical network device to appear as multiple separate, virtualized devices. |
| **Telco**                 | Short for telecommunications company, a provider of telecommunications services. |
| **VMWare**                | A subsidiary of Dell Technologies that provides cloud computing and platform virtualization software and services. |
| **Workload Cluster**      | In the context of Kubernetes, a set of worker machines, called nodes, that run containerized applications. Every cluster has at least one worker node. |
