---
title: Introduction & Contribution
sidebar_position: 1
---

## Sylva CI Introduction & Contribution Guide

This guide provides an overview of the CI/CD chain used by Sylva projects and outlines how to contribute to it. The CI chain ensures good code quality, deployment, and testing of Sylva in multiple environments.

### Pipelines Organization

Pipelines for `sylva-projects/sylva-elements/**/*` and `sylva-projects/sylva-core` are configured via the `.gitlab-ci.yml` file, which includes files under `.gitlab/ci/` for easier maintenance. To keep the code maintainable and limit duplication, we rely on GitLab CI templates:

- From the "to be continuous" project: [GitLab Project](https://gitlab.com/to-be-continuous)
- Developed by the Sylva team: [Sylva CI Templates](https://gitlab.com/sylva-projects/sylva-elements/ci-tooling/ci-templates)

These templates are imported in the `.gitlab-ci.yml` file and are always versioned.

### Sylva-elements Pipelines

Each satellite repository in the Sylva-elements group has its own CI jobs for linting, building, and testing. Additionally, some repositories have a `cross-repo` pipeline configured. These `cross-repo` pipelines allow maintainers of `Sylva-core` to start a Sylva-core deployment that uses the code from the `sylva-elements/xxx` MR directly from this MR.

```mermaid
flowchart LR

subgraph Sylva-core
direction TB
    E(Lint)
    --> F(Build)
    --> G(Deploy)
    --> H(Test)
end

subgraph Sylva-element/XYZ
direction TB
    A(Lint)
    --> B(Build)
    --> C(Tests)
    --> D(Publish)
end
id[cross-repo pipeline]

Sylva-element/XYZ --> id
id --> E
```

### Sylva-core

Each time you create a new MR in Sylva-core, a series of pipelines will start automatically based on the files you modified. These jobs ensure your code is compliant with different linters and build temporary OCI artifacts for deployment in this MR.

Once these jobs succeed, deployment jobs can be started.

Supported deployment flavors:

| Flavor      | Runners                   | Allowed to Start           |
|-------------|---------------------------|----------------------------|
| capd        | gitlab.com                | All Sylva-core devs        |
| capo        | Dedicated Orange runners  | Core devs & authorized list|
| capm3-virt  | Equinix servers           | All Sylva-core devs        |

All these flavors are available in OCI and non-OCI variants. The OCI variant means the deployment will rely on OCI artifacts built by the CI instead of fetching resources from upstream.

Default deployment jobs perform the following steps:

1. Deploy management cluster
2. Deploy a workload cluster
3. Perform an update of the management cluster
4. Perform an update of the workload cluster
5. Run tests (login tests, security tests, etc.)
6. Try to delete the workload cluster
7. Perform cleanup if needed, depending on the deployment flavor

Many specific scenario and option are available for theses pipelines, theses can be customized directly from the Merge request description.

See complete doc [here](https://sylva-projects.gitlab.io/dev-zone/codebase/cicd/ci-usage).


The status of the scheduled pipelines can be found [here](https://gitlab.com/sylva-projects/sylva-core/-/wikis/Scheduled-pipelines-report).

#### Troubleshooting Failed CI Deployments

If a CI deployment job fails, you can find clues in the job output. Many useful logs are dumped into job artifacts. These artifacts are deleted after 48 hours, but you can download them to analyze and troubleshoot your code after the CI job is over.

#### Adding Tests to Sylva-core Deployments

More complete doc with the list of tests available in [here](https://sylva-projects.gitlab.io/dev-zone/codebase/cicd/ci-tests).

## Contribution Guidelines

To contribute to Sylva CI, pick or create an issue. CI issues are tracked with the `~CI` label.

- [CI Issues](https://gitlab.com/groups/sylva-projects/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=CI&first_page_size=100)
- [CI MRs](https://gitlab.com/groups/sylva-projects/-/merge_requests?scope=all&state=opened&label_name[]=CI)

Additional labels can be used to specify the context of the issue/MR.

For the CI, we aim to build reusable code, limit code duplication, and keep things simple for end users. As the CI code changes frequently, don't hesitate to ask for help on Slack if needed!
