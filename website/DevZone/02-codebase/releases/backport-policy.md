---
title: Backport Policy
---

## Rationale

Once release branches have been created, there is a need for stability in these branches as they start being used in production. 
But there is also the need to fix some issues. The way to solve this is to submit **backports** of the needed fixes after working on them in `main`.  

## Process

Anyone can propose a fix as a candidate for a backport to a release branch.

### Backport candidates

- Security fixes
- Fix for a bug classified as `Medium` or `High` Priority
- Documentation fixes or improvements
- Patch upgrades of dependencies (ie. upgrades for components using x.y.z semver standard) if the upgrades fixes an issue explicitly identified as impacting a Sylva deployment
- CI evolutions

:::important
An evolution introducing a new feature **cannot be** a candidate for a backport.
:::

### Requirements

A backport of a bug fix can only be accepted if the fix for the `main` branch has been merged (unless the bug is specific to the release branch itself).

## What should a backport MR contain ?

A typical backport MR description should be kept very small.

:warning: It is better to **not** use the standard MR template.

**Please remove everything, except the CI configuration block**.

Your MR should look like this:
> Title: [backport-1.3] &lt;title of the MR that was backported&gt;
> 
> Description:
> 
> This MR is a backport of !`<MR that was backported>`.
>
> Closes #`<the issue tracking this backport, see below>`
>
> `Here you can optionally add more explanation if what was done was more than a plain cherry-pick`
>
> ## CI configuration
>
> Below you can choose test deployment variants to run in this MR's CI.
> 
> [...]

If additionally to the cherry-picking more changes were needed, those **need to be explained** in the MR description, for instance:

>  Additionally to cherry-picking, I had to retrofit an extra anvil to flabergast the lorem ipsum startup time. 

Summary check list:
* Title is "[backport-1.x] &lt;title of the MR that was backported&gt;"
* MR description is shortened as much a possible, eg. "This MR is a backport of !xxx. Closes #backport-issue".
* The "CI configuration" block is kept in MR description
* The MR has the `backport` label

Here is an example backport MR: https://gitlab.com/sylva-projects/sylva-core/-/merge_requests/3538

## Methodology

Backports are usually done by cherry-picking a Merge Request that has already been merged into the `main` branch.

We make use of GitLab Issues to track backports.

For an issue for which we'll want to backport fixes, the following will be done:
* the "main issue" where the problem was first reported and discussed will be labelled with `needs-backport-<version>` (e.g. `needs-backport-1.3.x`) to mark the fact that backports are wanted (multiple such labels can be added if backports are needed in more than one release)
* the [Sylva Gitlab Triage bot](https://gitlab.com/sylva-projects/sylva-elements/gitlab-triage/) will then automatically, after a delay, create a "backport issue" in which each of the backport will be individually tracked
* the backport MRs will refer to their matching "backport issue"

:::note
**Example:**
* Main issue: https://gitlab.com/sylva-projects/sylva-core/-/issues/1966
* Its corresponding backport issue: https://gitlab.com/sylva-projects/sylva-core/-/issues/1994
* Backport MR: https://gitlab.com/sylva-projects/sylva-core/-/merge_requests/3538
:::
