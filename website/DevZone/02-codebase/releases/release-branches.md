---
title: Release Branches
---

## Release Branches 

A release branch is a protected GitLab branch where a release is maintained, with in particular the backports of fixes as explained in [this document](/dev-zone/codebase/releases/backport-policy)

Patch releases for a given Sylva based version are tagged from such a branch.

Release branches are named as `release-x.y`.

For instance sylva-core release branch for Sylva 1.3 is `release-1.3` and `1.3.x` tags are created from this branch.

### Release branches status

We started the use of release branches for Sylva 1.3. Previous releases did not have a dedicated branch.

| `sylva-core` release branch | Status        |
| --------------------------- | ------------- |
| `release-1.3`               | Maintained    |

Release branches for `sylva-elements` sub-repositories may have to be created and maintained if fixes are required, but these are not currently created by default.
