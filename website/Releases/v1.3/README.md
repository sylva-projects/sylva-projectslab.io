---
title: Release v1.3.0
sidebar_position: -4
---

import SylvaCoreReleaseURL from '@site/static/js/sylvaCoreReleaseURL.jsx'

:::info
<SylvaCoreReleaseURL tag="1.3.0"></SylvaCoreReleaseURL>
(Also see [Sylva Units](https://gitlab.com/sylva-projects/sylva-core/-/blob/1.3.0/charts/sylva-units/units-description.md?ref_type=tags) )
:::

## Sylva v1.3.0 Overview

Sylva v1.3.0 introduces significant enhancements and updates, including:

- **Kubernetes 1.30 Support**: Full compatibility with Kubernetes 1.30, with the end of support for Kubernetes 1.27.  
- **Longhorn 1.7 for Baremetal Clusters**: Enhanced storage support for baremetal environments.  
- **Logging Stack Default Activation**: The logging stack is now stable and enabled by default.  
- **External Certificate Issuer**: Support for external certificate issuers for Kubernetes endpoints, improving flexibility and security.

This release will be maintained in a specific `release-1.3` branch, ensuring continuous bug fixes and updates through subsequent Sylva v1.3.x tags.


### Key Features

#### X509 Certificate Automation

- Streamlined management of X509 certificates using **ACME** or **Vault** issuers.
- Enhanced security and simplified certificate lifecycle operations.

#### Monitoring & Logging Enhancements

- **Loki Integration**:  
  - Enabled by default, providing powerful log management and analytics capabilities.

#### Storage Improvements

- **Longhorn Update to v1.7.2**:  
  - Improved support for degraded availability scenarios.
  - Reverted problematic upgrades, ensuring stability and performance.
