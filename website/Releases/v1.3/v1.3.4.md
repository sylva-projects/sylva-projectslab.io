---
title: Patch v1.3.4
sidebar_position: -8
---

import SylvaCoreRelease from '@site/static/js/sylvaCoreRelease.jsx';
import SylvaCoreReleaseDescription from '@site/static/js/sylvaCoreReleaseDescription.jsx';
import SylvaCoreReleaseURL from '@site/static/js/sylvaCoreReleaseURL.jsx'

:::info
Access from GitLab for more details on the releases.
<SylvaCoreReleaseURL tag="1.3.4"></SylvaCoreReleaseURL>
:::

# <SylvaCoreRelease tag="1.3.4"></SylvaCoreRelease>

<SylvaCoreReleaseDescription tag="1.3.4"> </SylvaCoreReleaseDescription>
