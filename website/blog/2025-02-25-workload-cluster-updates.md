---
title: Workload Cluster deployment - updates
description: This article will show how workload clusters deployments using GitOps has become simpler
authors: vlabra
tags: [technical, gitops, tutorial]
hide_table_of_contents: false
---

Previously, we explained how to deploy [workload clusters through GitOps](/blog/2024/12/01/workload-cluster-GitOps). Since then, Sylva has been updated to `1.3.x`, and user feedback has helped us improve the Custom Resource Definition (CRD). This article outlines the updates and how they simplify deployment.

<!-- truncate -->

## How to define code base version the per workload cluster

Previously, the Sylva code version was described in the values.yaml at `workload_cluster.sylva_source`, which meant the version was the same for all workload clusters. 

Starting with Sylva `1.3.x`, the version defined in the values.yaml is only the default one. It is merged with the values defined in the SylvaWorkloadCluster manifest at `spec.sylvaSourceVersion` which is now a required field. Hence, each workload cluster can now have its own version.

For instance, let the values.yaml and the SylvaWorkloadCluster manifest be as follows

```yaml
---
workload_clusters:
  sylva_source:
    type: git
    url: https://gitlab.com/sylva-projects/sylva-core.git
    tag: 1.2.5
```

```yaml
---
apiVersion: workloadclusteroperator.sylva/v1alpha1
kind: SylvaWorkloadCluster
spec:
  sylvaSourceVersion:
    tag: 1.3.0
```

Then the final result is

```yaml
---
apiVersion: helm.toolkit.fluxcd.io/v2
kind: HelmRelease
spec:
  values:
    source_templates:
      sylva-core:
        kind: GitRepository
        spec:
          url: https://gitlab.com/sylva-projects/sylva-core.git
          ref:
            tag: 1.3.0
```

## How to pass generic values to a workload cluster

Previously, the SylvaWorkloadCluster resource only enabled users to pass free form values in `clusterSettings`. These values are mapped into the sylva-units Helm release's `values.cluster` field.

However, parameters at the same level as `cluster` may be required, like the `sylva_diskimagebuilder_images` or `security.os_imagesskip_signing_check`. This is solved with a new parameter in the CRD, called `overridingValues`. In details:

- Free form object can be passed to `overridingValues`.
- `overridingValues` is merged into the `values` of the Sylva unit Helm release.
- For security reasons, `overridingValues` can not contain go-like templating with `{{ ... }}`.
- For consistency, fields defined in the CRD like k8sVersion, or clusterSettings can not be overridden.

## Complete example
Assuming OpenStack secrets have been added into the Vault in `teama/common/capo`, the following manifest is deploying a workload cluster

- Based on Sylva 1.3.0 code
- On an OpenStack cluster, with RKE2 control plane provider
- It supports insecure access to registry
- OS Versions are from the [disk image builder repo](https://gitlab.com/sylva-projects/sylva-elements/diskimage-builder/container_registry/8298520). In future Sylva version, these versions and tags may be automatically computed

```yaml
---
apiVersion: workloadclusteroperator.sylva/v1alpha1
kind: SylvaWorkloadCluster
metadata:
  name: wc1
  namespace: teama
spec:
  k8sVersion: "1.30"
  sylvaSourceVersion:
    tag: 1.3.0
  infrastructure: openstack
  controlPlane:
    provider: rke2
    replicas: 1
  machineDeployments:
    md1:
      replicas: 1
  secretValues:
    path: common
    key: capo
  overridingValues:
    sylva_diskimagebuilder_version: 0.3.11
    sylva_diskimagebuilder_images:
      ubuntu-jammy-hardened-rke2-1-30-9:
        enabled: true
    security:
      os_images:
        skip_signing_check: true
  clusterSettings:
    capo:
      resources_tag: rke2-capo
      ssh_key_name: foo
      network_id: bar
      image_key: ubuntu-jammy-hardened-rke2-1-30-9
```

---

## Conclusion

The new CRD fields enable users to deploy more granularly and easily workload clusters.
