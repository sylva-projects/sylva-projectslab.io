---
title: Workload Cluster deployment using GitOps
description: This article will introduce the tech preview on how to deploy workload clusters using GitOps
authors: prigoy
tags: [technical, gitops, tutorial]
hide_table_of_contents: false
---

Imagine an infrastructure that always runs exactly as planned, without surprises or drift.

**This is the promise of GitOps**.

Gitops has transformed modern DevOps by treating Git as the single source of truth. By storing the desired system state in a Git repository, GitOps enables tools like FluxCD to monitor and apply changes automatically. This powerful automation ensures that live systems stay in sync with the desired state, removing the need for manual updates and allowing for quick recovery by simply reverting a Git commit.

<!-- truncate -->

## GitOps: The Power of Automation and Safety

GitOps makes infrastructure and application management automated, consistent, and auditable. With every change tracked in Git, teams gain the flexibility to roll back configurations instantly if issues arise. This approach provides a reliable, efficient workflow where desired state changes are applied without manual intervention, ensuring systems remain in sync with source-controlled configurations.

![gitOps](./img/gitops.png)

### Key Benefits of GitOps

- **Automation**: Desired state changes are automatically applied, reducing the need for manual intervention.
- **Consistency**: Systems remain in sync with source-controlled configurations, ensuring uniformity across environments.
- **Auditability**: Every change is tracked in Git, providing a clear history and the ability to roll back if necessary.
- **Security**:
    - Users only need access to Git, not the Kubernetes API.
    - Kubernetes pulls configurations from external repositories, enhancing security by avoiding direct flows from external networks to the API.



## Traditional Management: Manual, Script-Driven, and Error-Prone

In a non-GitOps setup, managing clusters requires manual, script-driven processes, which are often cumbersome and prone to inconsistency. Users check out the Sylva core repository on local machines, make changes directly, and use custom scripts to apply them. Configuration values passed through environment variables add complexity, making it difficult to track and reproduce setups. Without automated synchronization, the cluster’s actual state may frequently differ from what’s defined in Git, lacking the consistency and reliability that GitOps ensures.

![apply-workload](./img/apply-workload.png)

## The GitOps Approach: Seamless Workload Cluster Management

In a GitOps-driven approach, workload cluster users define their desired state in the SylvaWorkloadCluster manifests, which are then committed to Git. FluxCD monitors for any changes in Git, automatically syncing the management cluster with the specified state. Operators provision resources based on these configurations, from namespace creation to resource deployment, all triggered by changes committed to Git. This approach automates the entire workflow, reducing manual steps, minimizing errors, and ensuring the system reflects the desired configuration in real time.

![gitops-workload](./img/gitops-workload.png)

## Diving in the Deployment Process

Now that you have a broader understanding of GitOps benefits in our Sylva's context, let's show you a workload cluster deployment journey using GitOps.

:::important
**Prerequisites**
- Have access to a management cluster, refer to the [installation documentation](/docs/category/installation) for more information.
:::

---

### Step 1: Accessing Vault


- Login to [Vault](/design/core-concepts/security/#vault)

---

### Step 2: Set Up Credentials in Vault

#### 2.1 Store Git Repository Access Secret (Only for private git repo)

- For a private Git Repository holding the SylvaWorkloadCluster manifest, store the access secret in Vault.
- This credential will be referenced in the `values.yaml` file via the `git_secret_path`.
- Example: Secret stored under `team-a/gitsecret`.

:::note
The names of the secrets belonging to a team should be prefixed by the team name.
:::

![gitsecret](./img/gitsecret.png)

#### 2.2 Store OpenStack Credentials for CAPO Deployment

- Add OpenStack credentials to Vault under the path `team-a/wc/capo`.
- Refer to these credentials in the SylvaWorkloadCluster manifest under `secretValues`.

Example JSON:
```json
{
  "cluster": {
    "capo": {
      "clouds_yaml": {
        "clouds": {
          "capo_cloud": {
            "auth": {
              "auth_url": "https://xx.yy.zz.aa/v3",
              "password": "dummypwd",
              "project_name": "baz.qux",
              "project_id": "123456789d",
              "user_domain_name": "Default",
              "project_domain_name": "Default",
              "username": "foo.bar"
            },
            "identity_api_version": 3,
            "interface": "public",
            "region_name": "Nova"
          }
        }
      }
    }
  }
}
```

![infrasecret](./img/infrasecret.png)

---

### Step 3: Sample SylvaWorkloadCluster Manifest

Store the SylvaWorkloadCluster manifest in a version control platform like GitLab or GitHub, and update values.yaml at `workload_clusters.teams.<team-name>.gitrepository_spec.url` with the Git URL containing the SylvaWorkloadCluster manifest as mentioned in Step 4.


```yaml
kind: SylvaWorkloadCluster
apiVersion: workloadclusteroperator.sylva/v1alpha1
metadata:
  name: wc1
  namespace: team-a
spec:
  k8sVersion: "1.29"
  infrastructure: openstack
  controlPlane:
    provider: rke2
    replicas: 1
  machineDeployments:
    my-md1-name:
      replicas: 2
  clusterSettings:
    capo:
      ssh_key_name: my_ssh_key  # replace me
      image_key: ubuntu-jammy-hardened-rke2-1-29-9  # only default image is supported
      network_id: ##################  # replace me
      flavor_name: "B1.xlarge"
      rootVolume: 50
      volumeType: ceph_ssd
  secretValues:
    path: wc
    key: capo
```

---

### Step 4: Update Environment Values

Edit the management cluster `values.yaml` file to include workload cluster details (.workload_clusters). Below is an example structure:

- Update `workload_clusters.sylva_source` to include the sylva-core repo details

- Workload clusters are organized into teams, each defined by a shared Git repository and common secrets used across all workload clusters in that repository. Dedicated namespaces are created for each team using `workload_clusters.teams.<team-name>`, where resources related to workload clusters are managed. These resources include Flux components like GitRepository and Kustomization, which point to the SylvaWorkloadCluster manifest and synchronize workload cluster configurations from the Git repository to the management cluster.


```yaml
workload_clusters:
  sylva_source:
    type: git
    url: https://gitlab.com/sylva-projects/sylva-core.git
    tag: <desired_tag>
  teams:
    team-a:
      kustomization_spec:
        interval: 15m
      gitrepository_spec:
        url: https://gitlab.com/example/private_repo1
        ref:
          branch: main
        interval: 120m
      gitrepository_path: "manifests/team-a"
      git_secret_path: gitsecret
```

#### Key Considerations

- **Sylva Source**:
  - If type is git, you must specify one of tag, commit, or branch.
  - For oci type, ensure tag is provided.
- **Teams Configuration**:
  - Set `gitrepository_spec.url` to the Git repository containing the SylvaWorkloadCluster manifest.
  - The `gitrepository_path` should point to the precise location of the manifest.

---

### Step 5: Apply Configuration

Update the management cluster with:

```bash
./apply.sh <your-management-cluster-env>
```

This will:

- Create team-specific namespaces and associated resources.
- Deploy Flux resources (GitRepository and Kustomization) pointing to the SylvaWorkloadCluster manifest.

---

### Step 6: Automated Workload Cluster Deployment

Once the Flux resources are in place, the SylvaWorkloadCluster custom resource is applied to the management cluster. Sylva’s operators and controllers will then deploy the workload cluster by creating a corresponding HelmRelease.

---

### Step 7: Fasten the Reconciliation

Force reconcile the workload cluster teams:

```bash
flux reconcile helmrelease workload-team-defs --with-source
```

Force reconcile a team Git repo:

```bash
flux reconcile -n team-a kustomization --with-source team-a
```

---

## Conclusion

Switching to GitOps for workload cluster management replaces traditional, manual methods with a streamlined, automated workflow. With Git as the single source of truth, workload cluster users and admins gain a powerful, reliable process that ensures consistency, improves traceability, and minimizes errors. Embrace GitOps, and let automation drive your clusters with confidence and precision.
