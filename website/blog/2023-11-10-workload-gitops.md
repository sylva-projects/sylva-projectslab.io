---
title: Presentation for Workload Cluster GitOps
description: Presentation
authors: prigoy
tags: [meeting, presentation, ppt]
hide_table_of_contents: false
---

Workload cluster Gitops workflow slides  

[download](./assets/workload-clusterGitops.pptx)
