---
title: LonghornS
---

# Longhorn Procedures

This document provides step-by-step instructions for configuring and managing Longhorn disks in Sylva.

## Enabling Longhorn

To use Longhorn in Sylva, ensure the following is set in your environment values:

```yaml
cluster:
  enable_longhorn: true
```

:::warning
Support for the `node.longhorn.io/create-default-disk: true` node label has been dropped. It is now mandatory to use dedicated disks for Longhorn. The OS disk cannot be used.
:::

## Defining Baremetal Node Drives as Longhorn Disks

These values follow the format defined by [Longhorn default disks and node configuration](https://github.com/longhorn/longhorn/blob/master/enhancements/20200319-default-disks-and-node-configuration.md#design).

### Using `longhorn_disk_config`

:::info
Defined longhorn disks will result in mounting each /dev/x disk at /var/longhorn/disks/x
:::

Edit your `values.yaml` file to include the `longhorn_disk_config` property for the desired baremetal host:

    ```yaml
    baremetal_hosts:
      my-bmh-foo:
        longhorn_disk_config:
          - path: "/var/longhorn/disks/disk_by-path_pci-0000:00:0a.0"
            storageReserved: 0
            allowScheduling: true
            tags:
              - vhdd
              - fast
          - path: "/var/longhorn/disks/disk_by-path_pci-0000:00:0b.0"
            storageReserved: 0
            allowScheduling: true
            tags:
              - vhdd
              - fast
    ```

#### Allowed Properties

The properties of `baremetal_hosts.x.longhorn_disk_config` or `baremetal_hosts.x.bmh_metadata.annotations` are defined in [this file](https://github.com/longhorn/longhorn-manager/blob/88c792f7df38383634c2c8401f96d999385458c1/k8s/pkg/apis/longhorn/v1beta2/node.go#L60-L74).

```sh
# Check longhorn documentation if needed
"path"
"storageReserved"
"allowScheduling"
"tags"
"diskDriver" with values - ""|auto|aio
"diskType" with values - filesystem|block
"evictionRequested"
```

The mandatory ones for having Longhorn schedule PVs on a disk are:

- `"path": "/var/longhorn/disks/x"`: The mount point of the disk x, recommended to be referenced by PCI addresses. It **needs to start with `/var/longhorn/disks/`**.
- `"allowScheduling": true`: Enables replica scheduling for the disk. The default value is `false`, so it is crucial to include this parameter. **For this reason, it's very important to use the `"allowScheduling"` inside the `longhorn_disk_config` or `annotation` value, as this parameter would indicate to Longhorn manager whether the user enabled/disabled replica scheduling for the particular disk.**

#### Allowed Patterns

```sh
/var/longhorn/disks/(sd|vd)[a-z]
/var/longhorn/disks/nvme[0-9]n[0-9]
/var/longhorn/disks/disk_by-path_pci-*
/var/longhorn/disks/disk_by-uuid_*
```

### Using Annotations (Deprecated)

Edit your `values.yaml` file to include the `sylvaproject.org/default-longhorn-disks-config` annotation for the desired baremetal host:

    ```yaml
    baremetal_hosts:
      my-bmh-foo:
        bmh_metadata:
          annotations:
            sylvaproject.org/default-longhorn-disks-config: '[ { "path":"/var/longhorn/disks/disk_by-path_pci-0000:00:0a.0", "storageReserved":0, "allowScheduling":true, "tags":[ "vhdd", "fast" ] }, { "path":"/var/longhorn/disks/disk_by-path_pci-0000:00:0b.0", "storageReserved":0, "allowScheduling":true, "tags":[ "vhdd", "fast" ] } ]'
    ```

:::note
- **Annotations properties are obsolete, supported only for proper migration to longhorn_disk_config**
- Currently, you can use `longhorn_disk_config` or `annotations` to define Longhorn disks.
- If both are provided, then the content of both the properties must be the same as this check is ensured in the code itself which will cause failure later if the contents of both properties are different.
- Follow the migration steps to adapt to use the newer way of defining longhorn disk config.
:::

#### Annotations Migration steps

1. Add the `longhorn_disk_config` property. [[example]](#using-longhorn_disk_config)
2. Apply and check that it is accepted (and equal to the annotation)
3. Remove the annotation
4. Apply


### Checking Mounted Disks

SSH into the node and run the following command to check the mounted disks:

```shell
lsblk
```

Example output:

```shell
NAME MAJ:MIN RM SIZE RO TYPE MOUNTPOINTS
vda 252:0 0 50G 0 disk
├─vda1
│ 252:1 0 550M 0 part /boot/efi
├─vda2
│ 252:2 0 8M 0 part
├─vda3
│ 252:3 0 49.4G 0 part /
└─vda4
  252:4 0 65M 0 part
vdb 252:16 0 50G 0 disk
vdc 252:32 0 50G 0 disk /var/longhorn/disks/disk_by-path_pci-0000:18:00.0-scsi-0:3:111:0
```

### Checking PVs Mounted on the Node

Run the following command to check the PVs mounted on the node:

```shell
kubectl get node <node-name> -o yaml | yq .status.volumesInUse
```

Example output:

```shell
- kubernetes.io/csi/driver.longhorn.io^pvc-17e5863f-4d15-419c-bf20-48e2547d64fc
- kubernetes.io/csi/driver.longhorn.io^pvc-1b91ddb0-15a4-4c50-af90-c33280e7cdfa
- kubernetes.io/csi/driver.longhorn.io^pvc-1cb0e490-d030-42ea-a16f-60a9b19f1e3a
- kubernetes.io/csi/driver.longhorn.io^pvc-1dc68632-3587-48a5-b481-d0f5f5b5dd43
- kubernetes.io/csi/driver.longhorn.io^pvc-68dc4c38-ecef-48d2-a02b-fe69d4ff1e26
- kubernetes.io/csi/driver.longhorn.io^pvc-7f0ec91b-dec6-4f53-a277-4d6c62279760
- kubernetes.io/csi/driver.longhorn.io^pvc-9c103ae6-c68f-42a2-8e88-dfe41f6c4c25
- kubernetes.io/csi/driver.longhorn.io^pvc-c3095a2c-ba1f-49a7-8796-7dc7dfc5f2de
```
