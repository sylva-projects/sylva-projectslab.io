---
title: NFS Ganesha
---

<span class="badge badge--primary">version: 1.2.0</span>

## Overview

NFS-Ganesha enables persistent volumes with `ReadWriteMany` (RWX) access mode for Sylva clusters, especially when the default storage backend does not support RWX volumes. It dynamically provisions volumes using a backend storage class and mounts them via an NFS provisioner.

In OpenStack-based Sylva clusters, the default storage backend is Cinder, which only supports `ReadWriteOnce` (RWO). To enable RWX volumes, NFS-Ganesha acts as an intermediary, using Cinder for storage while exposing an NFS interface.

## Enabling NFS-Ganesha

By default, NFS-Ganesha is disabled. To enable it, add the following configuration to your environment settings:

```yaml
units:
  nfs-ganesha:
    enabled: true
```

This automatically creates a **5GB PVC** (`nfs-ganesha-data-pvc`) using the cluster's default storage class. If needed, you can modify the default PVC size before enabling NFS-Ganesha:

```yaml
units:
  nfs-ganesha:
    enabled: true
  nfs-ganesha-init:
    kustomization_spec:
      postBuild:
        substitute:
          storage_size: 25Gi  # New PVC size
```

To specify a custom storage class for `nfs-ganesha-data-pvc`, use:

```yaml
units:
  nfs-ganesha:
    enabled: true
  nfs-ganesha-init:
    kustomization_spec:
      postBuild:
        substitute:
          storage_class: <storage_class_name>  # Enter the new storage class name
```

## Increasing NFS-Ganesha PVC Size

If the default **5GB** size is insufficient, follow these steps to increase it.

### 1. Update the environment settings

Modify your configuration to set the desired PVC size:

```yaml
units:
  nfs-ganesha-init:
    kustomization_spec:
      postBuild:
        substitute:
          storage_size: 25Gi  # Updated PVC size
```

### 2. Apply the changes

Run the following command on the bootstrap VM:

```shell
./apply.sh environment-values/my-rke2-capo/  # Management cluster
./apply-workload-cluster.sh environment-values/my-rke2-capo/  # Workload cluster
```

### 3. Verify NFS-Ganesha status

Check the stateful set:

```shell
kubectl get statefulsets -n nfs-ganesha
```

You should see:

```
NAME                                 READY   AGE
nfs-ganesha-nfs-server-provisioner   1/1     3d19h
```

### 4. Restart the NFS-Ganesha stateful set

```shell
kubectl scale statefulsets nfs-ganesha-nfs-server-provisioner -n nfs-ganesha --replicas=0
kubectl scale statefulsets nfs-ganesha-nfs-server-provisioner -n nfs-ganesha --replicas=1
```

### 5. Confirm the new PVC size

```shell
kubectl get pvc -n nfs-ganesha
```

Expected output:

```
NAME                   STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS
nfs-ganesha-data-pvc   Bound    pvc-17e7e724-b480-4d46-80d5-521eac52661e   25Gi       RWO            cinder-ceph-ssd
```

### 6. Find and access the provisioner node

Find the node hosting the `nfs-server-provisioner` pod:

```shell
kubectl get po -n nfs-ganesha -o wide nfs-ganesha-nfs-server-provisioner-0
```

Output:

```
NAME                                   READY   STATUS    RESTARTS   AGE   IP              NODE
nfs-ganesha-nfs-server-provisioner-0   1/1     Running   0          41s   1xx.xx.xx.xx   management-cluster-cp-18759f25fc-j6kh7
```

SSH into the node:

```shell
ssh -i key node-admin@xx.xx.xx.75
```

### 7. Resize the filesystem

Find the mounted PVC:

```shell
mount | grep pvc-17e7e724-b480-4d46-80d5-521eac52661e
```

Resize the filesystem:

```shell
resize2fs /dev/vdc
```

Verify the new size:

```shell
df -h /dev/vdc
```

Expected output:

```
Filesystem      Size  Used Avail Use% Mounted on
/dev/vdc         25G   88K   25G   1% /var/lib/kubelet/pods/.../volumes/kubernetes.io~csi/pvc-17e7e724-b480-4d46-80d5-521eac52661e/mount
```
