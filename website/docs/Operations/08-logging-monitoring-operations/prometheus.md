---
title: Prometheus
---

Prometheus alerting rules are crucial for proactive monitoring and incident response. They enable operations teams to receive notifications about potential issues before they impact users. It's important to tailor these rules to the specific needs of your environment to avoid alert fatigue and ensure that critical alerts are actionable and relevant.

## Alerting Rules

:::info
Additional rules can be provisioned in Prometheus.
:::

The `sylva-prometheus-rules` unit is responsible for managing the Prometheus rules, which are maintained in the [`sylva-prometheus-rules` GitRepository](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-prometheus-rules).

### Customizing Rules

To customize the alerting rules:
- Fork or clone the `sylva-prometheus-rules` repository.
- In your environment values modify the `.source_templates.sylva-prometheus-rules` 'GitRepository' resource URL/branch as needed.

```yaml
source_templates:
  sylva-prometheus-rules:
    spec:
      url: https://example.com/custom-prometheus-rules.git
      ref:
        tag: null # do not use a tag
        branch: platform1 # use a specific branch
```

### Default Behavior

By default, the `sylva-prometheus-rules` unit creates rules for `allclusters` and the specific cluster on which it is run. This behavior can be modified:

```yaml
units:
  sylva-prometheus-rules:
    helmrelease_spec:
      values:
        createRules:
          allclusters: false
          '{{ .Values.cluster.name }}': true # change this to use a static cluster name
```

If `{{ .Values.cluster.name }}` does not exist in the Git repository (under the `alert-rules` directory), it will need to be created or the name overridden in `values.yaml`.

### Rule Generation

The `sylva-prometheus-rules` unit generates `PrometheusRule` objects from every file under the specified directories. If `.Values.createRules.allclusters` is set to `true` (default), then the `allclusters/*yaml` rules are parsed last, regardless of other specified clusters.

:::info
This setup allows for rule overriding.
:::

### Example Configuration

Consider the following configuration:

```yaml
createRules:
  allclusters: true
  management-cluster: true
```

And the corresponding rule files:

```yaml
alert-rules/allclusters/health_alerts.yaml
alert-rules/allclusters/dummy.yaml

alert-rules/management-cluster/dell_idrac.yaml
alert-rules/management-cluster/health_alerts.yaml
alert-rules/management-cluster/hp_cpq.yaml
```

- Initially, the `PrometheusRule` with the names *dell-idrac*, *hp-cpq*, and *health-alerts* from `management-cluster` are created.
- Subsequently, *health-alerts* and *dummy* from `allclusters` are parsed. Since *health-alerts* is already applied from `management-cluster`, it will **not** be applied again. *dummy* will be applied as it does not override anything.

This configuration effectively allows users to override the *health-alerts* from `allclusters` with those from `management-cluster`.
