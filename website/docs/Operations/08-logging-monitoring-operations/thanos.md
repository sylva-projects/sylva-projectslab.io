---
title: Thanos
---

## Alerting Rules

:::info
Rules can be provisioned in Thanos.
:::

The Thanos Ruler component evaluates rules similarly to Prometheus.

The `sylva-thanos-rules` unit is responsible for managing the Thanos rules, which are maintained in the [`sylva-thanos-rules` GitRepository](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-thanos-rules).

### Customizing Rules

To customize the alerting rules:

- Fork or clone the `sylva-thanos-rules` repository.
- In your environment values modify the `.source_templates.sylva-thanos-rules` 'GitRepository' resource URL/branch as needed.


```yaml
source_templates:
  sylva-thanos-rules:
    spec:
      url: https://example.com/custom-thanos-rules.git
      ref:
        tag: null # do not use a tag
        branch: platform1 # use a specific branch
```
