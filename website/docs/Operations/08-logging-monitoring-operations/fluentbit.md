---
title: Fluent Bit
---

Fluent Bit is a lightweight log processor and forwarder, ideal for environments where resource usage needs to be minimized. It can forward logs to Fluentd or directly to other logging backends like Loki.

## Default Behavior

Fluentbit's default behaviour is to scrape the logs from `/var/log/containers/*.log` and forward them to fluentd .

## Custom Behavior

:::info
Additional configurations can be provisioned in Fluent Bit.
:::

The `logging` unit is responsible for managing the Fluentbit (and Fluentd) configurations.

### Configuration Template

To customize the Fluentd configurations:
- Fork or clone the `sylva-core` repository.
- Modify the configurations in `environment-values/${my-env}/values.yaml` :

```yaml
units:
    logging:
        helmrelease_spec:
            values:
             fluentbit: {...}
```
