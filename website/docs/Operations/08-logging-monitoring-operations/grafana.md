---
title: Grafana
---

Grafana dashboards are essential tools for visualizing real-time data and metrics. They provide valuable insights that help in monitoring the health and performance of applications and infrastructure. Customizing dashboards to reflect the specific needs and priorities of your environment can enhance monitoring effectiveness and aid in quicker decision-making.

## Dashboards

:::info
Additional dashboards can only be provisioned in the management cluster Grafana.
:::

The `sylva-dashboards` unit is responsible for managing the Grafana dashboards, which are stored in the [`sylva-dashboards` GitRepository](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-dashboards).

### Customizing

To customize the dashboards:
- Fork or clone the `sylva-dashboards` repository.
- In your environment values modify the `.source_templates.sylva-dashboards` 'GitRepository' resource URL/branch as needed.

```yaml
source_templates:
  sylva-dashboards:
    spec:
      url: https://example.com/custom-dashboards.git
      ref:
        tag: null # do not use a tag
        branch: platform1 # use a specific branch
```

### Provisioning

All `.json` files under the `dashboards` directory will be provisioned to Grafana. This allows for a flexible approach to dashboard management, enabling teams to rapidly deploy and update dashboards as required.
