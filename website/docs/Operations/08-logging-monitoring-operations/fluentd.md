---
title: Fluentd
---

Fluentd plays a pivotal role in comprehensive log collection and aggregation, enabling operations teams to gather logs from various sources, parse them, and forward them to multiple destinations for analysis and storage.

## Default Behavior

The default configuration of Fluentd is set to forward logs from all sources to a Loki backend. It also includes settings for buffering and error-handling behaviors to ensure efficient log management.

## Custom Behavior

:::info
Additional configurations can be provisioned in Fluentd.
:::

The `logging` unit is responsible for managing the Fluentd (and Fluentbit) configurations.

### Configuration Template

To customize Fluentd configurations:
- Fork or clone the `sylva-core` repository.
- Modify the configurations in `environment-values/${my-env}/values.yaml` :

```yaml
units:
    logging:
        helmrelease_spec:
            values:
             fluentd: {...}
```

### Configuration Examples

Below are examples of various Fluentd configurations that demonstrate how to set up sources, define routing rules, manage labels, configure buffering, and handle errors.

- [Source Configuration](#source-configuration)
- [Match and Routing Rules](#match-and-routing-rules)
- [Defining Labels](#defining-labels)
- [Buffer Configuration](#buffer-configuration)
- [Error Handling](#error-handling)

#### Source Configuration

The `Source` section configures the Fluentd source that listens for incoming log data.

```shell
<source>
  @type forward
  @id main_forward
  bind 0.0.0.0
  port 24240
</source>
```

#### Match and Routing Rules

This section shows how to define the rules for matching and routing log entries. Logs that match these rules are directed to specific labels for further processing. Here, it routes all logs to a designated label for additional handling.

```shell
<match **>
  @type label_router
  @id main
  metrics false
  <route>
    @label @978a53320aee9ea1efe62bfdf66c2e0f
    metrics_labels {"id":"clusterflow:cattle-logging-system:all-logs"}
    <match>
      namespaces
      negate false
    </match>
  </route>
</match>
```

#### Defining Labels

Specified labels are used for routing logs. It includes detailed configuration for sending logs to Loki, including authentication, metadata extraction, and buffering.


```shell
<label @978a53320aee9ea1efe62bfdf66c2e0f>
  <match **>
    @type loki
    @id clusterflow:cattle-logging-system:all-logs:clusteroutput:cattle-logging-system:loki
    extra_labels {"job":"management-cluster-job"}
    extract_kubernetes_labels true
    include_thread_label true
    insecure_tls true
    line_format json
    password xxxxx
    remove_keys ["kubernetes"]
    tenant management-cluster
    url http://loki-gateway.loki.svc.cluster.local
    username loki-user
    <label>
      container $.kubernetes.container_name
      container_id $.kubernetes.docker_id
      host $.kubernetes.host
      namespace $.kubernetes.namespace_name
      pod $.kubernetes.pod_name
      pod_id $.kubernetes.pod_id
    </label>
  </match>
</label>
```

#### Buffer Configuration

Buffering configures how logs are buffered before being sent to the destination. It specifies file-based buffering with a size limit and retry policies to ensure logs are reliably sent to Loki.


```shell
<buffer tag,time>
    @type file
    chunk_limit_size 8MB
    path /buffers/clusterflow:cattle-logging-system:all-logs:clusteroutput:cattle-logging-system:loki.*.buffer
    retry_forever true
    timekey 10m
    timekey_wait 1m
</buffer>
```

#### Error Handling

This section shows how Fluentd manages errors. In this setup, all errors are discarded by directing them to a null output, effectively ignoring them.

```shell
<label @ERROR>
  <match **>
    @type null
    @id main-fluentd-error
  </match>
</label>
```
