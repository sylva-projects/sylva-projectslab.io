---
title: Alertmanager
---

Alertmanager is a component of the Prometheus monitoring system that handles alerts sent by the Prometheus server and deduplicates, groups, and routes them to the correct receiver integrations such as email, PagerDuty, or OpsGenie. It also takes care of silencing and inhibition of alerts. Alertmanager provides a reliable way to handle notifications and alerts from a Prometheus server, ensuring that the right people are notified at the right time about operational issues.

### Configuration

The Alertmanager configuration is located under `monitoring.alertmanager.config`

It can contain a complete Alertmanager configuration or a partial one that will be merged on top of the default Rancher configuration - available [here](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-alertmanager-resources/-/blob/main/config/values.yaml).

```yaml
monitoring:
  alertmanager:
    config:
      global:
        # ResolveTimeout is the time after which an alert is declared resolved
        # if it has not been updated.
        resolve_timeout: 5m
      # The root route on which each incoming alert enters.
      route:
        group_by: [ alertname, severity, cluster_name, platform_tag ]
        group_wait: 3m
        # When the first notification was sent, wait 'group_interval' to send a batch
        # of new alerts that started firing for that group.
        group_interval: 15m
        # Renotify every 3h
        repeat_interval: 3h
        receiver: all_alarms
        routes:
        - matchers:
          - severity=~.*
          receiver: all_alarms
          continue: true
        - matchers:
          - severity=~^(critical|warning)$
          receiver: jira_receiver
          continue: true
        - matchers:
          - severity=info
          receiver: email_receiver
          continue: true
      inhibit_rules:
      # Apply inhibition if the alertname is the same on the same cluster
      - source_matchers:
        - severity=critical
        target_matchers:
        - severity=~^(error|warning|info)$
        equal: [ alertname, cluster_name ]
      - source_matchers:
        - severity=error
        target_matchers:
        - severity=~^(warning|info)$
        equal: [ alertname, cluster_name ]
      - source_matchers:
        - severity=warning
        target_matchers:
        - severity=info
        equal: [ alertname, cluster_name ]
      receivers:
      # no other configuration for "all_alarms"
      # means it's an empty receiver so no forwarding is done
      # but it can be used for filtering in the UI/API
      - name: all_alarms
      - name: jira_receiver
        webhook_configs:
        - url: http://alertmanager-jiralert:9097/alert
          send_resolved: false  
      - name: email_receiver
        email_configs:
        - to: alerts@example.com, extra_alerts@example.com
          from: alert_sender@example.com
          smarthost: mail.example.com:25
          send_resolved: true
          require_tls: false
```

For more details on the available features and how to configure them, please refer to the [Alertmanager documentation](https://prometheus.io/docs/alerting/latest/configuration/).

In order to validate that the alerts are routed correctly the [Routing tree editor](https://www.prometheus.io/webtools/alerting/routing-tree-editor/) tool can be used.

## Webhooks

The webhook receiver allows configuring a generic receiver.

### JIRAlert

JIRAlert provides a robust integration between Alertmanager and JIRA, enabling the automated creation of JIRA tickets based on alerts, which can significantly streamline incident management processes.

JIRAlert connects to one or more JIRA instances to create highly configurable JIRA issues, implementing Alertmanager's webhook HTTP API.

### Configuration

The configuration file for JIRAlert is essentially a list of receivers that match 1-to-1 with all Alertmanager receivers using JIRAlert, plus defaults (in the form of a partially defined receiver). The JIRAlert configuration is located under `monitoring.alertmanager.webhooks.jiralert`.

```yaml
monitoring:
  alertmanager:
    webhooks:
      jiralert:
        # If missing, the proxies are those configured in `.Values.proxies`
        # Use this if access to JIRA requires different proxies
        env:
          https_proxy: https://example.com:3128
          http_proxy: https://example.com:3128
          no_proxy: 127.0.0.1,jira.example.com
        config:
          # Both `defaults` and `receivers` are required
          defaults:
            # API access fields
            api_url: https://example.com/jira
            user: user
            password: password
            # Alternatively to user and password, use a Personal Access Token
            # personal_access_token: "Your Personal Access Token"
            # See https://confluence.atlassian.com/enterprise/using-personal-access-tokens-1026032365.html
          receivers:
            # At least one receiver must be defined
            # Must match the Alertmanager receiver name. Required.
            - name: jira_receiver
              project: Sylva
```

For more details on the available features and how to interact with these components, please refer to the [JIRAlert chart](https://github.com/prometheus-community/helm-charts/tree/main/charts/jiralert), specifically the `config` key.
