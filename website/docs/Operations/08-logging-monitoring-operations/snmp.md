---
title: SNMP
---

Simple Network Management Protocol (SNMP) is a widely used protocol for monitoring the health and welfare of network equipment, computer equipment, and even devices like UPSs. Implementing SNMP solutions allows network administrators to manage network performance, find and solve network problems, and plan for network growth. In the context of the Sylva project, SNMP is utilized to enhance the monitoring capabilities of network devices and servers by providing critical operational metrics that aid in proactive management and troubleshooting.

## Configuration

Machines specified in `rke2-capm3-virt.cluster.baremetal_hosts` with the label `bmh_metadata.labels.snmp-enabled: true` are automatically added to the monitored devices list. 
- The annotations `sylvaproject.org/snmp-auth` and `sylvaproject.org/snmp-hw-type` define the required authentication and SNMP modules for the `snmp-exporter`. 
- The annotation `sylvaproject.org/snmp-endpoint` specifies the target address.

```yaml
management-cp-0:
  bmh_metadata:
    labels:
      cluster-role: control-plane
      snmp-enabled: "true"
    annotations:
      sylvaproject.org/snmp-auth: foo
      sylvaproject.org/snmp-hw-type: dell_idrac
      sylvaproject.org/snmp-endpoint: 192.168.133.1
```

Additional SNMP targets can be defined in the environment values file:

```yaml
snmp:
  devices:
    - alias: Server1
      target: 1.2.3.4
      module: dell_idrac
      auth: snmpv3
      cluster_name: 'Client1'
    - alias: Server2
      target: 2.3.4.5
      module: hp_cpq
      auth: snmpv2
      cluster_name: 'Client2'
  auth:
    snmpv3:
      version: 3
      security_level: authPriv
      username: snmp
      password: xxxxx
      auth_protocol: SHA256
      priv_protocol: AES
      priv_password: xxxxx
    snmpv2:
      version: 2
      community: public
```

The `devices` key in the `snmp` dictionary is a **list** containing:

| Field | Content |
| ----- | ------- |
| alias | Server's name |
| target | Address of the BMC (iDrac, iLo, etc) |
| module | `snmp-exporter` module used to monitor the target |
| auth | `snmp-exporter` auth group containing the credentials for this target |
| cluster_name | Which cluster is this target member of |

The `auth` key in the `snmp` dictionary is a **dictionary** specifying authentication parameters for each auth name:

| Field | Content               | Possible values |
| ----- | --------------------- | --------------- |
| version | SNMP version | 2 \| 3 |
| community | SNMP community | |
| security_level | | noAuthNoPriv \| authNoPriv \| authPriv |
| username | | |
| password | | |
| auth_protocol | | MD5 \| SHA \| SHA224 \| SHA256 \| SHA384 \| SHA512 |
| priv_protocol | | AES \| AES192 \| AES256 |
| priv_password | | |

## Adding New Supported Hardware

To support additional hardware types, apart from modifying the validation schema in `snmp.values.[].module`, the module's SNMP-specific configuration for `snmp-exporter` must be added [here](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-snmp-resources/-/tree/main/files).

The name of the file should match the module name as it is used to build the final `snmp-exporter` configuration containing the `modules` and `auth` sections. This ensures that the SNMP monitoring is accurately tailored to the specific hardware being used.
