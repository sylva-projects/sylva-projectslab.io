---
title: Loki
---

## Enabling unit

Loki is disabled by default. To enable it, add the following to your `values.yaml`:

```yaml
units:
  loki:
    enabled: true
```

## Custom Behavior

:::info
Additional configurations can be provisioned in Loki.
:::

### Configurations Customization

To customize the Loki configurations:
- logging-config related settings can be done in `kustomize-units/logging-config`
- loki related settings  `environment-values/${my-env}/values.yaml`

### Log retention

If users want to customize log retention configuration, they can override `sylva-unit` chart values by changing their `my-env/values.yaml` in `environment-values`:

```yaml
units:
  loki:
    helmrelease_spec:
      values:
        loki:
          limits_config:
            retention_period: 96h       # user-defined retention period
```