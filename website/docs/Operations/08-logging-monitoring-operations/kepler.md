---
title: Kepler
---

Kepler is able to provide energy consumption metrics, in a Prometheus format, by means of an exporter that is installed on any Sylva cluster.

Energy metrics are given at container level or at node level.

Kepler can assign power to processes (i.e. containers) using a BPF program integrated into the kernel's pathway to extract process-related resource utilization metrics or use metrics from Hardware Counters or cGroups.

Kepler metrics can be scraped and visualized by the Prometheus and Grafana tools already available in Sylva clusters.

The kepler documentation is available at https://sustainable-computing.io, its source code at https://github.com/sustainable-computing-io/kepler.

### How to enable
Add the following to your `values.yaml`:
```yaml
units:
  kepler:
    enabled: true
```
By default Kepler dashboard is not enabled on workload clusters but if you want to enable it then we have to activate Kepler and sylva-dashboard units in your workload cluster values
```yaml
units:
  kepler:
    enabled: true
  sylva-dashboards:
    enabled: true
```

### Access the dashboard
A grafana dashboard is provided to take a look at the main Kepler metrics on energy consumption and carbon footprint overall of the cluster or weighted on the cluster workload (e.g. per Namespace).

You can access it from Grafana: select Dashboards, open the `Kepler Exporter Dashboard`.

### Using Redfish/IPMI to get power metrics from bare metal nodes
By default the `kepler` unit doesn't get nodes power metrics via redfish/IPMI.

If you want to enable this capability on a baremetal cluster:
- add the following to your `values.yaml`:
```yaml
units:
  kepler:
    enabled: true
    helmrelease_spec:
      values:
        redfish:
          enabled: true
```
- provide the credentials of the nodes BMC in your `secrets.yaml`:
```yaml
units:
  kepler:
    helm_secret_values:
      redfish:
        fileContent: |
          <NODE_NAME_1>,<USERNAME>,<PASSWORD>,<REDFISH IP/HOSTNAME>
          <NODE_NAME_2>,<USERNAME>,<PASSWORD>,<REDFISH IP/HOSTNAME>
```

:::warning
Be aware that the BMCs credentials will be stored in a Secret on the cluster where Kepler is running, this may be unwanted on workload clusters.

It is recommended to use read-only Redfish credentials with Kepler.
:::
