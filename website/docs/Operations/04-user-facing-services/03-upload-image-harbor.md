---
title: Upload Images on Harbor
sidebar_label: Upload Images on Harbor
sidebar_position: 8
---

## Overview

This procedure details all the steps needed to upload images on Harbor deployed on the management cluster or workload cluster.

### Steps

Follow the below steps to upload images on Harbor:

1. Install Docker on the control plane node of your cluster.

```shell
#Use below proxy if needed
# export https_proxy=http://proxy.rd.francetelecom.fr:8080
# export http_proxy=http://proxy.rd.francetelecom.fr:8080
apt update
apt install docker.io docker
```

2. Add "harbor.sylva" to the insecure registry for Docker.
  
```shell  
cat /etc/docker/daemon.json
{
"insecure-registries": ["harbor.sylva"]
}
```
  
3. Restart Docker.

```shell
sudo systemctl restart docker
```
  
4. Add "harbor.sylva" in hosts file.

```shell
<cluster-external-ip> harbor.sylva
```

5. Login to Harbor. The admin should generate username and password before doing this step.

```shell
docker login harbor.sylva
# Enter Username and password when prompted
```

6. Tag the image before pushing, please note that the repository is the desired repository which would be already created on Harbor by the admin or user. Kindly change the name accordingly.

```shell
docker tag your-image-name:tag harbor.sylva/repository/image-name:tag
#example
docker tag alpine:latest harbor.sylva/library/alpine:0.1
```

7. Push image to Harbor.

```shell
docker push harbor.sylva/repository/image-name:tag
#example
docker push harbor.sylva/library/alpine:0.1
```
