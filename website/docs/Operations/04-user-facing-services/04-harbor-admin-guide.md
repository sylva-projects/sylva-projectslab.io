---
title: Harbor Admin Guide
sidebar_label: Harbor Admin Guide
---

## Overview

This procedure details all the steps needed to create a project, user, repository, etc. on Harbor.

### Project creation

Follow the below steps to create a project on Harbor:

:::note
In Harbor, projects can be created using 2 methods: the user interface (UI) or the command-line interface (CLI). This guide will focus on creating a project using the CLI.
:::

1. Get Harbor admin credentials by running below commands:

```shell
#For Local DB
kubectl get secret -n harbor harbor-init -o json | jq -r '.data."HARBOR_ADMIN_PASSWORD"' | base64 -d
#For Keycloak
kubectl get secret sylva-units-values -o template="{{ .data.values }}" | base64 -d | grep admin_password

```

2. To create a new project run the below API

```shell
curl --insecure -u 'admin-name:password' -X POST "https://harbor.sylva/api/v2.0/projects" \
-H "Content-Type: application/json" \
-d '{
  "project_name": "new-project-name", 
  "public": true,
  "metadata": {
    "enable_content_trust": "false",
    "prevent_vul": "true",
    "severity": "high",
    "auto_scan": "true"
  },
  "cve_allowlist": {
    "expires_at": 1698428390,
    "items": [
      {"cve_id": "CVE-2021-44228"}
    ]
  },
  "storage_limit": 524288000
}'

```

3. To retrieve a list of projects run the below command

```shell 
curl --insecure  -u 'admin-name:password' -X GET "https://harbor.sylva/api/v2.0/projects"

```

:::note 
To login into Harbor, OpenID Connect (OIDC) authentication mode is used with Keycloak as the single sign-on (SSO) provider. Since OIDC is enabled, user accounts are not created directly within Harbor. Instead, users authenticate via Keycloak and their access to Harbor is managed through groups and permissions defined in the Keycloak configuration.
:::

4. To create user in Harbor using Keycloak, login to the Keycloak UI (retrieve login credentials using below commands)
```shell
kubectl get secret keycloak-initial-admin -n keycloak -o json | jq -r '.data.username' | base64 -d
kubectl get secret keycloak-initial-admin -n keycloak -o json | jq -r '.data.password' | base64 -d

```

5. In the Keycloak UI select Sylva realm  create a new client and add a service account role "realm-admin" for 
   that client. 

6. Get the access token by running the below command

```shell

curl -k https://keycloak.sylva/realms/sylva/protocol/openid-connect/token 
-d "client_secret=<CLIENT_SECRET>" 
-d "client_id=<CLIENT_ID>" 
-d "grant_type=client_credentials"

```

7. To create a new user in Keycloak run the following API 
```shell
curl --insecure --location --request POST "https://<keycloak-server>/admin/realms/<realm-name>/users" \
--header "Authorization: Bearer <access-token>" \
--header "Content-Type: application/json" \
--data-raw '{
    "username": "example-username",
    "firstName": "test",
    "lastName": "user",
    "email": "test.user@example.com",
    "enabled": true,
    "realmRoles": ["role1", "role2"],
    "credentials": [
        {
            "type": "password",
            "value": "securePassword123",
            "temporary": false
        }
    ]
}'

```

8. Once the new user is created, log in to Harbor using Keycloak and use the credentials of the newly created user.
   A request for password change will appear on the screen. After changing the password, log in to Harbor via the new user credentials.

9. Repositories in Harbor are automatically created within a selected project when an image is pushed to the    project. To push an image and create the corresponding repository, follow the steps below:

```shell
docker login harbor.sylva
# Enter Username and password when prompted
docker tag <image-name>:<tag> harbor.sylva/<project-name>/<image-name>:<tag>
docker push harbor.sylva/<project-name>/<image-name>:<tag>

```
