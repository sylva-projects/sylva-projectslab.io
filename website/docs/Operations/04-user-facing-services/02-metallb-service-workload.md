---
title: Expose a service through Metallb on an OpenStack workload cluster
sidebar_label: Service exposition through Metallb (OpenStack example)
---

:::caution
This how-to is applied only on the OpenStack Workload cluster.
:::

## Introduction

This how-to aims to explain how to expose a workload cluster service on OpenStack via a MetalLB L2advertisement configuration.

We can distinguish 2 workload cluster deployments: with or without external network

Let's consider:

* virtual network where the workload cluster is deployed is called `virtual-network-A`

In this how-to, we consider `virtual-network-A` is `198.18.2.0/24`


## Common steps

Few steps are common to both cases.

## Pre-requisite

First, it is mandatory to reserve an OpenStack Port (at least one) on virtual-network-A. Let's call it `port-A`.

This Port corresponds to fixed Ip (and mac-address) we will reuse in deployment values and in additional metallb manifests.

```shell
port create --network <net-A UUID> --description "metallb-pool1" --fixed-ip ip-address=198.18.2.10 metallb-pool1
```

You can remember port-A UUID for later use (mainly for external network deployment case)

Choose an IP address belonging to virtual-network-A, here we have chosen `198.18.2.10`.

## Workload cluster configuration steps

### Prepare environment file

The environment values are defined into `environment-values/workload-clusters/my-workload-cluster/values.yaml`.

Under `cluster.capo` section you can add `extra` parameters:

```yaml
cluster:
  capo:
    extra:
      allowed_address_pairs:
        - ip: "198.18.2.10"
```

:::info  allowed_address_pairs
You can add multiple allowed address pairs.

:::tip address pair
* `ip`: mandatory value, single ip address (CIDR notation also accepted: 192.168.1.0/24)
* `mac`: optional value, single mac-address
:::
:::

### Apply changes to workload cluster

 ```shell
./apply-workload-cluster.sh environment-values/workload-clusters/my-workload-cluster
 ```


## Expose a service via MetalLB on your workload-cluster

Once deployment or modifications have been applied on your workload cluster, you should be able to
expose a service via a metallb load-balancer.

In the following section, we will explain how to expose a basic NGINX web service through a Metallb load-balancer

## Metallb manifests

First, you need to create an IP Address Pool corresponding to at least 1 Ip address reserved in OpenStack Port

```yaml
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: ippool1
  namespace: metallb-system
spec:
  addresses:
  - 198.18.2.10/32
```


:::warning IPAddressPool
namespace must be **metallb-system**
:::

Then create a L2Advertisment based on this Pool

```yaml
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: l2adv-1
  namespace: metallb-system
spec:
  ipAddressPools:
  - ippool1
```

:::warning **L2Advertisement**
namespace must be **metallb-system**
:::

## Deploy an Nginx pod

You can use the following manifests examples to deploy a functional nginx pod

:::warning
replace *your-namespace* with the namespace of your choice
:::

Deploy the following manifest on your workload cluster

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: myapp-config
  namespace: myapp-ns
data:
  nginx.conf: |
    worker_processes  2;

    error_log  /var/log/nginx/error.log notice;
    pid        /var/run/nginx.pid;

    events {}

    http {
        # include       /etc/nginx/mime.types;
        default_type  application/octet-stream;

        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';

        access_log  /var/log/nginx/access.log main;

        sendfile        on;
        #tcp_nopush     on;

        keepalive_timeout  65;
        gzip  on;

        client_max_body_size          128M;
        proxy_max_temp_file_size      0;
        proxy_buffering               off;
        server_names_hash_bucket_size 256;

        server {
            listen             0.0.0.0:80 default_server;

            # Healthcheck
            location /ping {
                return 200      'Ready !';
                add_header       Content-Type text/plain;
                log_not_found    off;
            }

            location / {
                # Serve local files
                root              /var/www/;
            }
        }
    }
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: myapp-web-page
  namespace: <your-namespace>
data:
  index.html: |
    <HTML>
      Hello Sylva world
    </HTML>
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: myapp
  namespace: <your-namespace>
spec:
  selector:
    matchLabels:
      app: myapp
  template:
    metadata:
      labels:
        app: myapp
    spec:
      securityContext:
        runAsUser: 1001
        runAsGroup: 1001
        fsGroup: 1001
      containers:
      - name: myapp
        image: nginx:1.25.3-alpine
        resources:
          limits:
            memory: "128Mi"
            cpu: "500m"
        ports:
        - containerPort: 80
        securityContext:
          allowPrivilegeEscalation: False
          runAsNonRoot: true
          seccompProfile:
            type: RuntimeDefault
          capabilities:
            drop:
              - ALL
          runAsUser: 10000
        volumeMounts:
          - mountPath: /var/www/
            name: web-pages
          - name: nginx-conf
            mountPath: /etc/nginx/
          - name: nginx-cache
            mountPath: /var/cache/nginx/
          - name: nginx-run
            mountPath: /var/run/
      volumes:
        - name: web-pages
          configMap:
            name: myapp-web-page
        - name: nginx-conf
          configMap:
            name: myapp-config
        - name: nginx-cache
          emptyDir: {}
        - name: nginx-run
          emptyDir: {}
---
apiVersion: v1
kind: Service
metadata:
  name: myapp
  namespace: <your-namespace>
spec:
  selector:
    app: myapp
  ports:
  - port: 80
    targetPort: 80
  type: LoadBalancer
```

### Verifications

#### MetalLB

First we can verify metallb resources.

Verify your IPAddressPool

```shell
$> kubectl -n metallb-system describe ipaddresspools.metallb.io ippool1
Name:         ippool1
Namespace:    metallb-system
Labels:       <none>
Annotations:  <none>
API Version:  metallb.io/v1beta1
Kind:         IPAddressPool
Metadata:
  Creation Timestamp:  2024-03-13T13:18:22Z
  Generation:          1
  Resource Version:    723664
  UID:                 01a922df-5b75-4bf7-9406-3a357da1d78f
Spec:
  Addresses:
    198.18.2.10/32
  Auto Assign:       true
  Avoid Buggy I Ps:  false
Events:              <none>
```

and your L2Advertisement

```shell
$> kubectl -n metallb-system describe l2advertisements.metallb.io l2adv-1
Name:         l2adv-1
Namespace:    metallb-system
Labels:       <none>
Annotations:  <none>
API Version:  metallb.io/v1beta1
Kind:         L2Advertisement
Metadata:
  Creation Timestamp:  2024-03-13T13:18:26Z
  Generation:          1
  Resource Version:    723704
  UID:                 a6e920fc-3932-429b-a08d-14896a5a63e3
Spec:
  Ip Address Pools:
    ippool1
Events:  <none>
```

#### Service 

Then verify your service has the correct Ip address


```shell
$>kubectl -n <your-namespace> describe service myapp
Name:                     myapp
Namespace:                myapp-ns
Labels:                   <none>
Annotations:              field.cattle.io/publicEndpoints: [{"addresses":["192.168.128.131"],"port":80,"protocol":"TCP","serviceName":"myapp-ns:myapp","allNodes":false}]
                          metallb.universe.tf/ip-allocated-from-pool: ippool1
Selector:                 app=myapp
Type:                     LoadBalancer
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       100.73.98.153
IPs:                      100.73.98.153
LoadBalancer Ingress:     198.18.2.10
Port:                     <unset>  80/TCP
TargetPort:               80/TCP
NodePort:                 <unset>  32379/TCP
Endpoints:                100.72.148.201:80
Session Affinity:         None
External Traffic Policy:  Cluster
Events:
  Type    Reason        Age                    From                Message
  ----    ------        ----                   ----                -------
  Normal  IPAllocated   2m12s                  metallb-controller  Assigned IP ["198.18.2.10"]
  Normal  nodeAssigned  2m12s (x2 over 2m12s)  metallb-speaker     announcing from node "xxxxx-cp-f2e575cf8f-jhsnd" with protocol "layer2"
```

You should see `LoadBalancer Ingress` corresponds to one of the IP addresses of your IPAddressPool

**To verify the service itself**

The host from which you will do the test must be able to reach CP nodes because MetalLB agents are installed on such nodes.

Execute the command `curl --noproxy '*' http://198.18.2.10`

You should have a result similar to

```shell
$>curl http://198.18.2.10
<HTML>
  Hello Sylva world
</HTML>
```

## Expose service via an External floating IP

In case of workload cluster deployment with an external network, you should follow those additional steps

## Reserve a floating IP

With the following command you can create a new floating-ip in the external-network and associate it with the port we have created previously (port-A)
for our metallb load-balancer.

```shell
floating ip create --port <port-A UUID> --description external_ip_metallb <external-network UUID>
```

Let's call this floating ip: `floating-ip-A`

:::warning  routing
In order to be able to create such floating ip, routing must operational between both network
external-network and virtual-network-A
:::

## Verification

From a host which can reach the external-network, execute the command:  `curl --noproxy '*' http://<floating-ip-A>`

You should have a result similar to

```shell
$>curl http://<floating-ip-A>
<HTML>
  Hello Sylva world
</HTML>
```

## Troubleshooting

If you choose an ip-address in IPAddresspool not compatible with the range declared in `allowed_address_pairs`

The creation of IPAddresspool kubernetes objects will be successful but it won't work, it should log a message in `metallb-controler`
saying that the selected is not compatible, like:

```
{"caller":"service.go:150","event":"ipAllocated","ip":["192.168.128.11"],"level":"info","msg":"IP address assigned by controller","ts":"2024-03-12T16:38:08Z"}
{"caller":"service.go:108","error":"[\"192.168.128.11\"] is not allowed in config","event":"clearAssignment","level":"info","msg":"current IP not allowed by config, clearing","ts":"2024-03-13T13:10:52Z"}
```
