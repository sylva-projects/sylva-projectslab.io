---
sidebar_position: 1
title: WebUIs / APIs
---

:::info
This tutorial has been made using rke2-capo values (Openstack deployment).
:::

Once you've successfully deployed sylva, you'll notice :

- the accessible UIs :

![acc-uis](./img/01-deployment-success-uis.png)

- the console logs

```
...       (xxx.sylva must resolve to <ip>)
```

## Access Available UIs

Use the below code snippet **from the machine** you want to access those user interfaces via a browser.

```$ sudo vim /etc/hosts```

```
<ip> kubevirt-manager.sylva rancher.sylva flux.sylva harbor.sylva keycloak.sylva minio-monitoring-tenant-console.sylva minio-operator-console.sylva thanos.sylva vault.sylva

```

You can fetch the resolve IP **from the management** cluster with :

```
kubectl get secret sylva-units-values -o yaml \
| yq .data.values \
| base64 -d \
| yq .display_external_ip
```

| Software Name                | Address                                  | Pre-defined User | Authentication Guide          |
|------------------------------|------------------------------------------|------------------|-------------------------------|
| Rancher                      | [rancher.sylva](http://rancher.sylva)    | rancher             | [How to Authenticate](/docs/Operations/user-and-role-management/authentication#rancher) |
| Flux                         | [flux.sylva](http://flux.sylva)          | flux-webui             | [How to Authenticate](/docs/Operations/user-and-role-management/authentication#flux) |
| Harbor                       | [harbor.sylva](http://harbor.sylva)      | harbor             | [How to Authenticate](/docs/Operations/user-and-role-management/authentication#harbor) |
| Keycloak                     | [keycloak.sylva](http://keycloak.sylva)  | sylva-admin             | [How to Authenticate](/docs/Operations/user-and-role-management/authentication#keycloak) |
| Vault                        | [vault.sylva](http://vault.sylva)        |              | [How to Authenticate](/docs/Operations/user-and-role-management/authentication#vault) |
| Thanos                       | [thanos.sylva](http://thanos.sylva)      | thanos-user             | [How to Authenticate](/docs/Operations/user-and-role-management/authentication#thanos) |
| MinIO Monitoring Tenant Console | [minio-monitoring-tenant-console.sylva](http://minio-monitoring-tenant-console.sylva) | minio (root access) / console | [How to Authenticate](/docs/Operations/user-and-role-management/authentication#minio-monitoring-tenant-console) |
| MinIO Operator Console       | [minio-operator-console.sylva](http://minio-operator-console.sylva) | TODO             | [How to AuthenticateTODO](/docs/Operations/user-and-role-management/authentication#minio-operator-console) |


## APIs

Sylva gives access to some monitoring endpoints.

**TODO**: Provide a comprehensive guide to use those endpoints (GET requests? for the monitoring stack)

### MinIO

Also exposes those URLs (not webUIs) :

| Address                                  | Software Name                | Authentication Guide          |
|------------------------------------------|------------------------------|-------------------------------|
| [minio-monitoring-tenant.sylva](http://minio-monitoring-tenant.sylva) | MinIO Monitoring Tenant | [How to Authenticate/Query TODO](/docs/Operations/user-and-role-management/authentication) |

### Thanos

Also exposes those URLs (not webUIs) :

| Address                                  | Software Name                | Authentication Guide          |
|------------------------------------------|------------------------------|-------------------------------|
| [thanos-query.sylva](http://thanos-query.sylva) | Thanos Query | [How to Authenticate/Query TODO](/docs/Operations/user-and-role-management/authentication#thanos) |
| [thanos-receive.sylva](http://thanos-receive.sylva) | Thanos Receive | [How to Authenticate/Query TODO](/docs/Operations/user-and-role-management/authentication#thanos) |
| [thanos-storegateway.sylva](http://thanos-storegateway.sylva) | Thanos Store Gateway | [How to Authenticate/Query TODO](/docs/Operations/user-and-role-management/authentication#thanos) |

### Kubevirt dashboard login credentials

User by default: `admin`

The password is accessible from the management cluster. The secret `sylva-units-values` is created in the `sylva-system` namespace for the management cluster, while for workload clusters, it is created on the same management cluster within each workload cluster's specific namespace:

```shell
kubectl get secret sylva-units-values -n <namespace of specific cluster > -o template="{{ .data.values }}"   | base64 -d | grep kubevirt_admin_password
```

How Kubevirt dashboard looks

![kubevirt-dashboard](./img/19-kubevirt-dashboard.png)
