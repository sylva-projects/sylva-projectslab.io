---
title: Flow matrix
---

## Flow matrix and networking overview 

The diagram below illustrates the primary blocks that exchange flows within the system:

![Flows-blocs](img/Flows-blocs.png)

These flows are necessary:

- Between the bootstrap cluster and the management-cluster
- Between the management cluster and any workload cluster


Flows are grouped according to the network and zone:

- `ext` : related to all flows external to Sylva (access to DNS, registry, etc.)
- `BMC` : related to flows exchanged with the Bare-Metal Control interface
- `prov` : related to flows exchanged with bare-metal servers during provisioning phase
- `mgmt` : related to flow exchanged between clusters (from their default CNI networks)
- `node` : related to flows exchanged between nodes within a cluster on the default CNI network (may depend on node roles: control, worker and storage)

:::note
- Internal pod-to-pod specific flows are not detailed here as they can be internal to a node or node-to-node flows if pods are hosted on separate nodes.
- All internal flows are intended to be controlled through network policies.
:::


## Flow-groups details

### `ext` group

- Ingress (ext -> cluster)

  | Protocol | Port | Source                                    | Description                                                         |
  | -------- | ---- | ----------------------------------------- | ------------------------------------------------------------------- |
  | TCP      | 22   | Bastion / jump-host                       | SSH (should be restricted from a Bastion source)                    |
  | TCP      | 443  | Web client                                | UI access (Flux webUI, Rancher UI, Keycloak UI, Vault UI, Grafana)  |
  | TCP      | 6443 | external CLI client                       | kube-api                                                            |


- Egress (cluster -> ext)

  :::note For OpenStack
  The bootstrap cluster and the management cluster need to access all main OpenStack API: Keystone, Nova, Glance, Cinder, Heat, Neutron.

  From a workload cluster, it is possible to restrict to only Keystone, Cinder and Nova APIs.

  OpenStack API ports may depend on a potential external reverse-proxy configuration in front of OpenStack IaaS platform. The table below does not reflect that situation.
  :::
  
  | Protocol | Port | Source           | Description                                                  |
  | -------- | ---- | ---------------- | ------------------------------------------------------------ |
  | TCP      | 53   | any cluster node | DNS (to reach external DNS)                                  |
  | UDP      | 53   | any cluster node | DNS (to reach external DNS)                                  |
  | UDP      | 123  | mgmt-cluster     | NTP                     |
  | TCP      | 80   | any cluster node | Flux-controllers->gitlab, OCI registry access, OpenStack API |
  | TCP      | 443  | any cluster node | Flux-controllers->gitlab, OCI registry access, OpenStack API |
  | TCP      | 636  | mgmt-cluster     | optional if Keycloak is leveraging on external IDM through LDAPS |
  | TCP      | 389  | mgmt-cluster     | optional if Keycloak is leveraging on external IDM through LDAPS |
  
  :::note
  
  This table is not exhaustive regarding the workload flows. The workload cluster could require additional flows from its hosted applications, from its additional Multus interfaces and service exposition (e.g. to handle BGP session with external peer).
  
  :::

### `BMC` group (used only for Bare-Metal deployment)

- Ingress (management/bootstrap cluster -> BMC)
  
  | Protocol | Port | Source              | Destination | Description                                               |
  | -------- | ---- | --------------------| ----------- | ----------------------------------------------------------|
  | UDP      | 161  | mgmt-cluster nodes  | BMC         | SNMPv3 exporter (poll mode from Prometheus snmp_exporter) |
  | TCP      | 443  | mgmt-cluster nodes  | BMC         | Redfish API access                                        |
  | TCP      | 443  | bootstrap node      | BMC         | Redfish API access (only during bootstrap)                |


- Egress (BMC -> management/bootstrap cluster)

  | Protocol | Port | Source | Destination       | Description                                  |
  | -------- | ---- | ------ | ----------------- | -------------------------------------------- |
  | TCP      | 6180 | BMC    | mgmt-cluster VIP  | pull discovery image (IPA)                   |
  | TCP      | 6180 | BMC    | bootstrap node    | pull discovery image (only during bootstrap) |


#### `prov` group (used only for Bare-Metal deployment during provisioning phase)

- Ingress (management/bootstrap cluster -> BM during provisioning)

  | Protocol | Port | Source                      | Destination  | Description                                                |
  | -------- | ---- | ----------------------------| ------------ | ---------------------------------------------------------- |
  | TCP      | 9999 | mgmt-cluster nodes (Ironic) | BM IPA       | Ironic commands to running ramdisk                         |
  | TCP      | 9999 | Bootstrap node (Ironic)     | BM IPA       | Ironic commands to running ramdisk (only during bootstrap) |
  

- Egress (BM during provisioning -> management/bootstrap cluster)

  | Protocol | Port | Source   | Destination              | Description                                       |
  | -------- | ---- | -------- | ------------------------ | ------------------------------------------------- |
  | TCP      | 80   | BM IPA   | mgmt-cluster VIP         | HTTP pull OS image                                |
  | TCP      | 443  | BM IPA   | mgmt-cluster VIP         | HTTPS pull OS image                               |
  | TCP      | 5050 | BM IPA   | mgmt-cluster VIP         | to the Ironic Inspector                           |
  | TCP      | 6385 | BM IPA   | mgmt-cluster VIP         | to Ironic main API port                           |
  

### `mgmt` group (used for both VM and Bare-Metal deployment)

- Ingress (management-cluster -> workload cluster or bootstrap-cluster -> management cluster during the bootstrap step)

  | Protocol | Port | Source             | Destination                             | Description                                                   |
  | -------- | ---- | ------------------ | --------------------------------------- | --------------------------------------------------------------|
  | TCP      | 6443 | mgmt-cluster nodes | mgmt-cluster and workload-clusters VIPs | kube-api, capi-controller                                     |
  | TCP      | 6443 | bootstrap node     | mgmt-cluster VIP                        | kube-api, capi-controller (only during bootstrap)             |
 
  
- Egress (workload cluster -> management-cluster)

  | Protocol | Port | Source                        | Destination              | Description                                                            |
  | -------- | ---- | ----------------------------- | ------------------------ | ---------------------------------------------------------------------- |
  | TCP      | 53   | workload cluster master nodes | mgmt-cluster VIP         | Core-dns (internal dns)                                                |
  | UDP      | 53   | workload cluster master nodes | mgmt-cluster VIP         | Core-dns (internal dns)                                                |
  | TCP      | 443  | workload cluster nodes        | mgmt-cluster VIP         | HTTPS toward Rancher API (cattle-agent), logs centralization to Loki, or remote-write from Workload Prometheus to Thanos receive |



### `node` group (informative)

The following table is just informative and potentially not exhaustive. Egress and Ingress are not separated since it depends on which node is the component which originates the flow. All those flows can be controlled by network policies managed through Calico or NeuVector.

| Protocol | Port        | Description                                                  |
| -------- | ----------- | ------------------------------------------------------------ |
| TCP      | 22          | Node driver SSH provisioning                                 |
| TCP      | 80          | HTTP (various components)                                    |
| TCP      | 179         | Calico BGP Port                                              |
| TCP      | 443         | HTTPS (various components)                                   |
| UDP      | 1023        | K8S gateway                                                  |
| TCP      | 2376        | Node driver Docker daemon TLS port                           |
| TCP      | 2379        | etcd client requests                                         |
| TCP      | 2380        | etcd peer communication                                      |
| TCP      | 5473        | Calico typha                                                 |
| TCP      | 6443        | Kubernetes API                                               |
| TCP      | 7472        | metallb speaker                                              |
| TCP      | 8443        | Rancher webhook                                              |
| TCP      | 9098        | Calico typha                                                 |
| TCP      | 9099        | Calico node                                                  |
| TCP      | 9100        | Default port required by Monitoring to scrape metrics from Linux node-exporters |
| TCP      | 9345        | Rancher Node registration                                    |
| TCP      | 9443        | Rancher webhook                                              |
| TCP      | 9500–9503   | Longhorn ports (CSI, Manager, Webhook)                       |
| TCP      | 9796        | Default port required by Monitoring to scrape metrics from node-exporters |
| TCP      | 9808        | Cinder-CSI                                                   |
| TCP      | 10250       | Metrics server communication with all nodes API, kubelet     |
| TCP      | 10254       | Ingress controller livenessProbe/readinessProbe              |
| TCP      | 10256       | kube-proxy                                                   |
| TCP      | 10257       | kube-controller                                              |
| TCP      | 10259       | kube-scheduler                                               |
| TCP      | 21922       | node-ingress                                                 |
| TCP/UDP  | 30000–32767 | NodePort port range                                          |
