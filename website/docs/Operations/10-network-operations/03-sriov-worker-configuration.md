---
title: SR-IOV worker definition
---

## Prerequisites

SR-IOV workers are dedicated to network-intensive workloads (low latency, high throughput, real-time). The node configuration is optimized to avoid any noise from kernel, housekeeping tasks, or latency introduced by power consumption reduction mechanism.

### Bare Metal Worker

The BIOS configuration is not fully automated through Ironic and Redfish. Be sure that SR-IOV is enabled in BIOS and C-states are disabled using a performance profile (C-states can also be tuned through the [Kernel](#kernel)).

The hardware NUMA alignment between SR-IOV NIC and NUMA node is also mandatory to avoid the latency budget introduced by the cross-NUMA link. On a two NUMA nodes host, using SR-IOV from each NUMA requires two PCI NIC inserted in corresponding PCI slot. Here after an illustration with Dell R650.

![R650_3NICs](img/R650_3NICs.PNG)

The following image illustrates an internal view of a Bare-Metal SR-IOV worker:

![Sylva-Networks-BM-worker-master-SR-IOV](img/Sylva-Networks-BM-worker-master-SRIOV.png)



## Worker configuration

The worker(s) configuration is part of workload cluster definition in `my_workload_cluster/values.yaml`.

### Using node class profile

Node class profiles have been introduced to facilitate the worker configuration. 

#TODO: link to node class docs on Sylva

### CPU reservation tuning (Bare Metal)

The SR-IOV node class should set reservation in accordance to the bare-metal host topology. 

#### Kubelet:

```yaml
node_classes:
    sriov-generic:
      kubelet_config_file_options:
        reservedSystemCPUs: "0-3,64-67" # 8 vCPUs are explicitly reserved for kube and system      
```

The explicit `reservedSystemCPUs` list depends on the hardware vendor, topology and the number of cores. It has also to be adapted if the SR-IOV node acts as control plane node for workload cluster. Moreover, when the node acts as a control plane node, the control resource sizing depends on the cluster sizing (number of nodes and objects). When collocated, it is highly recommended to monitor the control plane resources consumption to avoid any downtime due to lack of control resources.

:::note

It is recommended when possible to collocate control plane with default node-class workers rather than SR-IOV ones. For Large clusters (more than 24 nodes) dedicated control plane nodes are highly recommended.

:::

#### Kernel:

If the BIOS setting allows C-states and frequency adaptation (P-states) through the kernel settings, it has to be disabled through `kernel_cmdline.extra_options`.  The kernel should also be configured to keep housekeeping process or any kernel noise out of the exclusive CPU pool used to pin containers with guaranteed request.

```yaml
sriov-generic:
  kernel_cmdline:
    extra_options: "idle=poll intel_pstate=disable intel_iommu=on iommu=pt nohz=on nohz_full=4-63,68-127 systemd.cpu_affinity=0-3,64-67 irqaffinity=0-3,64-67 nosoftlockup"
```

The explicit CPU list set for `nohz_full` is the opposite list of kubelet `reservedSystemCPUs`.

The explicit CPU list set for `systemd.cpu_affinity` and `irqaffinity` is the same than the kubelet `reservedSystemCPUs` list.

### Memory reservation tuning (Bare Metal)

#### Huge-pages

Huge-pages are mandatory and most of the times 1G huge-pages are used by CNF. The Amount of huge-pages requested is variable and depends on the workload request. The system will prevent to configure more than 90% of RAM as huge-pages (to let memory for the system). Using a node with a total RAM of 512GB, the example below will set 64 * 1Gi pages (64GB).

```yaml
hugepages:
  enabled: true
  2M_percentage_total: "0"
  1G_percentage_total: "0.125"
  default_size: 1G
```

#### Kubelet reserved memory

The proposed `kubeReserved` and `systemReserved` values in `sriov-generic` profile need to be adapted for bare metal. Since bare metal nodes have usually a large amount of memory, it is possible to ensure to the system and kube enough memory to not risk any OoM killer process on system and kube.

```yaml
kubeReserved:
  memory: 7Gi
systemReserved:
  memory: 7Gi
```

#### Hard Eviction

Similarly, considering bare metal nodes with large amount of RAM, it is possible to increase the hard eviction threshold to provide higher free memory margin.

```yaml
evictionHard:
  memory.available: "2000Mi"
```

#### Static Memory allocation (optional)

Accessing memory allocated in another NUMA node is considerably slower and could impact performance. With low latency use cases, the data transfer between NUMA nodes must be minimized using multiple NUMA nodes hosts. Using the `topology-manager` with `single-numa-node` policy does already ensure and enforce NUMA alignment. The `memory-manager` allows memory-cpu alignment or multi-numa memory allocation within the same node thus provides a more flexible solution compare to only use the `topology-manager` with `single-numa-node` policy .

In order to control CPU and memory alignment on the NUMA node(s), the `memory-manager-policy` must be set to `static` with explicit reservation per NUMA (for kube & system):

```yaml
kubelet_config_file_options:
  memoryManagerPolicy: static #offers the guaranteed memory (and hugepages) allocation in accordance to the NUMA topology
  reservedMemory:
    - numaNode: 0
      limits:
      memory: 1.5Gi
    - numaNode: 1
      limits:
      memory: 1Gi  
```

:::note
The sum of `reservedMemory` limits must be equal to the sum of `kubeReserved-memory` , `systemReserved-memory` and `evictionHard-memory-available`.
:::

The following table provides an example of static memory reservation. If the node acts as a control plane node, the control resource (mainly `kubeReserved`) sizing depends on the cluster sizing (number of nodes and objects).

| Memory reservation      | `systemReserved` `memory` | `kubeReserved` `memory` | `evictionHard` `memory.available` | `reservedMemory` `numaNode: 0` | `reservedMemory` `numaNode: 1` |
| ----------------------- | ------------------------- | ----------------------- | --------------------------------- | ------------------------------ | ------------------------------ |
| Worker-only (2 NUMA)    | 7Gi                       | 7Gi                     | 2000Mi                            | 8Gi                            | 8Gi                            |
| Worker-Control (2 NUMA) | 7Gi                       | 15Gi                    | 2000Mi                            | 12Gi                           | 12Gi                           |

:::warning
It is highly recommended to monitor the control plane resources consumption to avoid any downtime due to lack of resources: in nominal load, the control plane memory usage must be kept to at most 60% of reserved memory capacity to handle the resource usage spikes (such spike can occur during node failure or during upgrade).
:::

### Bare-metal custom sriov class example

The following setting defines a custom node-class adapted to a Dell server with 2 * 32 cores sockets and 512 GB of RAM (64GB in 1GB huge-pages). 

```yaml
    sriov-bm-32c:
      kernel_cmdline:
        hugepages:
          enabled: true
          2M_percentage_total: "0"
          1G_percentage_total: "0.125" #change me considering workload requirements
          default_size: 1G
        extra_options: "idle=poll intel_pstate=disable intel_iommu=on iommu=pt nohz=on nohz_full=4-63,68-127 systemd.cpu_affinity=0-3,64-67 irqaffinity=0-3,64-67 nosoftlockup" #change me considering the vendor CPU numbering
      kubelet_extra_args: {}
      kubelet_config_file_options:
        cpuManagerPolicy: static
        cpuManagerPolicyOptions:
          full-pcpus-only: "true"
        topologyManagerPolicy: single-numa-node
        topologyManagerScope: pod
        kubeReserved:
          memory: "7Gi"
        systemReserved:
          memory: "7Gi"
        reservedSystemCPUs: "0-3,64-67" #change me considering the vendor CPU numbering
        evictionHard:
          memory.available: "2000Mi"
      nodeTaints: {}
      nodeLabels:
        sylva.org/annotate-node-label-sriov: "true"
      nodeAnnotations: {}
      additional_commands:
        pre_bootstrap_commands: []
        post_bootstrap_commands: []
```



The custom node-class is then applied in the machine-deployment definition

```yaml
    md2:
      infra_provider: capm3
      node_class: sriov-bm-32c
      replicas: 3
      capm3:
        hostSelector:
          matchLabels:
            cluster-role: sriov-worker-2 # tweak as needed must match cluster-role defined in baremetal_hosts
            sylva.org/annotate-node-label-sriov: "true"
      network_interfaces: # tweak network configuration as needed ...
        bond0:
          type: bond
          bond_mode: 802.3ad
          bondXmitHashPolicy: layer3+4
          interfaces:
            - ens2f0
            - ens2f1
          vlans:
            - id: 2209
        ens2f0:
          type: phy
        ens2f1:
          type: phy
```



The machine-deployment will be mapped to the bare metal host available in regard to the `cluster-role` label:

```yaml
    my-dell-server1:  # corresponding credentials need to be set in secrets.yaml
      bmh_metadata:
        labels:
          cluster-role: sriov-worker-2
```

### Verify SR-IOV node configuration (bare-metal)

From the workload cluster:

```bash
kubectl get nodes #find your sr-iov node you want to check
kubectl describe node <your-sriov-node>  
```

From the node itself:

Verify the kernel setting

```bash
cat /proc/cmdline
BOOT_IMAGE=/boot/vmlinuz-5.14.21-150500.55.49-default root=LABEL=img-rootfs audit=1 audit_backlog_limit=8192 console=tty0 console=ttyS0,115200 no_timer_check nofb nomodeset gfxpayload=text net.ifnames=1 idle=poll intel_pstate=disable intel_iommu=on iommu=pt nohz=on nohz_full=4-63,68-127 systemd.cpu_affinity=0-3,64-67 nosoftlockup hugepagesz=1G hugepages=63 hugepagesz=2M hugepages=0 default_hugepagesz=2M default_hugepagesz=1G transparent_hugepage=never
# verify c-states is disbled
cpupower idle-info
CPUidle driver: none
CPUidle governor: menu
analyzing CPU 0:

CPU 0: No idle states

# verify cpu-isolation
cat /sys/devices/system/cpu/nohz_full
4-63,68-127
```

-to be completed-

## Test with sample pod deployment  

-to be completed-

### SR-IOV network operator settings 

-to be completed-