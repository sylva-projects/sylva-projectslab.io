---
title: Multus Sample Usage
---

## Multus test example on OpenStack deployment

:::info Procedure for test purpose only 
The following procedure contains manual configuration steps on OpenStack. This way of configuring Multus is still not fully usable for production since it includes non-persistent operations. 
:::

### Prerequisites

On Openstack, create an additional network (with DHCP enable or not): e.g. `subnet 10.10.10.0/24` with limited allocated pool `start 10.10.10.2 - end 10.10.10.126` (you need to keep allocable pool for the pod additional interfaces).

:::warning Single default gateway 
The additional networks shouldn't have a default gateway to external network otherwise OpenStack will try to configure it as the default gateway in the multi-attached VM but such VM should already have a default gateway which should be in the default CNI subnet (aka the management network).
:::

Multus can be used with Provider Network or Tenant Network. This example is using tenant network.

On the Workload Cluster enable Multus by declaring the corresponding `unit` within the cluster `values.yaml`: 

```yaml
units:
  multus:
    enabled: true

cluster:
  […]
```

Declare one or several workers (aka `machine_deployments`) with more than a single interface. In case of several worker configurations (several `machine_deployment` definitions), it is possible to add a node label to target the right node using such label (in the example below, `worker-class: multus` label is added to `md1` nodes).

```yaml
 machine_deployments:
   md1:
     replicas: 1
     rke2:
        nodeLabels:
          worker-class: multus
     capo:
       flavor_name: "your-flavor"
     network_interfaces:
       secondary:
         network_id: "your-2nd-network" # .network.id; VN needs to exist in OpenStack tenant
         vnic_type: normal # .vnicType; can be "direct" or "normal"
     #  third:
     #    network_id: "your-3rd-network"
     #    vnic_type: direct 
```

`vnic_type: direct` is used for attaching SRiOV VF. In this example, Multus plug-in used is `macvlan` thus the `vnic_type` need to be set to `normal`. It is possible to add a new interface with `normal` type to an existing worker (no need to delete and recreate it).

Then trigger reconciliation with (to be adapted to your context and K8s version):

```shell
./apply-workload-cluster.sh environment-values/workload-clusters/my-workload-cluster
```
Once your worker is ready with multiple interface (you can check using `openstack server list`, you need to allow ICMP and SSH on both interfaces using the OpenStack security groups.

```
openstack server list
+--------------------------------------+----------------------------------------+--------+-----------------------------------------------------------------------+--------------------------+-----------+
| ID                                   | Name                                   | Status | Networks                                                              | Image                    | Flavor    |
+--------------------------------------+----------------------------------------+--------+-----------------------------------------------------------------------+--------------------------+-----------+
| d1028418-55fc-43fd-ad18-d5986b470dc6 | cluster-md-md1-c759e98965-6xwn6        | ACTIVE | Workload-1=10.20.20.15; Workload-2=10.10.10.45; caas-mgmt=10.10.20.25 | N/A (booted from volume) | B1.large  |
```

If you planned to test the deployment of several pods with Multus using the same additional interface(s) of the worker, the OpenStack anti-spoofing need to be deactivated on this (those) worker additional interfaces (to be done on each port) otherwise additional allowed address-pair need to be configured.

```shell
openstack port set --disable-port-security <port>
```

### Retrieve the secondary interface name from the worker

The name of the interface is used by the `NetworkAttachmentDefinition`  and then by Multus to bind the pod secondary interface to the worker interface.

Connect with SSH on the worker:

```shell
ssh -i ~/my_key.pem user@ip of your worker
```
Retrieve the secondary interface name using `ip a` associated with your secondary subnet:
```shell
3: ens7: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 8950 qdisc fq_codel state UP group default qlen 1000
    link/ether fa:16:3e:db:c4:60 brd ff:ff:ff:ff:ff:ff
    altname enp0s7
    inet 10.10.10.45/24 metric 100 brd 10.10.10.255 scope global dynamic ens7
       valid_lft 61275sec preferred_lft 61275sec
```
### Prepare Kubernetes resources

Create a namespace for test by applying the following manifest on the Workload Cluster:
        
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: test-multus
  labels:
    name: test-multus
```

```shell
kubectl  --kubeconfig kubeconfig-owner apply -f namespace.yaml
```

Create network attachment by applying the following manifest on the Workload Cluster:

```yaml
apiVersion: "k8s.cni.cncf.io/v1"
kind: NetworkAttachmentDefinition
metadata:
  name: macvlan-confprivate
  namespace: test-multus

spec:
  config: '{
    "cniVersion": "0.3.0",
    "type": "macvlan",
    "master": "ens7",
    "mode": "bridge",
    "ipam": {
      "type": "host-local",
      "subnet": "10.10.10.0/24",
      "rangeStart": "10.10.10.200",
      "rangeEnd": "10.10.10.253",
      "gateway": "10.10.10.1"
    }
  }'
```
where `master` is the same interface as the one define on the workload cluster worker VM.

Create a pod having a second interface on the network attachment:
        

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mpod1
  namespace: test-multus
  annotations:
    k8s.v1.cni.cncf.io/networks: macvlan-confprivate

spec:
  affinity: #affinity is used to oblige the pod deployment on the right node(s)
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: worker-class
            operator: In
            values:
            - multus
  containers:
    - name: mpod1
      command: ["/bin/ash", "-c", "trap : TERM INT; sleep infinity & wait"]
      image: alpine
      securityContext:
        runAsUser: 1000
        runAsGroup: 3000
      
        allowPrivilegeEscalation: false
        runAsNonRoot: true
        capabilities:
          add:
          - NET_BIND_SERVICE
          drop:
          - ALL
        seccompProfile:
          type: RuntimeDefault
          
```
### Check configuration and behavior

Describe your pod to check the IP:

```shell
kubectl  --kubeconfig kubeconfig-owner  describe pod  mpod1 -n test
```
```json
k8s.v1.cni.cncf.io/network-status:
                    [{
                        "name": "k8s-pod-network",
                        "ips": [
                            "10.42.49.117"
                        ],
                        "default": true,
                        "dns": {}
                    },{
                        "name": "test/macvlan-confprivate",
                        "interface": "net1",
                        "ips": [
                            "10.10.10.202"
                        ],
                        "mac": "56:13:0c:ed:99:2d",
                        "dns": {}
                    }]

```

Then you can check the ping between the pod and the test VM:

```shell
kubectl --kubeconfig kubeconfig-owner -n test exec -it mpod1 -- ping -c 1 10.10.10.200
PING 10.10.10.200 (10.10.10.200): 56 data bytes
64 bytes from 10.10.10.200: seq=0 ttl=42 time=1.277 ms

--- 10.10.10.200 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 1.277/1.277/1.277 ms

```
## Multus usage on BM deployment

To be completed, especially config on Node
