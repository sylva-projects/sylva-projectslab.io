---
sidebar_position: 4
---

# Finding the default admin SSO password

Sylva tooling will generate a random default admin password, used for accessing the different services (Rancher, Flux Web UI and Vault) through the Keycloak default SSO account.

This password can be found in Vault (path secret/sso-account) or with the following command:

```shell
kubectl get secrets sylva-units-values -n sylva-system -o yaml | yq .data.values | base64 -d | yq .admin_password
```
