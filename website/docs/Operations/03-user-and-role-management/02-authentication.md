---
sidebar_position: 2
title: Authentication
---

## OIDC

Most of the time, you will simply use the Single-Sign-On (SSO) functionnality with the Keycloak OIDC using the [Keycloak credentials](#keycloak). This will allow the user to sign on only one time and access most of the applications.

## Local Accounts

In some cases, you might not want to use the OIDC. 
Local accounts credentials are available 
- in [Vault](#vault)
- if access to secrets, decoding secrets

### Rancher

Username: (pre-defined as 'rancher')

```
kubectl get secret -n keycloak keycloak-client-secret-rancher-client -o yaml \
| yq .data.CLIENT_ID \
| base64 -d
```

Password:

```
kubectl get secret -n keycloak keycloak-client-secret-rancher-client -o yaml \
| yq .data.CLIENT_SECRET \
| base64 -d
```

### Flux

Username: (pre-defined as 'flux-webui')

```
kubectl get secret -n keycloak keycloak-client-secret-flux-webui-client -o yaml \
| yq .data.CLIENT_ID \
| base64 -d
```

Password:

```
kubectl get secret -n keycloak keycloak-client-secret-flux-webui-client -o yaml \
| yq .data.CLIENT_SECRET \
| base64 -d
```

### Harbor

Username: (pre-defined as 'harbor')

```
kubectl get secret -n keycloak keycloak-client-secret-harbor-client -o yaml \
| yq .data.CLIENT_ID \
| base64 -d
```

Password:

```
kubectl get secret -n keycloak keycloak-client-secret-harbor-client -o yaml \
| yq .data.CLIENT_SECRET \
| base64 -d
```

### Keycloak

:::info
Also used for OIDC login for other applications
:::

Username: (pre-defined as 'sylva-admin')

```
kubectl get secret -n keycloak credential-sylva-sylva-admin-keycloak -o yaml \
| yq .data.username \
| base64 -d
```

Password:

```
kubectl get secret -n keycloak credential-sylva-sylva-admin-keycloak -o yaml \
| yq .data.password \
| base64 -d
```

### Vault

Refer to the [Sylva-vault documentation](../09-security-operations/vault.md) in the Operations part (will be moved here in another MR).

### Thanos

Username: (pre-defined as 'thanos-user')

```
kubectl get secret sylva-units-values -o yaml \
| yq .data.values \
| base64 -d \
| yq .units.monitoring.helm_secret_values.prometheus.extraSecret.data.username
```

Password:

```
kubectl get secret sylva-units-values -o yaml \
| yq .data.values \
| base64 -d \
| yq .units.monitoring.helm_secret_values.prometheus.extraSecret.data.password
```

### Minio Monitoring Tenant Console

We provide two different users (TODO: Document what each user does: 'root' & 'user')

#### Root

Username: (pre-defined as'minio')

```
kubectl get secret -n sylva-system thanos-minio-root -o yaml \
| yq .data.MINIO_ROOT_USER \
| base64 -d
```

Password:

```
kubectl get secret -n sylva-system thanos-minio-root -o yaml \
| yq .data.MINIO_ROOT_PASSWORD \
| base64 -d
```

#### User

Username: (pre-defined as 'console')

```
kubectl get secret -n minio-monitoring-tenant minio-monitoring-user -o yaml \
| yq .data.CONSOLE_ACCESS_KEY\
| base64 -d
```

Password:

```
kubectl get secret -n minio-monitoring-tenant minio-monitoring-user -o yaml \
| yq .data.CONSOLE_SECRET_KEY\
| base64 -d
```

### Minio Operator Console

Token retrieval:

```
kubectl get secret console-sa-secret -n minio-operator -o jsonpath='{.data.token}' \
| base64 -d

```
