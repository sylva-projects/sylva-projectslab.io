---
sidebar_position: 2
title: Identity and Access Management
---


## Keycloak Admin Console

To administer `Keycloak`, visit the "Keycloak GUI" at `https://keycloak.sylva` and access the [Administration Console](https://keycloak.sylva/admin/master/console).

![keycloak-ui](./img/keycloak-ui.png)

Authenticate using the password stored in `Vault` under the path `secret/keycloak` (the default login name is `admin`). For guidance on retrieving secrets from `Vault`, refer to the [relevant section](./vault#local-accounts).

> **_NOTE_**: Logging into the administration console logs you as the administrator of the `master` realm, which grants full privileges over other realms, such as `sylva`. Therefore, there is no need to log in to the `sylva` realm using `sylva-admin` credentials, as these do not provide administrative rights over `Keycloak` resources.

![keycloak-login](./img/keycloak-login.png)



## User Management

Users and groups are centralized within `Keycloak`. They can be created directly in `Keycloak` (as shown below) or imported from an external user federation source such as LDAP or AD.

> **_IMPORTANT NOTICE:_** Configuring external user federation is beyond the scope of Sylva. If external federation is configured, local `Keycloak` accounts (e.g., `sylva-admin`) may no longer be usable unless the federation allows `Keycloak` to write to the remote user directory. Granting such privileges to `Keycloak` could pose a security risk that should be carefully evaluated.

![keycloak-users](./img/keycloak-user.png)

![keycloak-groups](./img/keycloak-groups.png)

![keycloak-group-members](./img/keycloak-group-members.png)

### External Identity Provider

Configuring `Keycloak` to use an external identity provider, such as `Gitlab.com` or your company's IDP, can enhance user management. 

Below is an example configuration for integrating with `Gitlab.com`:

```json
{
  "realm": "sylva",
  "client_id": "4ac6........885",
  "client_secret": "a9f.........b19",
  "alias": "oidc",
  "authorization_url": "https://gitlab.com/oauth/authorize",
  "user_info_url": "https://gitlab.com/oauth/userinfo",
  "token_url": "https://gitlab.com/oauth/token",
  "default_scopes": "openid email profile",
  "display_name": "Gitlab",
  "extra_config": {
    "clientAuthMethod": "client_secret_post"
  }
}
```

![keycloak-idps](./img/keycloak-idp.png)

### User federation

Configuring `Keycloak` to use a user federation provider, such as `OpenLDAP`, can streamline user management. Below is an example of configuring `Keycloak` with a self-signed certificate for LDAPS:

#### Configure Keycloak with a Self-Signed Certificate for LDAPS

As Keycloak is a Java based application, a truststore file must be provided to the Java application in order to validate server certificates.

On Sylva, we create automatically this truststore and add the accurate configuration on Keycloak to use it.

Server certificate chain can be provided in your `values.yaml` file using this key:

```yaml
external_certificates:
  cacert: |
    # Add your certificate chain here
    -----BEGIN CERTIFICATE-----
    MIIC...
    -----END CERTIFICATE-----
```

### Configure Keycloak User Federation with LDAP

Below is an example configuration for an OpenLDAP server using TLS:

![keycloak-user-federation-ldaps-config](./img/keycloak-user-federation-ldaps-config.png)

Once the truststore is configured according to the LDAPS certificate, user synchronization can be performed:

![keycloak-ldaps-sync](./img/keycloak-ldaps-sync.png)

Keycloak server logs:

```bash
2024-01-31 16:55:25,466 INFO  [org.keycloak.storage.ldap.LDAPStorageProviderFactory] (executor-thread-41) Sync all users from LDAP to local store: realm: sylva, federation provider: ldap
2024-01-31 16:55:25,577 WARN  [org.keycloak.storage.ldap.LDAPStorageProvider] (executor-thread-41) Kerberos principal attribute not found on LDAP user [uid=adam,ou=people,dc=keycloak,dc=org]. Configured kerberos principal attribute name is [krb5PrincipalName]
2024-01-31 16:55:25,596 INFO  [org.keycloak.storage.ldap.LDAPStorageProviderFactory] (executor-thread-41) Sync all users finished: 1 imported users, 0 updated users
```

OpenLDAP server logs:

```bash
65ba7b7d conn=1022 fd=12 ACCEPT from IP=100.72.149.198:46550 (IP=0.0.0.0:636)
65ba7b7d conn=1022 fd=12 TLS established tls_ssf=256 ssf=256
65ba7b7d conn=1022 op=0 BIND dn="cn=admin,dc=keycloak,dc=org" method=128
65ba7b7d conn=1022 op=0 BIND dn="cn=admin,dc=keycloak,dc=org" mech=SIMPLE ssf=0
65ba7b7d conn=1022 op=0 RESULT tag=97 err=0 text=
65ba7b7d conn=1022 op=1 SRCH base="ou=people,dc=keycloak,dc=org" scope=2 deref=3 filter="(&(objectClass=posixAccount)(objectClass=account))"
65ba7b7d conn=1022 op=1 SRCH attr=entryUUID uid mail sn cn objectclass krb5PrincipalName modifyTimestamp createTimestamp
65ba7b7d conn=1022 op=1 SEARCH RESULT tag=101 err=0 nentries=1 text=
65ba7b7d conn=1022 op=2 UNBIND
65ba7b7d conn=1022 fd=12 closed
```

#### Example of a Lightweight LDAPS Deployment

To test user federation with an LDAPS server, you can use the following resources:

`deployment.yaml`

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: openldap
spec:
  replicas: 1
  serviceName: openldap
  selector:
    matchLabels:
      app.kubernetes.io/name: openldap
  template:
    metadata:
      labels:
        app.kubernetes.io/name: openldap
    spec:
      hostname: openldap
      containers:
        - name: openldap
          image: osixia/openldap:1.5.0
          imagePullPolicy: "Always"
          env:
            - name: LDAP_ORGANISATION
              value: "keycloak"
            - name: LDAP_DOMAIN
              value: "keycloak.org"
            - name: LDAP_ENABLE_TLS
              value: "yes"
            - name: LDAP_TLS_VERIFY_CLIENT
              value: "never"
            - name: LDAP_ADMIN_USERNAME
              value: "admin"
            - name: LDAP_ADMIN_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: adminpassword
                  name: openldap
            - name: LDAP_USERS
              valueFrom:
                secretKeyRef:
                  key: users
                  name: openldap
            - name: LDAP_PASSWORDS
              valueFrom:
                secretKeyRef:
                  key: passwords
                  name: openldap
          ports:
            - name: tcp-ldaps
              containerPort: 636
```

`svc.yaml`

```yaml
apiVersion: v1
kind: Service
metadata:
  name: openldap-0
  labels:
    app.kubernetes.io/name: openldap
spec:
  type: ClusterIP
  ports:
    - name: tcp-ldaps
      port: 636
      targetPort: tcp-ldaps
  selector:
    app.kubernetes.io/name: openldap
```

Create a secret for the OpenLDAP admin user and then apply the files:

```bash
kubectl --kubeconfig management-cluster-kubeconfig -n keycloak create secret generic openldap --from-literal=adminpassword=adminpassword --from-literal=users=user01,user02 --from-literal=passwords=password01,password02

kubectl --kubeconfig management-cluster-kubeconfig -n keycloak apply -f deployment.yaml

kubectl --kubeconfig management-cluster-kubeconfig -n keycloak apply -f svc.yaml
```

Add an organizational unit on the deployed OpenLDAP server:

```bash
cat people.ldif
dn: ou=people,dc=keycloak,dc=org
objectClass: top
objectClass: organizationalUnit
ou: people
root@openldap-0:/# ldapadd -x -D "cn=admin,dc=keycloak,dc=org" -w <yourpassword> -H ldap://127.0.0.1 -f people.ldif
```

Add a user to the previously created organizational unit:

```bash
cat adam.ldif
dn: uid=adam,ou=people,dc=keycloak,dc=org
objectClass: top
objectClass: account
objectClass: posixAccount
objectClass: shadowAccount
cn: adam
uid: adam
uidNumber: 16859
gidNumber: 100
homeDirectory: /home/adam
loginShell: /bin/bash
gecos: adam
userPassword: {crypt}x
shadowLastChange: 0
shadowMax: 0
root@openldap-0:/# ldapadd -x -D "cn=admin,dc=keycloak,dc=org" -w <yourpassword> -H ldap://127.0.0.1 -f adam.ldif
```

## Access Control to workload Clusters

While `Keycloak` plays a crucial role in user authentication to workload clusters, it's important to note that `Keycloak` only handles the authentication aspect. Authorization (defining who can do what) is managed at the OIDC client (e.g., `Rancher`) level. For instance, the following image shows Rancher binding the admin role to the group infra-admins:

![Rancher-role-binding](./img/role-binding.png)

You can manage the binding from the Rancher Web UI via the `sylva-admin` account, but note that in this case, the `sylva-admin` account must be a member of the Keycloak groups to bind. It is generally undesirable, and thus, group binding via the Rancher web UI is only recommended for testing purposes.

It is advisable to manage group binding via Rancher CRD (`globalrolebinding`, `projectroletemplatebindings`, and `clusterroletemplatebindings`) or even with the [Terraform Rancher provider](https://registry.terraform.io/providers/rancher/rancher2/latest/docs). 

Below are examples of CRD-based binding:

```yaml
apiVersion: management.cattle.io/v3
kind: GlobalRoleBinding
metadata:
  name: grb-infra-admins
  annotations:
    cleanup.cattle.io/grbUpgradeCluster: "true"
    lifecycle.cattle.io/create.mgmt-auth-grb-controller: "true"
  finalizers:
  - controller.cattle.io/mgmt-auth-grb-controller
globalRoleName: admin
groupPrincipalName: keycloakoidc_group://infra-admins
```

In summary, `Rancher` acts as an authentication proxy, enabling fine-grained access control based on `Keycloak` users/groups management as depicted below.

![sylva-rbac](./img/sylva-rbac.jpg)

> **_IMPORTANT SECURITY NOTICE:_** The Rancher administrator has access to the resources of the workload clusters, particularly to the volumes and to the Kubernetes secrets of the workload clusters. Consequently, **it is crucial to strictly control who is allowed to connect as an administrator to Rancher**.
