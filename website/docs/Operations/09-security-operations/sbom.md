---
sidebar_position: 5
title: SBOM
---

# Software Bill of Materials (SBOM)

The optional `sbom-operator` unit facilitates the creation of a Software Bill of Materials (SBOM) for Sylva. This unit deploys an [operator](https://artifacthub.io/packages/helm/ckotzbauer/sbom-operator) that generates the SBOM for each image used within a Sylva cluster.

By default, the SBOMs are exported to `configmaps`, which can be accessed using the following command:

```shell
kubectl get -A configmap -l ckotzbauer.sbom-operator.io
```

The `configmap` stores the content as `brotli` compressed binary data. The commands below demonstrate how to retrieve a specific SBOM:

```shell
kubectl get -n kyverno configmap kyverno-cleanup-admission-reports-28146180-l6jv2-cleanup -o jsonpath='{.binaryData.sbom}' | base64 -d > sbom.br
brotli -d sbom.br
```
Alternatively, SBOMs can be exported to a Git repository, an OCI registry, or to [Dependency Track](https://owasp.org/www-project-dependency-track/). 

## Dependency Track

Dependency Track is an OWASP tool that analyzes the components of SBOMs. To configure the `sbom-operator` to build and export SBOMs to an external Dependency Track server, modify the `values.yaml` file of the `sbom-operator` chart as follows:

```yaml
serviceAccount:
  create: true
  name: sbom-operator
securityContext:
   capabilities:
     drop:
     - ALL
   privileged: false
   readOnlyRootFilesystem: true
   runAsNonRoot: true
   runAsUser: 10001
   seccompProfile:
     type: RuntimeDefault
resources:
   limits:
     cpu: 500m
     memory: 500Mi
   requests:
     cpu: 100m
     memory: 100Mi
args:
  targets: dtrack
  verbosity: debug
  format: cyclonedx
  dtrack-base-url: https://dependency-track.example
  dtrack-api-key: xGNE...
envVars: # if using a proxy
      - name: https_proxy
        value: http://proxy.example:8080
      - name: http_proxy
        value: http://proxy.example:8080
      - name: no_proxy
        value: localhost,127.0.0.0/8,192.168.0.0/16,172.16.0.0/12,10.0.0.0/8,172.20.36.130,keycloak.sylva,.cluster.local,.cluster.local.,.svc
```

Dependency Track displays all imported SBOMs and provides a summary of vulnerabilities for each:

![dt-projects](./img/dt-projects.png)

For each image, you can view the list of components (software and packages) and check for vulnerabilities:

![dt-components](./img/dt-component-vulns.png)
