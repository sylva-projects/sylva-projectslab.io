---
sidebar_position: 6
title: Vault
---

Vault is responsible for securely managing secrets and passwords for various applications such as `Keycloak`, `Rancher`, `flux-webui`, and `Neuvector`. Authentication to Vault can be done using either the OIDC method via Keycloak or by utilizing the Vault Root token.

## Authentication

### Authentication Privileges Table

OIDC authentication offers restricted access compared to the Vault Root token, which provides full privileges over Vault resources. The table below outlines the variations in permissions:

| Privilege                                                                        | OIDC Authentication | Root Token Authentication |
| -------------------------------------------------------------------------------- | ------------------- | ------------------------- |
| Read Secret                                                                      | Yes                 | Yes                       |
| Update and Create Secret                                                         | **No**                  | Yes                       |
| Manage Secrets Engines                                                           | **No**                  | Yes                       |
| Create, Update, and Delete Auth Methods                                          | Yes                 | Yes                       |
| Create and Manage System Policies                                                | Yes                 | Yes                       |
| Read System Health Check                                                         | Yes                 | Yes                       |
| Manage System Backend (Audit, Config, etc.)                                      | **No**                  | Yes                       |


### OIDC Authentication

> **_NOTE:_** If the `.admin_password` is not provided in the  `secrets.yaml` environment file, you must connect to Vault using the Vault Root token. This will allow you to retrieve the randomly generated password for the SSO account from the vault path `/secret/sso-account`. You can refer to [this method](#root-token-authentication) for more information on how to do this.

To authenticate using OIDC, please refer to the visual guides provided below.

Upon accessing the UI, choose the `OIDC Method` and click on `Sign in with OIDC Provider`.
![vault-login-oidc](./img/vault-oidc.png)

Then, proceed to connect to Keycloak using the SSO account.
![vault-oidc-redirection](./img/vault-oidc-redirect.png)

### Root Token Authentication

Retrieve the Vault token using the command below:

```shell
kubectl -n vault get secret vault-unseal-keys -o jsonpath='{.data.vault-root}' | base64 -d
```

![vault-login-token](./img/vault-token.png)

## Local Accounts

Once authenticated, retrieve the local account passwords for each management cluster service either from the Vault GUI or the Vault CLI.

### Retrieve Secrets from Vault GUI

Access and manage secrets through the Vault GUI as shown:

![vault-secrets](./img/vault-paths.png)

### Retrieve Secrets from Vault CLI

Use the following commands to interact with Vault via the CLI:

```shell
export VAULT_TOKEN=$(kubectl -n vault get secret vault-unseal-keys -o jsonpath='{.data.vault-root}' | base64 -d)
kubectl -n vault get secret vault-tls -o jsonpath='{.data.ca\.crt}' | base64 -d > vault-ca.crt
export VAULT_ADDR=https://vault.sylva
export VAULT_CACERT=$PWD/vault-ca.crt
```

List available secrets:

```shell
$ vault kv list secret
Keys
----
flux-webui
keycloak
rancher
sso-account
```

Retrieve specific secret details:

```shell
$ vault kv get secret/rancher
=== Secret Path ===
secret/data/rancher

======= Metadata =======
Key                Value
---                -----
created_time       2023-06-09T13:49:58.903341566Z
custom_metadata    <nil>
deletion_time      n/a
destroyed          false
version            1

============ Data ============
Key                      Value
---                      -----
bootstrapPassword        <..bla bla..>
```
