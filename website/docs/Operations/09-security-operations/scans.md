---
sidebar_position: 3
title: Security Scans
---

## RKE2 Security Scan

Sylva includes the Kubernetes operator [cis-operator](https://github.com/rancher/cis-operator), which enables RKE2 security scans against the CIS benchmark:

```shell
$ kubectl get clusterscans
NAME                CLUSTERSCANPROFILE              TOTAL   PASS   FAIL   SKIP   WARN   NOT APPLICABLE   LASTRUNTIMESTAMP   
rke2-cis-hardened   rke2-cis-1.6-profile-hardened   122     82     4      0      29     7                2023-04-28T14:45:22Z   
```

### Fetch Scan Reports with kubectl

Obtain scan summaries using the command `kubectl get clusterscans -o yaml`. For more detailed scan reports, use the following `kubectl` commands:

```shell
$ kubectl get clusterscanreports
NAME                                  LASTRUNTIMESTAMP                                            BENCHMARKVERSION
scan-report-rke2-cis-hardened-drnh8   2022-10-28 14:45:40.019124438 +0000 UTC m=+2336.836186254   rke2-cis-1.6-hardened   
```

The report is formatted in JSON:

```shell
$ kubectl get clusterscanreport scan-report-rke2-cis-hardened-drnh8 -o jsonpath="{.spec.reportJSON}" | jq
{
  "version": "rke2-cis-1.6-hardened",
  "total": 122,
  "pass": 82,
  "fail": 4,
  "skip": 0,
  "warn": 29,
  "notApplicable": 7,
....
```

You can convert this JSON file into a human-readable report using the Python script `display-scan-report.py`, available [here](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/tools/display-scan-report.py), as follows:

```shell
$ kubectl get $(kubectl get clusterscanreports -o name) \
-o jsonpath="{.spec.reportJSON}" \
| ./display-scan-report.py
```

### Getting Scan Reports from Rancher

![ClusterScanReport](./img/cis-report.png)

![cis-result](./img/cis-result.png)

## Kubeadm Security Scan

Initiate a security scan on a `kubeadm` cluster by enabling the `trivy-operator` unit in your environment file `values.yaml`:

```yaml
units:
  trivy-operator:
    enabled: yes
```

The `trivy-operator` includes the CRD `clustercompliancereports`, which provides reports against various benchmarks:

```shell
$ kubectl get clustercompliancereports
NAME             AGE
cis              4h4m
nsa              4h4m
pss-baseline     4h4m
pss-restricted   4h4m
```

Select the CIS benchmark:

```shell
kubectl get clustercompliancereports cis -o yaml
```

## Vulnerabilities Check

### Trivy-Operator

To check vulnerabilities in **running** images, enable the deployment of the trivy-operator in your `environment-values` file `values.yaml`:

```yaml
units:
  trivy-operator:
    enabled: yes
```

List the vulnerability reports and summarize them with the command:

```shell
kubectl get -A vulnerabilityreports -o wide
```

Focus on a specific report for detailed analysis:

```shell
$ kubectl get -n calico-system vulnerabilityreport replicaset-calico-typha-89798ff7b-calico-typha -o wide
NAME                                             REPOSITORY                      TAG       SCANNER   AGE   CRITICAL   HIGH
replicaset-calico-typha-89798ff7b-calico-typha   rancher/mirrored-calico-typha   v3.25.0   Trivy     12m   1          1  
```

```shell
kubectl get -n calico-system vulnerabilityreport replicaset-calico-typha-89798ff7b-calico-typha -o yaml
```

The report will appear as follows:

```yaml
report:
  artifact:
    repository: rancher/mirrored-calico-typha
    tag: v3.25.0
  registry:
    server: index.docker.io
  scanner:
    name: Trivy
    vendor: Aqua Security
    version: 0.40.0
  summary:
    criticalCount: 1
    highCount: 1
    lowCount: 0
    mediumCount: 0
    noneCount: 0
    unknownCount: 0
  updateTimestamp: "2023-07-13T12:30:06Z"
  vulnerabilities:
  - fixedVersion: 2.16.0+incompatible
    installedVersion: v2.11.2-0.20200112161605-a7c079c43d51+incompatible
    links: []
    primaryLink: https://avd.aquasec.com/nvd/cve-2022-1996
    resource: github.com/emicklei/go-restful
    score: 9.1
    severity: CRITICAL
    target: ""
    title: Authorization Bypass Through User-Controlled Key
    vulnerabilityID: CVE-2022-1996
  - fixedVersion: 0.7.0
    installedVersion: v0.4.0
    links: []
    primaryLink: https://avd.aquasec.com/nvd/cve-2022-41723
    resource: golang.org/x/net
    score: 7.5
    severity: HIGH
    target: ""
    title: Avoid quadratic complexity in HPACK decoding
    vulnerabilityID: CVE-2022-41723
```

> **_Note_** that the trivy-operator includes, in addition to the vulnerability assessment, a variety of audit and compliance reports that are worth considering:

```shell
$ kubectl api-resources | grep aqua
clustercompliancereports                     compliance                         aquasecurity.github.io/v1alpha1            false        ClusterComplianceReport
clusterconfigauditreports                    clusterconfigaudit                 aquasecurity.github.io/v1alpha1            false        ClusterConfigAuditReport
clusterinfraassessmentreports                clusterinfraassessment             aquasecurity.github.io/v1alpha1            false        ClusterInfraAssessmentReport
clusterrbacassessmentreports                 clusterrbacassessmentreport        aquasecurity.github.io/v1alpha1            false        ClusterRbacAssessmentReport
configauditreports                           configaudit,configaudits           aquasecurity.github.io/v1alpha1            true         ConfigAuditReport
exposedsecretreports                         exposedsecret,exposedsecrets       aquasecurity.github.io/v1alpha1            true         ExposedSecretReport
infraassessmentreports                       infraassessment,infraassessments   aquasecurity.github.io/v1alpha1            true         InfraAssessmentReport
rbacassessmentreports                        rbacassessment,rbacassessments     aquasecurity.github.io/v1alpha1            true         RbacAssessmentReport
vulnerabilityreports                         vuln,vulns                         aquasecurity.github.io/v1alpha1            true         VulnerabilityReport
```
