---
sidebar_position: 4
title: NeuVector
---

# SUSE NeuVector

The optional unit 'neuvector' addresses the platform's Zero Trust Security by providing vulnerability management, compliance, runtime security, supply chain security, network visibility, and container segmentation. For further details, please refer to the [SUSE NeuVector Official Documentation](https://www.suse.com/products/neuvector/).

The image CVE scanning capability is disabled by default to conserve resources. To enable this feature, set the environment variable `.Values.security.neuvector_scanning_enabled` to true in your `values.yaml` file:

```yaml
security:
  neuvector_scanning_enabled: true
```

## Access to NeuVector Admin Console

To administer NeuVector, access its Web UI at `https://neuvector.<cluster_domain_name>`, e.g., https://neuvector.sylva.

![neuvector-login](./img/neuvector-login.png)

### OIDC Authentication

To log in with OIDC, click on the "Login with OpenID" button.

![neuvector-oidc-redirection](./img/neuvector-oidc-redirect.png)

Then enter your personal identifier and password. Users must belong to the `infra-admins` group in Keycloak to modify the configuration, or to the `neuvector-readers` group for read-only access.

### Local Authentication

To log in with a local account, click on the "Login" button. The only account provisioned is `admin`. Retrieve the password from Vault under the path `secret/neuvector`. Use this account only in emergency cases when OIDC authentication is unavailable.

## NeuVector Dashboard

![neuvector-dashboard](./img/neuvector-dashboard.png)

From the NeuVector dashboard, you can:

- Visualize network activity
- Manage assets
- Manage policies
- Manage security risks
- Manage notifications
- Adjust NeuVector settings

### Network Activity

![neuvector-network-activity](./img/neuvector-network-activity.png)

The view can be filtered by criteria such as namespaces to enhance readability. This view highlights unexpected network flows that may indicate malicious behavior.

### Assets Management

From the 'Assets' management menu, you can:

- Visualize platforms registered on NeuVector and launch security scans
- View namespaces and their resources (workloads, pods, services)
- View nodes and access results of their compliance checks and vulnerabilities
- View containers and their states

### Policies Management

From the 'Policy' management menu, you can:

- Add, update, or remove admission control policies
- Add, update, or remove group policies
- Add, update, or remove network policies

### Security Risks Management

From the 'Security Risks' management menu, you can manage:

- Vulnerabilities and vulnerability profiles
- Compliance and compliance profiles

### Notification Management

From the 'Notifications' management menu, you can display:

- Security events, including violations, threats, and incidents
- Risk reports
- Events

Notifications are stored in memory with a rolling limit of 4K per type and sub-type.

**It is recommended to forward these security events to a SIEM system** by configuring the Syslog feature under Settings > Configuration > Syslog.

## NeuVector Modes

NeuVector offers three distinct scanning modes for resources:

- **Discover**: In this mode, NeuVector discovers the infrastructure, services, and applications, automatically building a whitelist of network rules.
- **Monitor**: NeuVector monitors runtime violations of security events and raises security alerts.
- **Protect**: NeuVector blocks any detected network violations and attacks.

Resources are kept in discover mode by default. Manual action is required to switch them to monitor mode. It is advisable to switch to monitor mode quickly and, after a reasonable observation period, to protect mode. Note that the protect mode can cause denial of service if resource control policies are not well-defined.
