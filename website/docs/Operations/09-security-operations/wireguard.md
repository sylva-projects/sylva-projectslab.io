---
sidebar_position: 7
title: Wireguard
---

# Wireguard

The optional feature 'wireguard' of `calico` is a VPN that utilizes state-of-the-art cryptography. It is designed as a general purpose VPN for running on embedded interfaces and super computers alike, fit for many different circumstances. For more information, see [Wireguard Official Documentation](https://www.wireguard.com/).

It is disabled by default as *network performance with this feature has not been tested*. To enable this feature, set the environment variable `.Values.security.calico_wireguard_enabled` to `true` in your `values.yaml` file:

```yaml
security:
  calico_wireguard_enabled: true
```

