---
title: Node Maintenance
---

## Performing Node Maintenance

There are various scenarios where you may want to perform a node maintenance activity such as:

- When upgrading firmware
- When rebooting the node for any reason

While performing any of these operations, it is possible that the cluster accidentally breaks. To avoid such situation, follow the steps given here:

1. To ensure that the cluster is healthy while you are performing node maintenance activity, you must first make that node unschedulable.

```shell
kubectl cordon <node_name>
```

2. Now, drain the workloads running on that node to other nodes. To drain the node, run the following commands:

```shell
kubectl drain <node_name>  --ignore-daemonsets --delete-emptydir-data
```

- The `--ignore-daemonsets` flag is used to specify that the eviction process should not affect pods managed by DaemonSets during node maintenance or other operations.

- The `--delete-emptydir-data` flag is used to specify that data from `emptyDir` volumes associated with pods being evicted should be deleted during the node drain operation.

3. Now, stop the Kubernetes process running on the node. Run either of the following commands:

- If your node is a control-plane(server) node, run:

```shell
systemctl stop rke2-server
```

- If your node is a worker(agent) node, run:

```shell
systemctl stop rke2-agent
```

4. Now, proceed with your node maintenance activity. Once it is completed, reboot the node either by running `sudo reboot` or refer to [`Platform Shutdown` procedure](/docs/Operations/maintenance-operations/platform-shutdown) if you need the node stopped and not only restarted (important for Baremetal infrastructure). 

5. Once the node is rebooted, make sure the rke2 service is started. In case the service is not already up, you could try running either of the following commands:

- If your node is a control-plane(server) node, run:

```shell
systemctl start rke2-server
```

- If your node is a worker(agent) node, run:

```shell
systemctl start rke2-agent
```

6. Once the rke2 service is started, you must uncordon the node to ensure Kubernetes can now schedule workloads on this node. To uncordon, run the following command:

```shell
kubectl uncordon <node_name>
```
