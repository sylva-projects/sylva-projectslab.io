---
title: Shutdown/Start Sylva platform
sidebar_label: Shutdown/Start Sylva platform
---

## Overview

It might be necessary to shut down entirely a Sylva platform (e.g. reduce power and/or resource usage on the laboratory or pre-production platform).

Make sure the platform is in a stable state before shutting it down. Follow [this validation procedure](/docs/Install/validate/) to confirm its status. 

### OpenStack cluster

* Make sure all nodes are up and running and get a list of the current nodes for the clusters you are planning to apply this procedure. 

```shell
kubectl get nodes 
```
* The nodes' power off/on functionalities are made available through OpenStack API. 
* For all the nodes (virtual machines) that you want to shutdown run:

```shell
openstack server stop <server-id>
```
or
```shell
shutdown [-h|-r]
```
* For starting a node, run : 
```shell
openstack server start <server-id>
```

#### Baremetal cluster

To shutdown baremetal nodes we are using the metal3 feature to shutdown a node using `reboot` annotation (https://book.metal3.io/bmo/reboot_annotation) as their statuses are being controlled by the metal3 (BareMetalOperator). Otherwise, if we just try to shut down the baremetal nodes using ipmi, metal3 controller would try to start them again as they are defined with the `online: true` property in the `bmh_spec`. 

Management cluster is managing all the nodes of all the clusters, workload or management. Therefore, all workload clusters nodes can be powered off and powered on from the management cluster, but to shutdown and restart management cluster baremetal nodes, the process is slightly different: we are using the same reboot annotation feature, except for the last control-plane node. Otherwise having the annotation on all nodes would prevent them to restart again as we lose metal3 management over them. 

For the last control-plane node, use what you have available as a remote power management tool (ex: ipmi, redfish). For both commands you need to have the host ip , user and password used for authentication. Also ipmitool requires the interface name as configured on the server and redfish requires the authentication token that can be generated with this command : 
```shell
curl -k -D - -X POST 'https://<ip[:port]>/redfish/v1/SessionService/Sessions' -H "Content-Type: application/json" -d '{ "UserName": "<user>", "Password": "<password>" }' 
```

## Workload cluster

* Get the namespace of the workload cluster for which you want to apply the procedure below.

```shell
kubectl get ns
```

### Shutdown workload cluster baremetal nodes

* Get the list of the baremetal hosts.

```shell
kubectl get bmh -n <workload-cluster-namespace>
```
* Shutdown it down by adding the annotation for each baremetal host name:

```shell
kubectl annotate --overwrite bmh -n <workload-cluster-namespace> <baremetal-host-name> reboot.metal3.io/key='{"mode":"soft"}'
```

### Start workload cluster baremetal nodes

* To start the baremetal nodes we have to remove the `reboot.metal3.io` annotation. 

```shell
kubectl annotate --overwrite bmh -n <workload-cluster-namespace> <baremetal-host-name> reboot.metal3.io/key-
```
* You can check the power status of the bmh host using the command below. It should return `true` when the baremetal host is properly restarted.

```shell
kubectl get bmh -n <workload-cluster-namespace> <baremetal-host-name> -o json | jq .status.poweredOn
```

## Management cluster

* Get the namespace of the management cluster. Usually, it is `sylva-system`.

```shell
kubectl get ns
```

### Shutdown management cluster baremetal nodes

* Get the list of the baremetal hosts.

```shell
kubectl get bmh -n <management cluster namespace>
```
* Shutdown the baremetal hosts, except one by adding the annotation below

```shell
kubectl annotate --overwrite bmh -n <management cluster namespace> <baremetal-host-name> reboot.metal3.io/key='{"mode":"soft"}'
```
* On the last baremetal host powered it off:

```shell
ipmitool -H <IP> -I <interface> -U <user> -P <password> chassis power off
```
```shell
curl -k -X POST https://<ip[:port]>/redfish/v1/Systems/<system -id>/Actions/ComputerSystem.Reset -H 'X-Auth-Token: <authtoken>' -H "Content-Type: application/json" -d '{"ResetType": "GracefulShutdown"}'
```

### Start management cluster baremetal nodes

* On the last stopped baremetal host powered it on: 

```shell
ipmitool -H <IP> -I <interface> -U <user> -P <password> chassis power on
```
```shell
curl -k -X POST https://<ip[:port]>/redfish/v1/Systems/<system -id>/Actions/ComputerSystem.Reset -H 'X-Auth-Token: <authtoken>' -H "Content-Type: application/json" -d '{"ResetType": "On"}'
```
* Start the baremetal nodes except the previous one by removing the annotation.

```shell
kubectl annotate --overwrite bmh -n <management cluster namespace> <baremetal-host-name> reboot.metal3.io/key-
```
* Confirm Kubernetes is up and running, and check the nodes' status.

```shell
kubectl get bmh -n <management cluster namespace>
```

:::warning
Do not apply any other changes, like `cordon` or `taint`, to the kubernetes nodes for the shutdown/restart procedure. Apply the above steps with great care.
:::
 