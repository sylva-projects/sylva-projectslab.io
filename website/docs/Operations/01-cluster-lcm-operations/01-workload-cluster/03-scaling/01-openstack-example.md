---
title: Openstack Example
description: Scaling operations on an Openstack Workload Cluster
---

### Scale-out an OpenStack workload cluster

In this example, we will start with a small workload cluster (1 control plane node and 1 worker node) and we will add a second worker node.

* Initial state (1 control plane and 1 worker):

```shell
kubectl get nodes -o wide
NAME                                             STATUS   ROLES                       AGE     VERSION          INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
first-workload-cluster-cp-6b7c7c5432-ftx4c       Ready    control-plane,etcd,master   25h     v1.26.9+rke2r1   192.168.129.171   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1
first-workload-cluster-md-md0-efa450d0ad-znfn4   Ready    <none>                      3h29m   v1.26.9+rke2r1   192.168.128.122   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1

```
* Modify the number of replicas into the `values.yaml` file and apply the configuration based on the previous steps.

```yaml
cluster:
  control_plane_replicas: 1
  machine_deployments:
    md0:
      replicas: 2
```

Then:

```
./apply-workload-cluster.sh environment-values/workload-clusters/$WORKLOAD_CLUSTER

```
* Wait until the new configuration is applied.

```shell
kubectl get nodes -o wide
NAME                                             STATUS   ROLES                       AGE     VERSION          INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
first-workload-cluster-cp-6b7c7c5432-ftx4c       Ready    control-plane,etcd,master   26h     v1.26.9+rke2r1   192.168.129.171   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1
first-workload-cluster-md-md0-efa450d0ad-znfn4   Ready    <none>                      3h51m   v1.26.9+rke2r1   192.168.128.122   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1
first-workload-cluster-md-md0-efa450d0ad-r6d7c   Ready    <none>                      59s     v1.26.9+rke2r1   192.168.129.221   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1

```

### Scale-in an OpenStack workload cluster
In this example, we will start with a workload cluster that has 1 control plane node and 2 worker nodes, and we will remove a worker node.

* Initial state (1 control plane and 2 workers):

```shell
kubectl get nodes -o wide
NAME                                             STATUS   ROLES                       AGE     VERSION          INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
first-workload-cluster-cp-6b7c7c5432-ftx4c       Ready    control-plane,etcd,master   26h     v1.26.9+rke2r1   192.168.129.171   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1
first-workload-cluster-md-md0-efa450d0ad-znfn4   Ready    <none>                      3h51m   v1.26.9+rke2r1   192.168.128.122   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1
first-workload-cluster-md-md0-efa450d0ad-r6d7c   Ready    <none>                      59s     v1.26.9+rke2r1   192.168.129.221   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1

```

* Modify the number of replicas into the `values.yaml` file and apply the configuration based on the previous steps.

```yaml
cluster:
  control_plane_replicas: 1
  machine_deployments:
    md0:
      replicas: 1
```

Then:

```
./apply-workload-cluster.sh environment-values/workload-clusters/$WORKLOAD_CLUSTER

```
* Wait until the new configuration is applied.

```shell
kubectl get nodes -o wide
NAME                                             STATUS   ROLES                       AGE     VERSION          INTERNAL-IP       EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
first-workload-cluster-cp-6b7c7c5432-ftx4c       Ready    control-plane,etcd,master   27h     v1.26.9+rke2r1   192.168.129.171   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1
first-workload-cluster-md-md0-efa450d0ad-znfn4   Ready    <none>                      4h      v1.26.9+rke2r1   192.168.128.122   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1
first-workload-cluster-md-md0-efa450d0ad-r6d7c   Ready,SchedulingDisabled   <none>    9m      v1.26.9+rke2r1   192.168.129.221   <none>        Ubuntu 22.04.3 LTS   5.15.0-87-generic   containerd://1.7.3-k3s1

```