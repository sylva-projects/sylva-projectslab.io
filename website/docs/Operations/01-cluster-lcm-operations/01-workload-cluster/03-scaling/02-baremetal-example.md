---
title: Baremetal Example
description: Scaling operations on a Baremetal Workload Cluster
---

### Scale-out a baremetal workload cluster

In this example, we will start with a small workload cluster (1 control plane node) and we will add a worker node.

- Initial State (1 control plane)

```shell
$ kubectl --kubeconfig workload-cluster-kubeconfig get nodes -o wide
NAME                                STATUS   ROLES                       AGE     VERSION           INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
my-rke2-capm3-control-plane-2qrrb   Ready    control-plane,etcd,master   4h30m   v1.26.9+rke2r1    172.20.36.142   <none>        Ubuntu 22.04.3 LTS   5.15.0-92-generic   containerd://1.7.3-k3s1
```

- Modify the number of replicas (according to your requirement) into the `values.yaml` file:

```yaml
cluster:
  machine_deployments:
    md0:
      replicas: 1
      capm3:
        hostSelector:
          matchLabels:
            cluster-role: worker  # must match a label defined in `baremetal_hosts.x.bmh_metadata.labels` to associate a BMH
```

- Then, apply the configurations:

```shell
$ ./apply-workload-cluster.sh environment-values/workload-clusters/$WORKLOAD_CLUSTER
```

- Wait until the new configuration is applied:

```shell
$ kubectl --kubeconfig workload-cluster-kubeconfig get nodes -o wide
NAME                                STATUS   ROLES                       AGE     VERSION           INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
my-rke2-capm3-control-plane-2qrrb   Ready    control-plane,etcd,master   4h34m   v1.26.9+rke2r1    172.20.36.142   <none>        Ubuntu 22.04.3 LTS   5.15.0-92-generic   containerd://1.7.3-k3s1
my-rke2-capm3-md0-zcs5d-2sq2d       Ready    <none>                      152m    v1.26.11+rke2r1   172.20.36.143   <none>        Ubuntu 22.04.3 LTS   5.15.0-92-generic   containerd://1.7.7-k3s1
```

### Scale-in a baremetal workload cluster

In this example, we will start with a workload cluster that has 1 control plane node and 2 worker node, and we will remove a worker node.

- Initial State (1 control plane, 2 worker node)

```shell
$ kubectl --kubeconfig workload-cluster-kubeconfig get nodes -o wide
NAME                     STATUS   ROLES                       AGE     VERSION          INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
my-rke2-capm3-dl360-35   Ready    control-plane,etcd,master   4h22m   v1.28.8+rke2r1   172.20.36.145   <none>        Ubuntu 22.04.4 LTS   5.15.0-101-generic   containerd://1.7.11-k3s2
my-rke2-capm3-dl360-36   Ready    <none>   3h13m   v1.28.8+rke2r1   172.20.36.143   <none>        Ubuntu 22.04.4 LTS   5.15.0-101-generic   containerd://1.7.11-k3s2
my-rke2-capm3-dl360-30   Ready    <none>   3h7m   v1.28.8+rke2r1   172.20.36.144   <none>        Ubuntu 22.04.4 LTS   5.15.0-101-generic   containerd://1.7.11-k3s2
```

- Check the baremetal server corresponding to the node we wish to delete (since node's name is same as baremetalhost's name, we can easily identify the server):

```shell
$ export KUBECONFIG=management-cluster-kubeconfig
$ kubectl get machine -n $WORKLOAD_CLUSTER
NAMESPACE       NAME                                     CLUSTER              NODENAME                                 PROVIDERID                                                                        PHASE     AGE     VERSION
my-rke2-capm3   my-rke2-capm3-control-plane-jt257        my-rke2-capm3        my-rke2-capm3-dl360-36                   metal3://my-rke2-capm3/my-rke2-capm3-dl360-36/my-rke2-capm3-cp-87f78f1fa0-zcmss   Running   4h31m   v1.28.8
my-rke2-capm3   my-rke2-capm3-md0-lgdfh        my-rke2-capm3        my-rke2-capm3-dl360-35                   metal3://my-rke2-capm3/my-rke2-capm3-dl360-35/my-rke2-capm3-md-md0-87f78f1fa0-tv4q8   Running   3h10m   v1.28.8
my-rke2-capm3   my-rke2-capm3-md0-ndd75        my-rke2-capm3        my-rke2-capm3-dl360-30                   metal3://my-rke2-capm3/my-rke2-capm3-dl360-30/my-rke2-capm3-md-md0-87f78f1fa0-698vs   Running   3h3m    v1.28.8

$ kubectl get bmh -n $WORKLOAD_CLUSTER
NAMESPACE       NAME                     STATE         CONSUMER                            ONLINE   ERROR   AGE
my-rke2-capm3   my-rke2-capm3-dl360-35   provisioned   my-rke2-capm3-cp-87f78f1fa0-tv4q8   true             23h
my-rke2-capm3   my-rke2-capm3-dl360-36   provisioned   my-rke2-capm3-md-md0--87f78f1fa0-zcmss   true             23h
my-rke2-capm3   my-rke2-capm3-dl360-30   provisioned   my-rke2-capm3-md-md1-87f78f1fa0-698vs   true             23h
```

- After identifying the baremetal server we wish to delete, apply the cluster.x-k8s.io/delete-machine annotation to the machine resource (while scaling down the cluster, the kubernetes cluster API will prioritize deleting the annotated node):

```shell
$ kubectl annotate machine my-rke2-capm3-dl360-30 -n $WORKLOAD_CLUSTER cluster.x-k8s.io/delete-machine=yes
```

- We can now decrease the number of replicas and remove details for the baremetal server corresponding to the machine we deleted in values.yaml and secrets.yaml.

- Wait until the new configuration is applied.

```shell
$ kubectl --kubeconfig workload-cluster-kubeconfig get nodes -o wide

NAME                     STATUS   ROLES                       AGE     VERSION          INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION       CONTAINER-RUNTIME
my-rke2-capm3-dl360-35   Ready    control-plane,etcd,master   5h16m    v1.28.8+rke2r1   172.20.36.145   <none>        Ubuntu 22.04.4 LTS   5.15.0-101-generic   containerd://1.7.11-k3s2
my-rke2-capm3-dl360-36   Ready    <none>   5h8m   v1.28.8+rke2r1   172.20.36.143   <none>        Ubuntu 22.04.4 LTS   5.15.0-101-generic   containerd://1.7.11-k3s2
```