---
title: Workload Cluster Addition
sidebar_label: Addition
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

This procedure explains the addition of a new workload cluster, assuming the management cluster is already up and running. This procedure can be used to create multiple workload clusters based on **OpenStack** or **Metal3** (baremetal) infrastructure providers.

## Prerequisites

### Workload interconnection with Management-cluster

:::info
Understanding the [flow matrix](/docs/Operations/network-operations/flow-matrix) helps to properly configure workload clusters.
:::

Requires to have IPv4 interconnection between the workload cluster(s) and the management cluster management networks allowing the required flows.  

If the management cluster is behind a NAT (e.g. on top of OpenStack using a floating IP), its external IP must be set in the workload-cluster(s) definition `values.yaml` using `.cluster.mgmt_cluster_ip: a.b.c.d` (cf. [sylva-capi-cluster](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster)).

<Tabs groupId="infraproviders">

<TabItem value="openstack" label="Openstack">

:::note
[Management cluster network requirement](/docs/Install/prerequisites/management-requirements)
:::

</TabItem>

<TabItem value="bm" label="BareMetal"> 

:::note
[Management cluster network requirement](/docs/Install/prerequisites/management-requirements)
:::

The management cluster must reach :
- **the BMC interfaces of workload-cluster BM hosts**.
- **the BM hosts provisioning interface(s)** during the provisioning step.

The workload cluster BM hosts must reach :
- **DHCP server** from their provisioning interface during the provisioning step.

</TabItem>
</Tabs>

### Infrastructure

<Tabs groupId="infraproviders">

<TabItem value="openstack" label="Openstack">

#### Tenant requirements for Workload Cluster

:::info
To deploy a single workload clusters with 3 replicas. 
:::

If the tenant is shared with the management cluster, the tenant quotas need to be adapted in regard to the workload needs. If a quota is not mentioned, it means that the default value is enough (no interest to modify it).

- instances (10: default value) - this default can be changed to scale or deploy multiple workload clusters. The usual minimal instances number is 5 (3 control nodes + 2 worker nodes)
- volumes (20) - depends on user Persistent Volume Claims (PVC) number and instances number. It should be larger than the number of instances + 3 (control volumes) + number of user PVC
- gigabytes (2000) - depends on the number of instances and workload PVC sizes
- cores (64) - 64 vCPU or more, depends on the flavors used
- RAM (128000) - 128GB or more, depending on the flavors used
- networks (10: default value) - if numerous different networks are used on pods with Multus, it can be useful to increase this value.
- ports (50: default value) - if numerous workers with Multus are used, it can be useful to increase this value.
- floating-ips (1) - a floating IP is absolutely needed when the MC is on a public network and the WC on a private network. It may also be needed in other cases.

#### Workload cluster deployment on a different platform

In case the workload cluster is deployed on a **distinct OpenStack platform**:

1. Create the network for the workload cluster
2. Create the base (Glance) image on OpenStack on the other Sylva platform.
3. Create a key pair for the tenant on the other Sylva platform .

</TabItem>

<TabItem value="bm" label="BareMetal"> 

#### Bare-Metal infrastructure requirements


For no-HA workload cluster (minimal configuration), **one bare-metal server is required**; acting as K8s controller and K8s worker
For HA workload cluster, at least **three servers** are required. 

Each server with controller (master), worker and storage (Longhorn) roles requires the following minimal resources:
 - 24 CPU cores (48 vCPU with HT)
 - 64GB RAM
 - 240GB flash boot disk (SSD)
 - 1x Dual 25Gbps NIC (additional NIC is recommended to handle additional Multus interfaces)
 - Additional dedicated storage device (SSD recommended) if Longhorn is activated on the node
 - Additional NIC(s) are requested for SRIOV worker (1 per NUMA)

The same network prerequisites are required than for deploying a management cluster on Bare-Metal, please refer to the [related section](/docs/Install/prerequisites/management-requirements).

</TabItem>
</Tabs>

## Installation Steps

To create a new workload cluster, follow the below steps.

### Prepare environment files

From bootstrap VM, all the commands need to be run from the `sylva-core/` directory.

What is interesting vs. the management cluster guide, is that for workload clusters we can select out of multiple K8s versions.

:::caution
OS images for deploying workload cluster nodes (regardless of the `baremetal` or `openstack` infrastructure type) are controlled from the management cluster. 

You select possible image from the [Sylva container registry](https://gitlab.com/sylva-projects/sylva-elements/diskimage-builder/container_registry).

1. Enable these images in your `values.yaml` **of the management cluster**, adding (one or both):

```yaml
# sylva-core/environment-values/current-environment-values/values.yaml

sylva_diskimagebuilder_images:
 ubuntu-jammy-plain-rke2-x-y-z: #CHANGE ME
    enabled: true

 ubuntu-jammy-plain-kubeadm-x-y-z: #CHANGE ME
    enabled: true
```

2. Use `apply.sh` script to update your management cluster:

```shell
./apply.sh environment-values/current-environment-values
```

:::caution
If you plan to perform a baremetal workload cluster deployment from a management cluster deployed on Openstack, you must ensure that the values of the management cluster have the following units enabled:

```yaml
units:
   capm3:
      enabled: true
```

If it's not the case, you'll have to add those values in the description file of the management cluster and update the management cluster with:

```shell
./apply.sh environment-values/current-environment-values
```
:::

### Prepare configuration files

Sample values files are available in the directory `environment-values/workload-clusters`,  choose the appropriate sample and copy it into your own directory e.g.:

```shell
cp -r environment-values/workload-clusters/bootprov-infraprov environment-values/workload-clusters/my-wc-bootprov-infraprov
```

Inside the new directory, you will find `values.yaml` and `secrets.yaml` files for the workload cluster where you will need to define your deployment parameters.


### Trigger the workload cluster deployment

:::note
For workload cluster deployed on baremetal, we need to increase the reconcile timeout value as node introspection and provisioning takes time
:::

```shell
# workload cluster on OpenStack VMs
./apply-workload-cluster.sh ./environment-values/workload-clusters/my-wc-bootprov-infraprov

# workload cluster on baremetal
APPLY_WC_WATCH_TIMEOUT_MIN=120 ./apply-workload-cluster.sh ./environment-values/workload-clusters/my-wc-bootprov-infraprov
```

## Tips

:::tip 1. install multiple workload clusters

```shell
# fill in values.yaml and secrets.yaml for each cluster
# and deploy the workload clusters
./apply-workload-cluster.sh ./environment-values/workload-clusters/my-cluster1
./apply-workload-cluster.sh ./environment-values/workload-clusters/my-cluster2
```
:::
