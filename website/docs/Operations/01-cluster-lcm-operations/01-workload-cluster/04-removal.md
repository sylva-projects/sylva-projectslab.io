---
title: Workload Cluster Removal
sidebar_label: Removal
---

:::tip
#### Using the management cluster kubeconfig

```
cd ~/sylva-core
export KUBECONFIG=management-cluster-kubeconfig
```

#### Find the workload cluster namespace

```
kubectl get ns -A
```
:::

### Delete the `sylva-units` Helm Release in the workload namespace  

```sh
kubectl delete HelmRelease/sylva-units -n workload-cluster-ns
```

It should prompt `helmrelease.helm.toolkit.fluxcd.io "sylva-units" deleted`.

### Watch delete job being completed

```
ubuntu@am-sylva:~/sylva-core$ kubectl get all -n workload-cluster-ns
NAME                                          READY   STATUS      RESTARTS   AGE
pod/cluster-pre-delete-hook-fxv6s             0/1     Completed   0          4m12s
pod/sylva-units-delete-kustomizations-z5zm5   1/1     Running     0          4m39s

NAME                                          COMPLETIONS   DURATION   AGE
job.batch/cluster-pre-delete-hook             1/1           70s        4m13s
job.batch/sylva-units-delete-kustomizations   0/1           4m40s      4m40s
```

### Remove the workload cluster namespace

:::info
No need to wait for `job/sylva-units-delete-kustomizations` to complete
:::

```
kubectl delete ns workload-cluster-ns
```

It should prompt `namespace "workload-cluster-ns" deleted`. 
