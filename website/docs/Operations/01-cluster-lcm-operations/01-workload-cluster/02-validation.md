---
title: Workload Cluster Validation
sidebar_label: Validation
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Overview

This procedure explains how to check workload cluster(s) are correctly installed.

### Get workload cluster kubeconfig file

There are two ways to get a workload cluster `kubeconfig` file:

1. from Rancher web UI (recommended if Rancher Unit is installed);
2. from `kubectl` CLI (useful for troubleshooting purpose from the bootstrap VM).

#### Method 1: From Rancher web UI

1. From a web browser, login to the Rancher dashboard (cf. [Admin Onboarding section](/docs/Operations/user-and-role-management/authentication#rancher)).
2. Find the workload cluster you want the kubeconfig from.
3. Download kubeconfig (as shown in the below image)

![Download workload cluster kubeconfig](./img/download-wc-kubeconfig.png)

:::note
With this approach:

- access to the workload cluster is done via the management cluster;
- the `kubeconfig` file refers to the management cluster _domain name_ rather than its IP address.
:::

Reference (Rancher documentation): [Accessing Clusters with kubectl from Your Workstation](https://ranchermanager.docs.rancher.com/how-to-guides/new-user-guides/manage-clusters/access-clusters/use-kubectl-and-kubeconfig#accessing-clusters-with-kubectl-from-your-workstation)

#### Method 2: From the CLI

For convenience or troubleshooting purpose, a different `kubeconfig` file can be retrieved from the bootstrap VM.

Assuming your workload cluster is named `my-wc-bootprov-infraprov` and sits in the
`my-wc-bootprov-infraprov` namespace, you can get its `kubeconfig` file with:

```shell
cd ~/sylva-core
MY_WC=my-wc-bootprov-infraprov
MY_WC_NS=my-wc-bootprov-infraprov
```

```shell
kubectl get secret -n ${MY_WC_NS} ${MY_WC}-kubeconfig \
    --kubeconfig management-cluster-kubeconfig \
    -o template="{{ .data.value }}" | base64 -d > ${MY_WC}-kubeconfig
```

:::note
With this approach:
- access to the workload cluster is direct: it does not pass through the management cluster;
- the `kubeconfig` file refers to the _workload cluster_ virtual IP address
:::

Same as the management cluster, a config file will be available in the `/sylva-core` directory.

```shell
# With MY_WC is still defined, you can use below export
export KUBECONFIG=${MY_WC}-kubeconfig

# Then you cass directly access this workload API
kubectl get pods -A
```
