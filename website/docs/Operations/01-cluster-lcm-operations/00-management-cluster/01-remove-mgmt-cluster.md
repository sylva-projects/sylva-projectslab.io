---
title: Management cluster deletion for OpenStack deployment
sidebar_label: Management Cluster Removal
---

This procedure allows to delete the OpenStack resources associated to an Openstack installation using a certain `.cluster.capo.resources_tag`. If the same tag is shared by multiple Sylva on VM instances, all would be cleaned.

From `sylva-core` folder:

:::warning
Do **NOT** run this script as an OpenStack admin.
:::

```shell
$ ./tools/openstack-cleanup.sh my-cloud <resources_tag> 
# resources_tag is by default set to sylva-<$OS_USERNAME>
```
