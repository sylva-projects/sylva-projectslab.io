---
title: Applying changes on the management cluster
---

Once the bootstrap and the pivot phases are done, the management cluster can be updated with:

```shell
./apply.sh <your-environment-values-directory>
```