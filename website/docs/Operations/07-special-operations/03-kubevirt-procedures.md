---
title: KubeVirt
sidebar_position: 3
---

:::info
For an in-depth understanding of KubeVirt and its integration within Sylva, please refer to the [KubeVirt Overview](/dev-zone/developer-perspectives/relevant-units/kubevirt-overview).
:::

# KubeVirt Procedures

This document provides concise commands to address various use cases for managing KubeVirt VMs.

## Enable kubevirt

Into the cluster's `values.yaml`, add:

```yaml
units:
  kubevirt:
    enabled: true
```

Then (example for the workload cluster):

```shell
./apply-workload-cluster.sh environment-values/workload-clusters/first-workload-cluster
```

## How to Create a VM

Example: `cirros-vm.yaml`

```yaml
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: sylva-tests-cirros-vm
spec:
  running: true
  template:
    metadata:
      labels:
        kubevirt.io/size: small
        kubevirt.io/domain: testvm
    spec:
      domain:
        devices:
          disks:
            - name: containerdisk
              disk:
                bus: virtio
            - name: cloudinitdisk
              disk:
                bus: virtio
          interfaces:
            - name: default
              masquerade: {}
        resources:
          requests:
            memory: 64M
      networks:
        - name: default
          pod: {}
      volumes:
        - name: containerdisk
          containerDisk:
            image: quay.io/kubevirt/cirros-container-disk-demo
        - name: cloudinitdisk
          cloudInitNoCloud:
            userDataBase64: SGkuXG4=
```

### VM Creation

With the workload cluster kubeconfig:
```shell
kubectl apply -f cirros-vm.yaml
```

### Check VM Status

```shell
kubectl get vms -n <namespace>
```

### Start/Stop the VM

```shell
kubectl patch virtualmachine sylva-tests-cirros-vm --type merge -p '{"spec":{"running":true}}'
kubectl patch virtualmachine sylva-tests-cirros-vm --type merge -p '{"spec":{"running":false}}'
```

### Interact Using `virtctl` CLI

Enable or install the `virtctl` binary:

```shell
virtctl console/start/stop <vm-name>
```

### Patch VMs

Start the VM:

```shell
kubectl patch virtualmachine <vm-name> --type merge -p '{"spec":{"running":true}}'
```

Stop the VM:

```shell
kubectl patch virtualmachine <vm-name> --type merge -p '{"spec":{"running":false}}'
```

## Accessing VMs

Connect to the serial console of the VM:

```shell
virtctl console <vm-name>
```

### Access VM Using SSH (Key and Password)

Create a secret from the public key:

```shell
kubectl create secret generic my-pub-key --from-file=key1=<path-of-public-key> --namespace <namespace>
```

Create a VM using the template:

```yaml
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: sylva-testvm-ssh
  namespace: kubevirt-tests
spec:
  running: true
  template:
    spec:
      domain:
        devices:
          disks:
            - disk:
                bus: virtio
              name: containerdisk
            - disk:
                bus: virtio
              name: cloudinitdisk
          rng: {}
        resources:
          requests:
            memory: 1024M
      terminationGracePeriodSeconds: 0
      accessCredentials:
        - sshPublicKey:
            source:
              secret:
                secretName: my-pub-key
            propagationMethod:
              noCloud: {}
      volumes:
        - containerDisk:
            image: quay.io/containerdisks/fedora:38
          name: containerdisk
        - cloudInitNoCloud:
            userData: |-
              #cloud-config
              password: fedora
              chpasswd: { expire: False }
              ssh_pwauth: True
          name: cloudinitdisk
```

### Describe VMI to Check VM IP

```shell
kubectl describe vmi <vm-name> -n <namespace>
```

### Login to VM Using Key

From the cluster node hosting the VM:

```shell
ssh -i <private-key> fedora@<IP>
```

### Login to VM Using Password

From the cluster node hosting the VM:

```shell
ssh fedora@<IP>
```

Enter the password.

## Delete VM

```shell
kubectl delete vm <vm-name>
```

## How to Test

Using nested virtualization, implement and test on CAPO for the management cluster and baremetal/CAPO for the workload cluster. CI runs validate its functionality. Users can run their own customized templates for VMs where the KubeVirt unit is enabled or create templates under the `kubevirt-test-vms` unit.

## Live migration of VMs

Live migration is a process during which a running Virtual Machine Instance moves to another compute node while the guest workload continues to run and remain accessible.
Live migration feature is enabled by default in kubevirt.

For live migration of VM, HA cluster is required.

### How to initiate live migration

There are various ways of doing the live migration.

### Using virtctl to initiate live migration

```shell
virtctl migrate <vmi name>
```

### Using declarative way

Define a VM YAML file: `vm-migration.yaml`

```shell
apiVersion: kubevirt.io/v1
kind: VirtualMachineInstanceMigration
metadata:
  name: migration-job
  namespace: <namespace of vm>
spec:
  vmiName: <vmi name>
```

```shell
kubectl apply -f vm-migration.yaml
```

### How it looks like

Below is the state of vmi and related pods before starting live migration.

```shell
root@sylva-large-vm:~/sylva-core# kubectl get vmi -A
NAMESPACE        NAME                    AGE    PHASE     IP             NODENAME                                 READY
kubevirt-tests   sylva-tests-centos-vm   112m   Running   100.72.38.90   management-cluster-cp-1e47fe552f-5657f   True
kubevirt-tests   sylva-tests-cirros-vm   112m   Running   100.72.38.91   management-cluster-cp-1e47fe552f-5657f   True

root@sylva-large-vm:~/sylva-core# kubectl get pods -A | grep -i kubevirt-tests
kubevirt-tests                     virt-launcher-sylva-tests-centos-vm-tk4wh                         3/3     Running     0              113m
kubevirt-tests                     virt-launcher-sylva-tests-cirros-vm-z84bs                         3/3     Running     0              113m
```

After applying any of above procedure for live migration

- vmi will remain intact.
- New pod related to that vmi will be created and old pod will start migrating.
- Old pod will turn into Completed state and new pod will be in running state.

```shell
root@sylva-large-vm:~/sylva-core# kubectl get vmi -A
NAMESPACE        NAME                    AGE    PHASE     IP               NODENAME                                 READY
kubevirt-tests   sylva-tests-centos-vm   118m   Running   100.72.125.177   management-cluster-cp-1e47fe552f-nxccv   True
kubevirt-tests   sylva-tests-cirros-vm   118m   Running   100.72.38.91     management-cluster-cp-1e47fe552f-5657f   True

root@sylva-large-vm:~/sylva-core# kubectl get pods -A | grep -i kubevirt-tests
kubevirt-tests                     virt-launcher-sylva-tests-centos-vm-tk4wh                         0/3     Completed   0              117m
kubevirt-tests                     virt-launcher-sylva-tests-centos-vm-tw6sb                         3/3     Running     0              57s
kubevirt-tests                     virt-launcher-sylva-tests-cirros-vm-z84bs                         3/3     Running     0
```

### Cancel a live migration

Live migration can be canceled by simply deleting the migration object

```shell
kubectl delete pod <pod under migration> -n <namespace>
```

Live migration can also be canceled using virtctl, by specifying the name of a VMI which is currently being migrated

```shell
virtctl migrate-cancel <vmi name> --namespace <namespace>
```

## VM migration from VMware to KubeVirt

Migrating a Virtual Machine (VM) from VMware to KubeVirt involves several steps.
The process typically includes exporting the VM from VMware, converting it to a compatible format, and then importing it into KubeVirt. Here's a general outline of the steps involved:

### Export VM from VMware

Export the disk in VMDK or other formats

- Right-click on the VM in the VMware vSphere or vCenter interface.
- Select _Export OVF Template_ or _Export_ and choose the appropriate format (OVA, OVF, or VMDK).
- Download above exported template from relevant data store on VMware.

### Convert VM disk to a compatible format

KubeVirt uses raw or qcow2 disk formats for virtual machines. If you exported the VM as VMDK, you will need to convert it.

`qemu-img` tool can be used for disk conversion which can be downloaded using `apt install qemu-utils` on Ubuntu platform

```shell
qemu-img convert -f vmdk -O qcow2 <exported vmdk image>.vmdk <name for qcow2 image>.qcow2
```

### Upload disk image to server

You will need to upload the disk image to any public http based platforms like cloud object storage(s3/gcp/azure), any ftp server, openstack swift or any file hosting services which should be accessible from your Kubernetes cluster where the VM will be installed.

### Create an image via KubeVirt dashboard

Click on _Images_ and provide the required details like name, http based url on KubeVirt dashboard, to create an image from the converted template

![kubevirt-create-image](./img/01-kubevirt-create-image.png)

### Launch VM

Click on _Virtual Machines_ and provide relevant information like name, namespace

![kubevirt-create-vm](./img/02-kubevirt-create-vm.png)

Click on _Disk_ tab and select _image_ to use the above-created image from the template

![kubevirt-select-image](./img/03-kubevirt-select-image.png)

### VM Access and verification

Access the migrated VM from VMware to KubeVirt

[How to access vm](#access-vm-using-ssh-key-and-password)

## Dashboard for VM management

VMs can be managed through the KubeVirt dashboard via `kubevirt-manager` unit, which is enabled by default alongside the `kubevirt` unit.

[How to access UI of Kubevirt](/docs/Operations/user-facing-services/access-uis)

## How to create VM with DataVolume

CDI uses a DataVolume to handle importing and managing PersistentVolumes(PVs) for VM disks. A DataVolume describes the source of the image and how it should be imported.

Create a data volume to import the disk image. For this example, we'll import a disk image from an HTTP based URL (you can also import from S3, local uploads, etc).

fedora-data-volume.yaml

```yaml
apiVersion: cdi.kubevirt.io/v1beta1
kind: DataVolume
metadata:
  name: fedora-datavolume
  namespace: kubevirt-tests
spec:
  source:
    http:
      url: "https://download.fedoraproject.org/pub/fedora/linux/releases/38/Cloud/x86_64/images/Fedora-Cloud-Base-38-1.6.x86_64.qcow2"  # Fedora cloud image URL
  pvc:
    accessModes:
      - ReadWriteOnce
    resources:
      requests:
        storage: 10Gi
```

Apply this DataVolume manifest to your cluster:

```shell
kubectl apply -f fedora-data-volume.yaml
```

CDI will now start importing the disk image and store it in the specified PersistentVolumeClaim (PVC). You can check the status using:

```shell
kubectl get datavolume -n kubevirt-tests

NAME                PHASE       PROGRESS   RESTARTS   AGE
fedora-datavolume   Succeeded   100.0%                103m
```

Once the DataVolume is ready, the disk image will be available as a PVC.

```shell
kubectl get pvc -n kubevirt-tests

NAME                STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS      VOLUMEATTRIBUTESCLASS   AGE
fedora-datavolume   Bound    pvc-7a976f14-9b28-44bb-adf5-b140069b6570   10Gi       RWO            cinder-ceph-ssd   <unset>                 104m
```

Create a KubeVirt Virtual Machine (VM) Using the above created DataVolume.

fedora-dv-vm.yaml

```yaml
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: fedora-vm
  namespace: kubevirt-tests
spec:
  running: true
  template:
    metadata:
      labels:
        kubevirt.io/domain: fedora-vm
    spec:
      domain:
        devices:
          disks:
            - name: rootdisk
              disk:
                bus: virtio
        resources:
          requests:
            memory: 2048Mi
      volumes:
        - name: rootdisk
          dataVolume:
            name: fedora-datavolume
```

Apply the VM manifest

```shell
kubectl apply -f fedora-dv-vm.yaml
```

Monitor the VM

```shell
kubectl get vm -n kubevirt-tests
NAME                    AGE    STATUS    READY
fedora-vm               110m   Running   True
```
