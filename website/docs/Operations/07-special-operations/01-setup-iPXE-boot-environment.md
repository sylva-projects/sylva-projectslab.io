---
title: Setup iPXE boot environment to provision BM nodes
sidebar_label: Setup iPXE boot environment
---

:::warning
Booting BM nodes with PXE is only for experimental purposes and is not supported for production platforms. Only Virtual Media based installations are supported in production.
:::

## Overview

Today in Sylva, by default BM provisioning is only possible through Virtual Media using Redfish/IPMI. While Virtual Media brings several advantages over PXE-based installations in many cases, legacy hardware such as Dell 13G does **not** support Virtual Media. In those cases, it is possible to leverage on iPXE setup to provision the nodes. This document describes extra steps to be performed to set up an iPXE-based environment & it does not change the main installation guidelines/procedure.

## Prerequisites

- A Linux VM/Server for hosting DHCP & TFTP services
- Setup DHCP BOOTP relay, if DHCP service is hosted on a separate network than provisioning
- Management cluster is already installed
- Ironic external service IP address should be reachable from provisioning network
- Enable PXE for network interface which is supposed to be used as provisioning interface on all servers
- Provisioning interface is having access to provisioning network natively i.e. Native VLAN is set on switch for provisioning network

## Procedure

### Setting up DHCP & TFTP server

Apart from the usual IP pool definition, set the below options globally in `dhcpd.conf`:

```powershell
option client-architecture code 93 = unsigned integer 16;
if exists user-class and option user-class = "iPXE" {
	filename "http://<ip-of-ironic-service>:6180/boot.ipxe";
} elsif option client-architecture = 00:00 {
	filename "undionly.kpxe";
} else {
	filename "ipxe.efi";
}
```

Make sure to change the IP address of the Ironic service. Once the management cluster is installed, you can run the below command to check the Ironic service external IP address:

```powershell
kubectl get svc -n metal3-system ironic
```

For example, the external IP address here is `192.168.78.24`:

```powershell
root@management-cluster-cp-e2e49d8d39-zwhkr:~# kubectl get svc -n metal3-system ironic
NAME     TYPE           CLUSTER-IP     EXTERNAL-IP                   PORT(S)                                        AGE
ironic   LoadBalancer   10.43.21.157   192.168.78.24,192.168.78.24   6180:30021/TCP,5050:31230/TCP,6385:32216/TCP   47d
```

You also need to keep the boot files `undionly.kpxe` & `ipxe.efi` in the root directory of the TFTP directory. These files can be downloaded from the links given below:

- [undionly.kpxe](http://boot.ipxe.org/undionly.kpxe)
- [ipxe.efi](http://boot.ipxe.org/ipxe.efi)

Refer to [this link](https://ipxe.org/howto/chainloading) for more information on Setting up PXE chain loading.

:::note
Installation of the DHCP/TFTP server is not in the scope of this document. The installation procedure may vary depending on the operating system.
:::

### Update values.yml

As stated above, by default all templates use Virtual Media-based URLs to connect with BMC of servers. It is mandatory to update `values.yml` accordingly. For example, you will use the below for a Dell 13G server:

```powershell
bmc:
  address: idrac-redfish://<bmc-ip-address>/redfish/v1/Systems/System.Embedded.1
  disableCertificateVerification: true
```

Refer to [this link](https://github.com/metal3-io/baremetal-operator/blob/main/docs/api.md) for more information on API and Resource Definitions

Now, nodes should be ready to boot from PXE. You can proceed with your workload cluster installation.
