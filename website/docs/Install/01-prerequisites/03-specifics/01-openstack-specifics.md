---
title: "Openstack Specifics"
reviewed: true
---

## Setting Up the Base OpenStack Environment

:::note
The minimum required version of OpenStack is Queens (Train or higher recommended).
:::

After connecting to the bootstrap VM, proceed with the following steps:

- Install the OpenStack client:

```shell
sudo apt install python3-openstackclient python3-heatclient jq # Optional, for use behind a proxy
mkdir -p ~/.config/openstack
```

- Create a `clouds.yaml` file, adjusting the content to match your OpenStack credentials/settings:

```shell
cat <<EOF > ~/.config/openstack/clouds.yaml
clouds:
  my-cloud:
    auth:
      auth_url: "https://openstack_auth_url/v3"  # CHANGE ME
      password: PASSWORD                         # CHANGE ME
      project_domain_name: default               # CHANGE ME
      project_name: "PROJECT_NAME"               # CHANGE ME
      user_domain_name: default                  # CHANGE ME
      username: USERNAME                         # CHANGE ME
    endpoint_type: publicURL
    identity_api_version: 3
    interface: public
    region_name: RegionOne                       # CHANGE ME
    verify: false
EOF
```

**Tips**: 
To retrieve the values for this `clouds.yaml` file, log into Horizon with the tenant used to create the bootstrap VM, navigate to `Project/Project/API access/Download OpenStack RC file clouds.yaml`.

- Verify your credentials by testing them with:

```shell
openstack --os-cloud my-cloud project list
```

:::note
In Sylva versions prior to 1.1.0, you had to manually download images and upload them to Glance. This is now automatically handled by Sylva based on the image specified in the cluster definition.
:::

### Creating a Key Pair for the Tenant

```shell
openstack --os-cloud my-cloud keypair create my_key > my_key.pem
chmod 0600 my_key.pem
```

The public key generated will be injected into all VMs created by Sylva. The `my_key.pem` file stores the private key, which you can use to connect to Sylva VMs using the default user (`node-admin`), with a command like:

```shell
ssh -i ./my_key.pem node-admin@my_vm_ip
```

:::caution
Ensure you back up and securely store the `.pem` file.
:::
