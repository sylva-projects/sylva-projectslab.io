---
title: Bootstrap VM
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

The bootstrap VM should be based on `Ubuntu 22.04` with at least:
- 4 vCPUs
- 8GB RAM
- 40GB Disk (50GB or more recommended)

<Tabs groupId="infraproviders">

<TabItem value="openstack" label="Openstack"> 

#### Hosting Environment Requirements

It is simplest to share the same tenant and networks for both the bootstrap VM and management cluster VMs. More details are in the [next section](management-requirements).

#### Connectivity Requirements

Refer to the network requirements in the [next section](management-requirements).
</TabItem>

<TabItem value="bm" label="Bare-Metal">

#### Hosting Environment Requirements

A VM hosting environment with necessary connectivity to management cluster nodes is required.

#### Connectivity Requirements

Detailed flow matrix information is available [here](/docs/Operations/network-operations/flow-matrix).

The bootstrap VM needs IP connectivity to:
  - BMC interfaces of management cluster servers.
  - The `management` network of the servers (the underlying network which will handle the management cluster CNI).
  - Sylva artifacts to pull code and images.
</TabItem>
</Tabs>
