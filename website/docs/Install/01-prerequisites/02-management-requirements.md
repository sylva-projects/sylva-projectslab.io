---
title: Management Cluster
sidebar_position: 1
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs groupId="infraproviders">

<TabItem value="openstack" label="Openstack"> 
Deploying on OpenStack requires access to an IaaS tenant with sufficient resources for the management cluster deployment.

#### Management Tenant Minimal Quotas

These quotas are for hosting the bootstrap VM and deploying the management clusters with 3 replicas. If the same tenant is used for deploying workload clusters on VMs, quotas should be increased accordingly.

- **Instances**: 10 (default) - Adjust based on the number of workload clusters.
- **Volumes**: 50 (default: 10) - Increase based on PVC and instance requirements.
- **Gigabytes**: 2000 (default: 1000) - Adjust based on potential workload PVs.
- **Cores**: 64 (default: 20) - 64 vCPUs or more, depending on workload VMs.
- **RAM**: 128000 MB (default: 51200 MB) - 128GB or more, depending on workload VMs.
- **Networks**: 10 (default) - Adjust based on Multus requirements for additional networks.
- **Ports**: 50 (default) - Adjust based on Multus requirements for additional networks.
- **Security Groups**: 15 (+5 per workload cluster)
- **Security Group Rules**: 100 (+20 per workload cluster)

:::note
The total instances number is 4 (bootstrap + 3 management) + number of VMs requested for workload cluster
:::

#### Network Requirement

The network on which your nodes will be attached should have connectivity to OpenStack APIs and external resources. 

It can be a VXLAN-based tenant network or a VLAN-based provider network. Ensure DHCP, DNS forwarding, and metadata service access are enabled. The subnet size should be at least /28 (would allow to address up to 13 nodes and a gateway), though /27 or wider is recommended.

:::note
`network_id` in the environment-values
:::

For tenant networks, if you require access via a Floating IP, ensure the network is connected to a router attached to the desired external network, and have at least 2 floating IPs available before starting deployment.

:::note
`external_network_id` in the environment-values
:::

The VMs in this tenant must access:
- **OpenStack API**: For CAPI to create VMs.
- **Sylva Artifacts (or local registry mirror)**: For pulling Sylva OCI images.
</TabItem>

<TabItem value="bm" label="Bare-Metal">
For a lab or pre-production deployment, at least one node is required (non Highly Available environment).

Three nodes are required for production or any HA environment supporting live rolling upgrades.

#### Node Minimum Resources

 - 16 CPU cores (32 vCPUs with Hyper-Threading)
 - 32GB RAM
 - 240GB flash boot disk (SSD)
 - Additional dedicated storage device (SSD recommended, 960GB minimum) for Longhorn

#### Network Prerequisites

- **BMC Interfaces**: Must be reachable from the management cluster. Only essential flows should be authorized from/to the Out-of-Band network (allows sensitive operations). See [flow matrix section](/docs/Operations/maintenance-operations/platform-shutdown).

#### Provisioning Steps

The bare-metal nodes provisioning is currently using Virtual-Media from BMC, but it can also use a legacy step inherited from PXE/iPXE boot mechanism which is the default method in Metal3/Ironic, only if DHCP is configured accordingly. PXE/iPXE brings complexity and is subject to simplification in future releases and Virtual-Media is the only recommended way of provisioning.

There are four main steps in bare-metal provisioning through Metal3/Ironic:

  1. At first, Ironic registers bare-metal host via its BMC address.
  2. Then it goes through an `introspecting` state which needs the host to boot using a downloaded discovery image (AKA ramdisk image). During this state, Ironic waits for call back from host until it is discovered.
  3. Then, after computing the introspection results, the machine is identified in the hardware inventory and the machine can download the deployment image. Some additional configuration tasks are done by Ironic. Node state will be `provisioning` at this stage. Then, it finally reboots the machine.
  4. The final node state is `provisioned`.

Details:

- Using the BMC addresses included in `values.yaml` and the BMC credentials included in `secrets.yaml`, Ironic tells the host through the BMC interface to download the discovery image, mounts it as a virtual media and boots on it.

- The default discovery OS (IPA) network configuration is injected by baremetal operator relying on BMH `ip_preallocations` values.

- Ironic will identify the machine in its inventory, will allocate the definitive IP address and will tell the machine to download the deployment image and its final configuration.

- Ironic will reboot the machine on its deployment image and perform few additional configuration tasks, then the machine goes in an "provisioned" state. 

Use the following command to check provisioning status:

```shell
kubectl get bmh -A
```
More details are available [here](https://github.com/metal3-io/metal3-docs/blob/main/design/baremetal-operator/how-ironic-works.md).

#### Network Allocation Example

| Network         | VLAN id | CIDR             | Gateway       | Metal3 pools                         |
| --------------- | ------- | ---------------- | ------------- | ------------------------------------ |
| Provisioning    | 2016    | 10.199.39.192/27 | 10.199.39.193 | Provisioning pool: 10.199.39.219-220 |
| Primary Network | 2015    | 10.188.36.128/26 | 10.188.36.129 | Primary pool: 10.188.36.148-149      |

</TabItem>
</Tabs>
