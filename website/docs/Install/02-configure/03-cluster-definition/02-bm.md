---
title: "Bare-Metal Cluster Specifics"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## DHCP less bare-metal node bootstrap

Previous Sylva versions required a DHCP server available on a native network (without VLAN tags) connected to the interfaces of bare-metal servers. This was required for Ironic Python Agent to get their interfaces with a first configuration allowing to reach the Ironic IPA server. This way of bootstrapping bare-metal nodes is still possible however it is now possible to avoid this design complexity by setting (statically) in the `values.yaml` a BM node network pre-configuration. 

The drawback of this DHCP-less installation is to require more informations to be set in the values prior to get first introspection results (which could help defining the rest of bare-metal values in the 2-step way suggested [below](#Helpful 2-step values Generation)). The BM nodes need firstly to have their primary interfaces preconfigured to be able to send introspection results to Ironic IPA server. This preconfiguration (BMH `ip_preallocations` , `mac_address` mapped to the interface values `network_interfaces`) is injected by baremetal operator to the discovery OS (IPA) network configuration.

The `mac_address` can be retrieved from the BMC interfaces of the BM node. 

Example for a control_plane node:

```yaml
control_plane:
  capm3:
    primary_pool_network: 10.188.36.128           # CHANGE ME, needs to be a pre-existing primary network (see pre-requisites)
    primary_pool_prefix: "26"                     # CHANGE ME, primary pool IP address collection
    primary_pool_gateway: 10.188.36.129           # CHANGE ME
    primary_pool_start: 10.188.36.148             # CHANGE ME, starting address in the primary network range (This range setting is still needed even if static IP preallocation per BM host is used for DHCP-less provisioning, the preallocation IP should be part of this range, see below)
    primary_pool_end: 10.188.36.150               # CHANGE ME, ending address in the primary network range

 #   The provisioning network is required only if Ironic is not reachable from primary network (e.g. with DHCP-based provisioning)
 #   provisioning_pool_name: provisioning-pool     # required field if provisioning network is needed
 #   provisioning_pool_network: 10.199.39.192      # CHANGE ME, needs to be a pre-existing provisioning network (see pre-requisites)
 #   provisioning_pool_prefix: "27"                # CHANGE ME, provisioning pool IP address collection
 #   provisioning_pool_gateway: 10.199.39.193      # CHANGE ME
 #   provisioning_pool_start: 10.199.39.195        # CHANGE ME, starting address in the provisioning address range
 #   provisioning_pool_end: 10.199.39.197          # CHANGE ME, ending address in the provisioning address range
    hostSelector:
        matchLabels:
          cluster-role: control-plane
 #   provisioning_pool_interface: bond0  # Optional provisioning interface for DHCP mode
    primary_pool_interface: bond0.100
  network_interfaces: # 
    bond0:
      type: bond
      interfaces:
        - ens1f0 # Should be the inspection name (if known) or a logical mapping from the interface MAC address (see below)
        - ens1f1
      vlans:
        - id: 100  # VLAN ID is fixed, per libvirt-metal internals
    ens1f0:
        type: phy
    ens1f1:
        type: phy
```

```yaml
baremetal_hosts:  # corresponding credentials need to be set in secrets.yaml
  my-hpe-server:
    interface_mappings:
      ens1f0:
        mac_address: ba:ad:00:c0:ef:ee # CHANGE ME, this is the MAC address of baremetal server
      ens1f1:
        mac_address: ba:ad:00:c0:ef:ef 
    ip_preallocations:
      primary: 10.188.36.148 # CHANGE ME, this is the static IP allocation of the baremetal server (not used if DHCP based)
    longhorn_disk_config:
      - path: "/var/longhorn/disks/disk_by-path_pci-0000:00:0b.0" # CHANGE ME, name of the block device to be used by longhorn
        storageReserved: 0
        allowScheduling: true
        tags:
          - ssd
          - fast
```

:::note

The mapping defined under the `baremetal_hosts` definition is also useful when using heterogeneous hardware under a same node group. Indeed it was previously required that all servers of a given group (`control_plane` or `machine_deployments`) have the same interface names, which is not always possible. The addition of interface mappings at the baremetalHost level addresses this limitation.

When logical mapping is used, the interface definition must be a `bond` (even with a single interface).

:::

:::warning

When LLDP is activated on the switch where the server is connected to, it can interfere with the network configuration set in the introspection OS (via network-manager) for DHCP-Less provisioning. To overcome this side-effect, the workaround consists in disabling part of the LLDP from the metal3 unit values:

```yaml
units:
  metal3:
    helmrelease_spec:
      _postRenderers:
        - kustomize:
            patches:
              - patch: |
                  - op: replace
                    path: /data/IRONIC_INSPECTOR_VLAN_INTERFACES
                    value: "''"
                target:
                  kind: ConfigMap
                  name: ironic-bmo

```

:::



## Helpful 2-step values Generation

The `values.yaml` for a baremetal cluster contains information specific to the hardware, in particular:
* under `cluster`, the names of the network interfaces
* under `baremetal_hosts`, information on the root disk (`rootDeviceHints`), and on the disks to use for Longhorn

It isn't always easy to know this information beforehand.

To make it easier, we propose the following "two-step" approach, which let's you trigger Metal3 **hardware inspection first**,
look at the results of the inspection to complete the values, and **then pursue the installation**:

* step 1: leave default values for network interfaces and disks in `values.yaml`

* step 2: set the `cluster` unit as _suspended_ in `values.yaml`

    ```yaml
    units:
      cluster:
        suspend: true
    ```

    The effect of this setting is that the creation of the Kubernetes resources that depend upon settings under `.cluster` will be suspended, with the exception of `BaremetalHosts` resources which will be created and only go through their inspection phase.

* step 3: run `boostrap.sh` (for management cluster) or `apply-workload-cluster.sh` (for a workload cluster), as you would do if you had fully prepared your values (see the corresponding documentation)

* step 4: the script should at some point stop progressing, showing you that the only pending activity is:

    ```
    ...
    ⏸ HelmRelease/cluster - ...
    ```

    And showing you that the `cluster-bmh` unit is Ready (this unit defines the `BaremetalHosts` resources):

    ```
    ...
    ✓ Kustomization/sylva-system/cluster-bmh - Resource is ready
    ```

    At this point you need to **interrupt the script** (CTRL-C).

    (This is fully safe, at this stage the script is not acting on the bootstrap stack, is merely observing its evolution)

* step 5: wait for the hardware inspection to be done (`BaremetalHost` state moving from `inspecting` to `available`)

    ```
    $ kubectl get baremetalhosts  # for a workload cluster, add -n your-workload-cluster; 
    NAME                     STATE       CONSUMER   ONLINE   ERROR   AGE
    mgmt-22926481-dl360-34   available              false            125m
                             ^^^^^^^^^
    ```

    You can also look at the `status.operationHistory.inspect` of a `BaremetalHost` to see the start and end times for the inspection.

* step 6: at this point you can inspect the `BaremetalHosts` object, and in their status find the hardware information you need

    Example (for a workload cluster, add `-n your-workload-cluster`):

    ```terminal
    $ kubectl get baremetalhosts.metal3.io management-cluster-management-cp-0 -o yaml | yq .status.hardware
    cpu:
      arch: x86_64
      clockMegahertz: 4000
      count: 96
      flags:
        ...
      model: Intel(R) Xeon(R) Gold 6240R CPU @ 2.40GHz
    firmware:
      bios:
        date: 07/16/2020
        vendor: HPE
        version: U32
    hostname: localhost.localdomain
    nics:
      - mac: 48:df:37:e7:82:39
        model: 0x8086 0x158b
        name: ens2f0
        speedGbps: 25
      - mac: 48:df:37:e7:77:34
        model: 0x8086 0x158b
        name: ens2f1
        speedGbps: 25
    [...]
      - ip: fe80::c970:a1:2d5b:c6ac%ens1f0
        mac: 48:df:37:e7:7a:30
        model: 0x8086 0x158b
        name: ens1f0
        speedGbps: 25
    ramMebibytes: 393216
    storage:
      - alternateNames:
          - /dev/sda
          - /dev/disk/by-path/pci-0000:5c:00.0-sas-0x31402ec0168d74f4-lun-0
        hctl: "1:0:2:0"
        model: MK001920GWSSE
        name: /dev/disk/by-path/pci-0000:5c:00.0-sas-0x31402ec0168d74f4-lun-0
        serialNumber: S523NA0N907721
        sizeBytes: 1920383410176
        type: SSD
        vendor: ATA
        wwn: "0x5002538e0092314a"
        wwnWithExtension: "0x5002538e0092314a"
      [...]
      - alternateNames:
          - /dev/sdg
          - /dev/disk/by-path/pci-0000:5c:00.0-scsi-0:1:0:0
        hctl: "1:1:0:0"
        model: LOGICAL VOLUME
        name: /dev/disk/by-path/pci-0000:5c:00.0-scsi-0:1:0:0
        serialNumber: PEYHB0FRHEF32X
        sizeBytes: 480070426624
        type: SSD
        vendor: HPE
        wwn: "0x600508b1001ca5ec"
        wwnVendorExtension: "0xc626171c4c8b6282"
        wwnWithExtension: 0x600508b1001ca5ecc626171c4c8b6282
    systemVendor:
      manufacturer: HPE
      productName: ProLiant DL360 Gen10 (P19766-B21)
      serialNumber: CZ205300VJ
    ```
    
* step 7: finish preparing `values.yaml`  (see section below) and `secrets.yaml` 

* step 8: _remove_ `units.cluster.suspend: true` from `values.yaml`

    ```yaml
    units:
      cluster:
        # suspend: true  ## remove this
    ```

* step 9: proceed with the installation, running `boostrap.sh` (for management cluster) or `apply-workload-cluster.sh` (for a workload cluster), as you would do if you had fully prepared your values (see the corresponding documentation)



<Tabs groupId="test" defaultValue={null}>
<TabItem value="management" label='Management cluster specifics'>

</TabItem>

<TabItem value="workload" label='Workload cluster specifics'>

</TabItem>
</Tabs>