---
title: "Openstack Cluster Specifics"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


<Tabs groupId="test" defaultValue={null}>
<TabItem value="management" label='Management cluster specifics'>
### Cluster virtual ip

By default, management cluster uses a virtual-ip shared by all control plane nodes.

In case of Openstack deployment, it is required to specify a network_id in capo cluster section.

```yaml
cluster:
  capo:
    network_id: xxxxx   # CHANGE ME with Openstack virtual network uuid
```

### Cluster public ip

Cluster public ip is optional in Sylva stack, it allows you to expose your cluster on an external network.

In case of Openstack deployment, we specify an external_network_id in capo cluster section.

```yaml
openstack:
  external_network_id: "openstack external network uuid" # CHANGE ME 
```

With such configuration, Sylva stack will get a floating-ip dynamically on the specified network.

If you want to reuse, an existing floating-ip, you can configure the `cluster_floating_ip_uuid` instead of `external_network_id` like following:


```yaml
openstack:
  # external_network_id: xxxxxx
  cluster_floating_ip_uuid: "openstack floating ip on external network uuid" # CHANGE ME
```

:::warning
You can't configure `external_network_id` and `cluster_floating_ip_uuid` simultaneously, those values are mutually exclusive.
:::

### Explained example

Here is an example of a cluster `values.yaml` file for OpenStack.

```yaml
cluster:
  capo:
    ssh_key_name: "your-key"              # CHANGE ME
    network_id: "your-net-id"             # CHANGE ME
    flavor_name: "m1.xlarge"              # CHECK ME
  # control_plane_replicas: 3             # CHANGE ME if needed, use 1 only for lab clusters
  machine_deployments:
    md0:
      replicas: 0                         # CHECK ME, optional for management cluster
      capo:
        flavor_name: "m1.large"           # CHECK ME

openstack:
  external_network_id: "your-ext-net-id"  # CHANGE ME, optional, only if a floating IP is wanted
  storageClass:
    type: "ceph_ssd"                      # CHECK ME (openstack volume type list)
```

- Flavor minimal requirements for management cluster nodes:
  - 8 vCPU
  - 16GB RAM (24GB is recommended for a single node management cluster)
  - Disk 40GB (root on volume)

- Different flavors can be used for workload cluster nodes and management cluster nodes (if the workload cluster is to be deployed on OpenStack VMs). Within a cluster, different flavors can be used for control and worker nodes.

- Management cluster control node flavor example: m1.xlarge (8vCPU, 16GB RAM, 160 GB disk)

- Storage Class: SSD storage is required especially for K8s controller. `openstack --os-cloud my-cloud volume type list` displays the available storage class on the platform.

- `ssh_key_name`: the name of the OpenStack key name created [here](/docs/Install/prerequisites/specifics/openstack-specifics/#creating-a-key-pair-for-the-tenant)

- By default, the cluster node anti-affinity is set to “hard anti-affinity” (`control_plane_affinity_policy: anti-affinity`) meaning that if you request for 3 replicas, the IaaS must have 3 distinct hosts to allow deployment (this is requested for production). For lab it is possible to override this default setting:

  ```yaml
  openstack:
    control_plane_affinity_policy: soft-anti-affinity
  ```

:::caution
If OpenStack instances do not use Cinder root volume (e.g. using computes with local storage), ensure the size of the image being used is within the flavor's disk size or use a larger flavor to avoid machine creation failures during the deployment
:::

:::note
If the management cluster is hosted on OpenStack infrastructure, on the same tenant than the bootstrap VM, you should use the same secrets as in the `clouds.yaml` file (refer to bootstrap VM documentation).
:::

:::tip
The `project_domain_name` is not present in the openrc or `clouds.yaml` file that can be downloaded from Horizon. You can get it from OpenStack CLI with the following commands:

- run `openstack project show <PROJECT_NAME>` to get the`domain_id`
- run `openstack domain show <DOMAIN_ID>` to get the domain name
:::

:::caution
If you opt to set the `admin_password` field above, you absolutely need to choose a secure one, compliant with the password policy enforced by Keycloak: length(12), upperCase(1), lowerCase(1), and digits(1). Otherwise, the SSO login would not be available for the UI.
If you do not set one here, we will pick a random one and you can retrieve it in Vault (path `secret/sso-account`) or by using the following command: `kubectl get secrets sylva-units-values -o template="{{ .data.values }}" | base64 -d | grep admin_password`.
:::


</TabItem>

<TabItem value="workload" label='Workload cluster specifics'>

</TabItem>
</Tabs>