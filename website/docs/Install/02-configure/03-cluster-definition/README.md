---
title: "Step3: Cluster definition"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import Values from './_partials/values.mdx';
import Secrets from './_partials/secrets.mdx';

:::important
For each cluster, the environment values (`values.yaml` and `secrets.yaml`) need to reflect the desired parameters for the cluster.

Preparing these values is **the** major task required to create a new cluster.
It is crucial to understand that values below `.cluster` are defined in the [sylva-capi-cluster chart](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster). The documentation does not cover all possibilities, so visiting the provided link will help operators define the specific needs of the cluster.

**This document provides additional information to help you tune the environment values**.
:::

<Tabs groupId="install-tabs" defaultValue={null}>

<TabItem value="values" label='values.yaml'>
<Values></Values>
</TabItem>

<TabItem value="secrets" label='secrets.yaml'>
<Secrets></Secrets>
</TabItem>

</Tabs>

## Specific parameters

import DocCardList from '@theme/DocCardList';

<DocCardList />