---
title: "vSphere Cluster Specifics"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

<Tabs groupId="test" defaultValue={null}>
<TabItem value="management" label='Management cluster specifics'>

:::note
Specific capv values are listed in the [sylva capi-cluster chart](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster).
Here we will focus on some important values for capv management cluster.
:::

### vCenter credentials

- Provide your **vCenter credentials** in `environment-values/my-capv-env/secrets.yaml`

```yaml
cluster:
  capv:
    username: # replace me
    password: # replace me
```

### vSphere cluster configuration

- Adapt `environment-values/my-capv-env/values.yaml` to suit your environment:

```yaml
  ...
  cluster:
    [...]
    capi_providers:
      infra_provider: capv   # vSphere infra provider
      bootstrap_provider: cabpk/cabpr  # Kubeadm/RKE2 bootstrap provider

    capv:
      image_name: # Template from which to clone the node VMs
      dataCenter: # Datacenter name
      networks:
        default:
          networkName: # VSphere default network for VMs
      server: # VSphere DNS server name
      dataStore: # VSphere datastore name
      tlsThumbprint: # VSphere https TLS thumbprint
      ssh_key: # SSH public key for VM access
      folder: # VSphere folder
      resourcePool: # VSphere resoucepool
      storagePolicyName: # VSphere storage policy name
      
      # Control Planes sizing defaults
      diskGiB: 50
      memoryMiB: 8192
      numCPUs: 4

    machine_deployments:
      md0:
        [...]
        capv:
          # Worker nodes sizing defaults
          diskGiB: 50
          numCPUs: 4
          memoryMiB: 8192

cluster_virtual_ip: # replace me
```
:::note
  - The `image_name` field contains the name or the inventory path of the VM template used to deploy the VMs for the cluster. The VM template is created from an OVA image. You can find a set of pre-built images in the [repository of CAPV provider](https://github.com/kubernetes-sigs/cluster-api-provider-vsphere#Kubernetes-versions-with-published-OVAs).
  - The `bootstrap_provider` field can currently be set to `cabpk` for kubeadm or to `cabpr` for RKE2
  - The `tlsThumbprint` field contains the SHA1 thumbprint of the vCenter certificate. It can be retrieved from the certificate with this command:
  
```shell
openssl x509 -sha1 -fingerprint -in ca.crt -noout
```
:::

### vSphere cluster removal

#### Remove workload clusters

Follow this [procedure](/docs/Operations/cluster-lcm-operations/workload-cluster/removal).

#### Remove management cluster

Delete the cluster VMs via vCenter or govc CLI.

:::note
On cluster deletion, you need to consider that PVs won't be deleted automatically. [Here is a link to a relevant documentation](https://support.kublr.com/support/solutions/articles/33000282670-vsphere-remove-orphaned-container-volumes)
:::

</TabItem>
</Tabs>
