---
title: Docker Mirror
---

### Moving past Docker Hub rate limiting

You can [run a local registry mirror](https://docs.docker.com/registry/recipes/mirror/#run-a-registry-as-a-pull-through-cache) and have the management cluster be configured with a `docker.io` registry mirror to point to it by configuring `registry_mirrors` in environment-values with its endpoint.

```yaml
registry_mirrors:
  hosts_config:
    docker.io:
    - mirror_url: http://your.mirror/docker
```
