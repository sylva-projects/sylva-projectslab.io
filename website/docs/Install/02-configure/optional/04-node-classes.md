---
title: Utilizing the Node Class in Sylva
---

## Introduction

The Sylva project aims to establish a unified set of worker classes to streamline configurations and improve project implementation. This document outlines the proposal for standardizing worker classes within Sylva, focusing on their main features, configurations, and how they cater to different application requirements.

## Standard Node Classes

Sylva introduces three primary node classes, alongside customizable options, to accommodate various computing and networking needs. Each class is tailored to specific performance and configuration requirements:

- **Generic Node Class**: Ideal for IT and non-intensive network workloads, offering a baseline performance with optimized footprint and without specialized configurations such as huge-pages or CPU pinning.
  
- **Intensive Node Class**: Designed for workloads that demand intensive computing power and predictable performance but less flexibility, featuring huge-pages and static CPU manager settings for CPU pinning.
  
- **SR-IOV Node Class**: Targets high network performance and near real-time computing, supporting huge pages, SR-IOV, CPU pinning, NUMA topology awareness, and system CPU isolation.
  
- **Custom Node Class**: Provides flexibility for custom kernel, boot, and kubelet settings to meet specific CNF requirements.

Starting from Sylva V1.0.0, the generic node class is predefined. Users can define additional node classes by modifying the `cluster.node_classes` property.

### Deployment Considerations

When deploying Sylva, the defined node classes become available for both management and workload clusters through the `shared_workload_clusters_values` configmaps.

It is possible to select a node class for :

- the entire cluster : `cluster.default_node_class`
- the control plane: `cluster.control_plane.node_class`
- machine deployments (default value): `cluster.machine_deployment_default.node_class`
- a specific machine deployment: `cluster.machine_deployments.X.node_class`

Node classes are only available on RKE2 clusters. It has not yet been implemented for Kubeadm.

## Kernel Configuration

### Huge Pages

**Configurable Numbers per Size**: Supports 2M and 1G huge pages, with a reserved amount of non-huge memory for the system and kube.

**Example**: 16GB of memory is kept as memory not allocatable in Huge Pages, 64 * 1G Huge Pages are configured.

```yaml
node_classes:
  <node_class_name>:
    non_hugepages_minimum_memory_gb: 16
      kernel_cmdline:
        hugepages:
          enabled: true
          hugepagesz_2M: 0
          hugepagesz_1G: 64
          default_size: 1G
```

### Additional Kernel Options

**Purpose**: Activate specific kernel options like Intel_iommu, sriov_numvfs, and system CPU isolation.

**Example**: Enabling SR-IOV options

```yaml
node_classes:
  <node_class_name>:
    kernel_cmdline:
      extra_options: "Intel_iommu=on Intel_iommu=pt sriov_numvfs=32"
```

## Kubelet configurations

Customize kubelet settings to optimize node performance and resource management. For a comprehensive list of configurable parameters, visit the [Kubernetes documentation](https://kubernetes.io/docs/tasks/administer-cluster/kubelet-config-file/).

**Example**: Customizing Eviction Hard Limits

```yaml
node_classes:
  <node_class_name>:
    kubelet_config_file_options:
      evictionHard:
        memory.available: "500Mi"
        nodefs.available: "10%"
        nodefs.inodesFree: "5%"
        imagefs.available: "15%"
```

## Node Taint/Labels/Annotations

Define taints, labels, and annotations to manage node behavior and workload allocation effectively.

```yaml
node_classes:
  <node_class_name>:
    nodeTaints: {}
    nodeLabels:
      sylva.org/node-label: true
    nodeAnnotations:
      sylva.org/node-annotation: true
```

## Additional Commands

Specify pre and post-bootstrap commands for further customization and optimization of node setup.

```yaml
node_classes:
  <node_class_name>:
    additional_commands:
      pre_bootstrap_commands: []
      post_bootstrap_commands: []
```

## Examples

Below are YAML configuration examples for the intensive and SR-IOV node classes, illustrating how to apply the discussed settings and options.

- **Intensive Node Class Example**

```yaml
node_classes:
  intensive:
    non_hugepages_minimum_memory_gb: 16
    kernel_cmdline:
      hugepages:
        enabled: true
          hugepagesz_2M: 1000
          hugepagesz_1G: 64
          default_size: 1G
      extra_options: ""
    kubelet_extra_args: {}
    kubelet_config_file_options:
      featureGates:
        CPUManager: true
      cpuManagerPolicy: static
      kubeReserved:
        cpu: "500m"
        memory: "1Gi"
      systemReserved:
        cpu: "500m"
        memory: "1Gi"
      evictionHard:
        memory.available: "500Mi"
        nodefs.available: "10%"
        nodefs.inodesFree: "5%"
        imagefs.available: "15%"
    nodeTaints: {}
    nodeLabels:
      sylva.org/annotate-node-label-intensive: true
    nodeAnnotations: {}
    additional_commands:
      pre_bootstrap_commands: []
      post_bootstrap_commands: []
```

- **SR-IOV Node Class Example**

  The following example provides CPU isolation which may cope with a Dell server 2x32 cores (hyper-threading activated)  but need to be adapted to other CPU topology.

```yaml
node_classes:
  sriov:
    non_hugepages_minimum_memory_gb: 16
    kernel_cmdline:
      hugepages:
        enabled: true
        hugepagesz_2M: 0
        hugepagesz_1G: 64
        default_size: 1G
      extra_options: "processor.max_cstate=1 intel_idle.max_cstate=1 intel_pstate=disable cpufreq.off=1 intel_iommu=on iommu=pt nosoftlockup nohz_full=4-63,68-127 systemd.cpu_affinity=0-3,64-67 irqaffinity=0-3,64-67" #change me
    kubelet_extra_args: {}
    kubelet_config_file_options:
      cpuManagerPolicy: static
      cpuManagerPolicyOptions:
        full-pcpus-only=true
      topologyManagerPolicy: single-numa-node
      topologyManagerScope: pod
      kubeReserved:
        memory: "7Gi"
      systemReserved:
        memory: "7Gi"
      reservedSystemCPUs: "0-3,64-67" #change me
      evictionHard:
        memory.available: "2000Mi"
        nodefs.available: "10%"
        nodefs.inodesFree: "5%"
        imagefs.available: "15%"
    nodeTaints: {}
    nodeLabels:
      sylva.org/annotate-node-label-sriov: true
    nodeAnnotations: {}
    additional_commands:
      pre_bootstrap_commands: []
      post_bootstrap_commands: []
```
