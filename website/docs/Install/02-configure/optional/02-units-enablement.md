---
title: Enable/Disable units
---

In order to have a lighter deployment, you can disable some units. You can find the list of units [here](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/charts/sylva-units/units-description.md?ref_type=heads).

Here you can see an example from `values.yaml`.

```yaml
units:
  thanos:
    enabled: false
  harbor:
    enabled: false
  gitea:
    enabled: false
  [...]
```