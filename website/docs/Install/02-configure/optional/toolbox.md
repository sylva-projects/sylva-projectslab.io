---
title: "Toolbox"
---

:::note
The Sylva Toolbox is shipped directly when **bootstraping** sylva-core.

Many tools like `kubectl`, `sylvactl`, `yq`, `flux`, `helm`, `clusterctl`, and `kind` are available in the `sylva-core/bin` folder.
:::

To use them comfortably, from the `sylva-core` directory, run:

```shell
source bin/env
```

This will put these tools in your $PATH and also enable shell completion for those that support it.

### Download the toolbox by itself

<div class ="autowrap"> 

```shell
mkdir ./sylva-toolbox
docker run --rm registry.gitlab.com/sylva-projects/sylva-elements/container-images/sylva-toolbox | tar xzv -C ./sylva-toolbox
```

</div>

You can then source the tools :

```sh
source sylva-toolbox/env
```
