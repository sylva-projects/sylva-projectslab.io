---
title: Proxy Configuration
---

If you need to set proxies for your VM, you can use the below template.

```shell
#add proxy to /etc/environment
echo "http_proxy=" | sudo tee -a /etc/environment
echo "https_proxy=" | sudo tee -a /etc/environment
echo "no_proxy=" | sudo tee -a /etc/environment

#export proxy for the current shell
export http_proxy=
export https_proxy=
export no_proxy=
```
