---
title: "Step1: Clone"
---

## Clone `sylva-core`

```shell
git clone https://gitlab.com/sylva-projects/sylva-core.git
cd sylva-core
```

:::warning
From now on, every command needs to be run from the `sylva-core/` folder.
:::