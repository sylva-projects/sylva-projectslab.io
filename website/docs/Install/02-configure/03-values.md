---
title: "Step2: Generate Values"
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Generate your custom environment values

Depending on your environment, sample values files are available in the directory `environment-values/`,  choose the appropriate sample and copy it into your own directory:

<Tabs groupId="infraproviders">

<TabItem value="openstack" label="Openstack">

```
cp -r environment-values/rke2-capo/ environment-values/my-rke2-capo 
```

</TabItem>

<TabItem value="bm" label="Bare-Metal">

### Bare-Metal

```
cp -r environment-values/rke2-capm3/ environment-values/my-rke2-capm3 
```

### Virtualized Bare-Metal

```
cp -r environment-values/rke2-capm3-virt/ environment-values/my-rke2-capm3-virt 
```

</TabItem>

<TabItem value="docker" label="Docker">

```
cp -r environment-values/rke2-capd/ environment-values/my-rke2-capd 
```

</TabItem>

<TabItem value="vmware" label="VMWare">

```
cp -r environment-values/rke2-capv/ environment-values/my-rke2-capv 
```

</TabItem>
</Tabs>

Then adjust the generated `values.yaml` and `secrets.yaml` files to match your environment,
following the [cluster definition documentation](cluster-definition).

**Please follow the [cluster definition documentation](cluster-definition) to finish the preparation of these files**.





