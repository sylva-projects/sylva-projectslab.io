---
title: Installation Validation
sidebar_position: 0
---

## Validate Sylva Deployment

### Setup the shell environment

To validate a Sylva deployment, you will need to use some tools such as `kubectl` and `sylvactl` from the bootstrap VM. Those tools are installed under the `sylva-core/bin` folder. If you want to use them easily, you can run:

```shell
source bin/env
```
from the `sylva-core` folder. It will add them to your path and install the autocompletion for most of them.

:::tip
To make the configuration persistent, refer to the [sylva-toolbox](/docs/Install/troubleshooting/#sylva-toolbox) section of the troubleshooting guide.
:::

### Retrieve API and dashboard endpoints

* The `bootstrap.sh` script provides an output with:
   - the list of deployments with latest states
   - the Ingresses exposed in the management cluster accessible either through:
  - (VM-CAPO) an IP address randomly set by the stack from the `capo.network_id` subnet (used for the VMs/nodes ports), 
     - (VM-CAPO) via an OpenStack Floating IP (if the `openstack.external_network_id` was provided)
     - (BM-CAPM3) via an IP from the `public_pool` in the `capm3` values
  
* The same output can be achieved with the below command:

 ```shell
 sylvactl watch --kubeconfig management-cluster-kubeconfig
 ......................
 ✓ Kustomization/default/rancher - Resource is ready: Rancher UI can be reached at https://rancher.sylva (rancher.sylva must resolve to 172.27.0.5)
 ✓ Kustomization/default/rancher-webhook-service - Resource is ready
 ✓ Kustomization/default/vault - Resource is ready: Vault UI can be reached at https://vault.sylva (vault.sylva must resolve to 172.27.0.5)
 ......................
 ✓ All Done!
  # here the 172.27.0.5 would be the access IP (a FIP if the `capo.external_network_id` was provided in environment-values)

 ```

* To get the URLs and the Ingress IP at any point, after the deployment, run:

```shell
kubectl get ingress -A --kubeconfig management-cluster-kubeconfig
NAMESPACE       NAME                      CLASS    HOSTS            ADDRESS          PORTS     AGE
cattle-system   rancher                   nginx    rancher.sylva    172.27.0.5   80, 443   3d2h
flux-system     flux-webui-weave-gitops   nginx    flux.sylva       172.27.0.5   80, 443   3d2h
keycloak        keycloak-ingress          <none>   keycloak.sylva   172.27.0.5   80, 443   3d2h
vault           vault                     <none>   vault.sylva      172.27.0.5   80, 443   3d2h
```
In case points have an Ingress IP address as a fixed IP address, retrieve the floating IP associated with this fixed IP address.

```shell
openstack --os-cloud my-cloud floating ip list --long
```

### Prepare your client environment to access the dashboard

Setup the connectivity to the Ingress IP from any Bastion or bootstrap VM (direct connection or SOCKS tunnel in browser).

Populate your DNS with the domain you have configured (e.g. `sylva.your_domain`) for accessing the UI:

```shell
172.27.0.5 rancher.sylva.your_domain flux.sylva.your_domain keycloak.sylva.your_domain vault.sylva.your_domain   # DNS mapping for Ingress URLs to the relevant IP address
```

For lab, you can resolve locally the `sylva` (default) or `sylva.your_domain` (custom) domain using your `hosts` file to resolve the URLs to the Ingress IP

> For Linux:

```shell
cat /etc/hosts | grep rancher.sylva
172.27.0.5 rancher.sylva flux.sylva keycloak.sylva vault.sylva   # DNS mapping for Ingress URLs to the relevant IP address
```

> For Windows:
`C:\Windows\System32\drivers\etc`


![image-2.png](./img/image-2.png)

In your browser, set no proxy for `.sylva`. Then, for example, for Rancher, the URL is https://rancher.sylva. 
To test url access : `curl -kv https://rancher.sylva` (or the other URLs provided in the output)

### Get the credentials

 The SSO login username is `sylva-admin` (technical account using Keycloak backend) and the local user is `admin` (both should be tested).

**For SSO login**

You can retrieve it in Vault (secret/sso-account - learn more about this at in [Sylva Password Management](https://sylva-projects.gitlab.io/operations/security/password-management)) or by using the following command:

```shell
kubectl --kubeconfig management-cluster-kubeconfig \
  get secret sylva-units-values \
  -o template="{{ .data.values }}" \
  | base64 -d | grep admin_password
```

**For local user**

```shell
kubectl --kubeconfig management-cluster-kubeconfig \
  get secret --namespace cattle-system bootstrap-secret \
  -o go-template='{{.data.bootstrapPassword|base64decode}}{{"\n"}}'
```

### Access to additional dashboards

#### Vault UI

Vault web UI is accessible at the URL [https://vault.sylva](https://vault.sylva/) with the following method and credentials:

- Method: OIDC
- Role: default
- User name: sylva-admin (SSO login)

Once logged in Vault, it is possible to retrieve the Keycloak `admin` password (this account is required to add new groups like a `user_admin` and `secu_admin` groups which will be used to add nominal users accounts). Refer to the [user-management guide](/docs/Operations/user-and-role-management/user-management) for more details.
It is also possible to get it from the Keycloak namespace

```yaml
kubectl --kubeconfig management-cluster-kubeconfig get secrets -n keycloak keycloak-initial-admin -o jsonpath='{.data.password}' | base64 -d
```

#### Keycloak UI

Keycloak administration console is accessible at the URL https://keycloak.sylva/admin/master/console, with the credentials:

- User name: `admin`
- Password: retrieved in Vault

Keycloak web UI for users as `sylva-admin` (SSO login) is accessible at the URL https://keycloak.sylva/admin/sylva/console

#### Monitor Clusters with Rancher UI Built-in Dashboards

Once logged in Rancher UI you can see all the clusters Rancher is managing. Rancher UI offers capabilities for cluster orchestration and monitoring. After selecting a cluster, you'll be able to access any of the built-in dashboards for this cluster:

1. In the upper left corner, click on **Cluster Management**.
2. On the Clusters page, go to the cluster `local` which is your management cluster, to see the specific dashboard UI. Next, click on **Explore**.
3. In the left navigation bar, click on `Monitoring` to list the available dashboards.
 * Grafana ([reference](https://ranchermanager.docs.rancher.com/integrations-in-rancher/monitoring-and-alerting/rbac-for-monitoring#role-based-access-control-for-grafana))
 * Prometheus ([reference](https://ranchermanager.docs.rancher.com/integrations-in-rancher/monitoring-and-alerting/built-in-dashboards#prometheus-ui))
 * AlertManager ([reference](https://ranchermanager.docs.rancher.com/integrations-in-rancher/monitoring-and-alerting/built-in-dashboards#alertmanager-ui))

#### Test access to Flux-WebUI

- Apply the same networking prerequisites as above to access to Flux-webUI URL (https://flux.sylva)
- Flux-WebUI ([reference](https://docs.gitops.weave.works/docs/getting-started/ui/)) credentials:
  * If you are already logged in using Keycloack SSO, you can select this method to enter Flux-Web UI

### Acceptance tests

Launch Sonobuoy to check the Kubernetes conformance of the deployed cluster

Launch an Ubuntu VM. This VM needs to access to the cluster network. On this VM, install Sonobuoy by following the next steps.


1. Configure the proxy if required

```shell
sudo sh -c "cat <<EOF >> /etc/environment
export https_proxy=your_proxy #CHANGE ME
export http_proxy=your_proxy #CHANGE ME
export no_proxy=127.0.0.0/8,10.0.0.0/8,.sylva #CHANGE ME
EOF"
exit
```

2. Install [Sonobuoy](https://github.com/vmware-tanzu/sonobuoy/):

```shell
wget https://github.com/vmware-tanzu/sonobuoy/releases/download/v0.56.16/sonobuoy_0.56.16_linux_386.tar.gz
tar -xvf sonobuoy_0.56.16_linux_386.tar.gz
sudo mv sonobuoy /usr/local/bin/.
```

3. Copy the kubeconfig of your cluster on the VM
4. Launch the Sonobuoy quick test on the cluster to validate your setup.
    The quick mode checks the connectivity.

```shell
export KUBECONFIG=my-management-cluster-kubeconfig #CHANGE ME
sonobuoy run --wait --mode quick
```

Once every thing is fine, proceed with the cleanup:

```shell
sonobuoy delete
```

5. On the management cluster only create the following ClusterPolicy and PolicyException before launching sonobuoy.

```shell
apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  name: sonobuoy-disable-policy
  annotations:
    kustomize.toolkit.fluxcd.io/force: Enabled
spec:
  rules:
  - name: sonobuoy-sa-automount
    match:
      any:
      - resources:
          kinds:
          - ServiceAccount
          names:
          - default
          namespaces:
          - prestop-*
          - svcaccounts-*
          - aggregator-*
          - crd-webhook-*
    mutate:
      patchStrategicMerge:
        automountServiceAccountToken: true
---
apiVersion: kyverno.io/v2alpha1
kind: PolicyException
metadata:
  name: sonobuoy-exception
spec:
  exceptions:
  - policyName: disallow-latest-tag
    ruleNames:
    - autogen-require-image-tag
  match:
    any:
    - resources:
        kinds:
        - Pod
        - ReplicationController
        - Deployment
        - ReplicaSet
        - CronJob
        namespaces:
        - resourcequota-*
```

6. Launch Sonobuoy conformance:

```shell
export KUBECONFIG=my-management-cluster-kubeconfig #CHANGE ME
sonobuoy run --wait
```

Note that the execution will take 1h45min.

7. Retrieve the results and list the tests performed:

```shell
results=$(sonobuoy retrieve)
sonobuoy results $results --mode=report
sonobuoy results $results --mode=dump

```
In case the test fails, for example:
[It] [sig-api-machinery] StatefulSet Basic StatefulSet functionality [StatefulSetBasic] Scaling should happen in predictable order and halt if any stateful pod is unhealthy. [Conformance]
This test can be run again:
```shell
sonobuoy run --e2e-focus="StatefulSet Basic StatefulSet functionality \[StatefulSetBasic\] Scaling should happen in predictable order and halt if any stateful pod is unhealthy" --wait
```

8. On the management cluster delete the ClusterPolicy and PolicyException created before.
 
9. Proceed with the cleanup:

```shell
sonobuoy delete
```
