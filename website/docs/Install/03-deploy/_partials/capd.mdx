## Deploying clusters in Docker using CAPD

A short introduction for CAPI on Docker can be found [here](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/docs/capd.md)

Even if it is not representative of any real-life deployment use-case, running clusters in Docker is useful to enable the testing of lifecycle management of clusters without any infrastructure requirement.

It can be used to test that stack on a laptop or in [GitLab-ci](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/.gitlab-ci.yml). You just have to
[install Docker](https://docs.docker.com/engine/install/) as a prerequisite, and then clone this
[project](https://gitlab.com/sylva-projects/sylva-core). The recommended Docker version is 23.0.6 which is proven
to work by [GitLab-ci](.gitlab-ci.yml). There is a known issue described in [issue #273](https://gitlab.com/sylva-projects/sylva-core/-/issues/273) when
using Docker 24.0.0 and [issue #368](https://gitlab.com/sylva-projects/sylva-core/-/issues/368) when using Docker between 19.03.1 to 19.03.4.

> **_NOTICE:_** When deploying on SUSE OS, please check whether DNS server is installed and enabled, see [SUSE Domain Name System](https://documentation.suse.com/sles/15-SP5/html/SLES-all/cha-dns.html).

The `bootstrap.sh` script which you'll use below, will create for you a kind cluster that will be used as the bootstrap cluster.

Once the `bootstrap.sh` script is finished with success and the pivot from bootstrap to management cluster is done with success, the kind bootstrap cluster will be removed. (except for libvirt-metal deployments, because the management nodes are VMs living in pods of the bootstrap cluster).
To keep the bootstrap cluster you can do an `export CLEANUP_BOOTSTRAP_CLUSTER=no` before running the `bootstrap.sh`.

If available in the Linux environment variables, the following vars will be used to customize this kind cluster:

- [`KIND_POD_SUBNET`](https://kind.sigs.k8s.io/docs/user/configuration/#pod-subnet)
- [`KIND_SVC_SUBNET`](https://kind.sigs.k8s.io/docs/user/configuration/#service-subnet)
- [`KIND_CLUSTER_NAME`](https://kind.sigs.k8s.io/docs/user/configuration/#name-your-cluster)

As we'll be creating a fair amount of containers, it is recommended to increase the filesystem watcher limit in order to avoid reaching the limit.

```shell
echo "fs.inotify.max_user_watches = 524288" | sudo tee -a /etc/sysctl.conf
echo "fs.inotify.max_user_instances = 512" | sudo tee -a /etc/sysctl.conf
sudo sysctl -p /etc/sysctl.conf
```

You should also add your user to `docker` group, it'll allow you to run the rest of the stack without privileges:

```shell
sudo usermod -aG docker $USER
```

Then you may adapt your environment values in `environment-values/kubeadm-capd/values.yaml` (using kubeadm) or
`environment-values/rke2-capd/values.yaml` (using cluster-api-rke2 bootstrap provider).

For capd deployment, you have to:

- provide the proxy address if you are using one:

```yaml
proxies:
  http_proxy: http://your.company.proxy.url
  https_proxy: http://your.company.proxy.url
  no_proxy: 127.0.0.1,localhost,192.168.0.0/16,172.16.0.0/12,10.0.0.0/8
```

- modify existing parameter `cluster_virtual_ip: xx.xx.xx.xx` in file [`environment-values/kubeadm-capd/values.yaml`](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/environment-values/kubeadm-capd/values.yaml) or [`environment-values/rke2-capd/values.yaml`](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/environment-values/rke2-capd/values.yaml) accordingly

Then you can bootstrap the management cluster creation with kubeadm:

```shell
./bootstrap.sh environment-values/kubeadm-capd
```

If you want to deploy a cluster using cluster-api-rke2 bootstrap provider, you just have to use the [`environment-values/rke2-capd`](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/environment-values/rke2-capd) directory instead:

```shell
./bootstrap.sh environment-values/rke2-capd
```

For more details on environment-values generation you can have a look at the [dedicated README](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/environment-values/README.md).

> **_NOTE:_** If you intend to contribute back to this project (we would be happy to welcome you!), you should probably create your own copy of environment-values prior to editing them. This way you won't be editing files that are tracked by Git, and will not commit them by inadvertence:
>
> ```shell
> cp -a environment-values/rke2-capd environment-values/my-rke2-capd
> ```