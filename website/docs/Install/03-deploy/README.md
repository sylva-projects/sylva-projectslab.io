---
title: Trigger the deployment
sidebar_position: 2
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


<Tabs groupId="infraproviders">

<TabItem value="docker" label="Docker">
```sh
./bootstrap.sh environment-values/my-rke2-capd
```
</TabItem>

<TabItem value="vmware" label="VMWare">
```sh
./bootstrap.sh environment-values/my-rke2-capv
```
</TabItem>

<TabItem value="openstack" label="Openstack">
```sh
./bootstrap.sh environment-values/my-rke2-capo
```
</TabItem>

<TabItem value="metal3" label="Metal3">
:::note
For management cluster deployed on baremetal, we need to increase the reconcile timeout value as the provisioning takes time.
:::

```sh
BOOTSTRAP_WATCH_TIMEOUT_MIN=120 ./bootstrap.sh environment-values/my-rke2-capm3
```
</TabItem>

</Tabs>



If all goes well, the end of `bootstrap.sh` output will look like this:

    ```
    ✔ Sylva is ready, everything deployed in management cluster
       Management cluster nodes:
    NAME                                    STATUS   ROLES                       AGE     VERSION
    vdr3-mgmt-cluster-cp-b4809e4d94-dpf2n   Ready    control-plane,etcd,master   10m     v1.26.9+rke2r1
    vdr3-mgmt-cluster-cp-b4809e4d94-gwk5m   Ready    control-plane,etcd,master   7m17s   v1.26.9+rke2r1
    vdr3-mgmt-cluster-cp-b4809e4d94-mzvsq   Ready    control-plane,etcd,master   16m     v1.26.9+rke2r1
    
    🌱 You can access following UIs
    NAMESPACE       NAME                      CLASS    HOSTS                 ADDRESS          PORTS     AGE
    cattle-system   rancher                   nginx    rancher.sylva.vdr3    172.20.130.185   80, 443   4m46s
    flux-system     flux-webui-weave-gitops   nginx    flux.sylva.vdr3       172.20.130.185   80, 443   92s
    harbor          harbor-ingress            <none>   harbor.sylva.vdr3     172.20.130.185   80, 443   84s
    keycloak        keycloak-ingress          <none>   keycloak.sylva.vdr3   172.20.130.185   80, 443   4m26s
    vault           vault                     <none>   vault.sylva.vdr3      172.20.130.185   80, 443   6m41s
    
    🎉 All done
    
    🗑 Delete bootstrap cluster
    Deleting cluster "sylva" ...
    Deleted nodes: ["sylva-control-plane"]
    ```

You can now proceed to the [management cluster validation](../validate) step.

If you do not see the `🎉 All done` line and get an error, you need to understand why the installation failed and fix it. The [troubleshooting](../troubleshooting) page can help you with that task.


