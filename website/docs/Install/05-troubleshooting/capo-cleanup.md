---
title: Openstack Cleanup
---

One limitation of this approach is that management cluster can not delete itself properly, as it will shoot itself in the foot at some point.

For resources created by CAPO, the [`tools/openstack-cleanup.sh`](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/tools/openstack-cleanup.sh) script is provided to help cleaning the resources.
