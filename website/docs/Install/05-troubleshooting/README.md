---
title: Troubleshooting
sidebar_position: 1
---

This document gathers information in order to help an operator understand why a management cluster installation failed and how to fix it.


## Where to troubleshoot from

All the common troubleshooting tasks can be done **from the bootstrap VM**:

* read the `bootstrap.sh` log and get the deployment status with `sylvactl watch`
* inspect OpenStack resources with the `openstack` command line client (for deployment on OpenStack VMs)
* inspect kubernetes resources in the bootstrap cluster
* login the management cluster machines and inspect the systems
* after the pivot, inspect kubernetes resources in the management cluster

For deployment on OpenStack VMs, you can do additional tasks **from a machine with a web browser** and proper network access:

* login OpenStack horizon dashboard to inspect the resources created and have a look at the tenant resource consumption
  :::caution
  By default, will not be able to login the management cluster VMs using horizon console: the default user does not have a password. In some cases, it can be useful to setup a password, but you will need to login with SSH to do this.
  :::


## Tools available in the bootstrap VM

### Sylva toolbox

The `sylva-core/bin` folder contains many useful tools for troubleshooting, including:

  * `kubectl`: to inspect and manage Kubernetes resources
  * `helm`: to inspect and manage Helm releases
  * `flux`: to inspect and manage Flux resources (GitOps system)
  * `sylvactl`: to watch the status of Flux resources created by Sylva
  * `clusterctl`: to inspect and manage the clusters created by Cluster API
  * `kind`: to delete the bootstrap cluster when the management cluster installation failed and needs to be restarted from scratch

To use them easily, run:
```shell
source bin/env
```
from the `sylva-core` directory. It will add `sylva-core/bin` to your path and add autocompletion in your shell for most of the tools.

You can add permanently this configuration in your environment by running the following command *from the `sylva-core` directory*:

```bash
grep -q "source \"$(realpath bin/env)\"" ~/.bashrc || echo "source \"$(realpath bin/env)\"" >> ~/.bashrc
```

### OpenStack client

The `openstack` client can be used to inspect the OpenStack resources created during the installation, particularly the servers (`openstack server list`) and the Heat stacks (`openstack stack list`).

:::tip
To avoid passing `--os-cloud=my-cloud` parameter to all the OpenStack commands, you can export the `OS_CLOUD` environment variable in your shell:
```
export OS_CLOUD=my-cloud
```
::::

### Ubuntu software

All the software available in Ubuntu can be installed with `sudo apt install <package>`


## Inspect the management cluster machines

To troubleshoot issues that happen during the installation of the kubernetes resources in the management cluster before the pivot, i.e. before the `management-cluster-kubeconfig` file is available in the bootstrap VM, it is useful to login the management cluster control plane machines and run `kubectl` and other commands there.

### Log in the management cluster machines

For deployment on top of OpenStack, list the machines with:

```shell
openstack server list
```

The management cluster control plane machines are named `<my-mgmt-cluster>-cp-<random-string>`, where:

* `<my-mgmt-cluster>` is the `cluster.name` value you set in the `values.yaml` file (by default: `cluster`)
* `<random-string>` looks like a random meaningless identifier (e.g. `7432ecd1d6-gxkct`)

Identify your machine then read its IP address in the `Networks` column.

Finally login the desired machine with:
```shell
ssh -i ./my_key.pem node-admin@<my-vm-ip>
```
where `my_key.pem` is the private key you created during [Step1: Bootstrap VM Creation](/docs/Install/prerequisites/specifics/openstack-specifics#creating-a-key-pair-for-the-tenant)

### Inspect the rke2 service

Check status of the `rke2-server` system service:

```
systemctl status rke2-server
```

### Read the logs

Show the logs of the `rke2-server` system service:
```
journalctl -u rke2-server
```

Show the logs of the `containerd` container runtime:

```
less /var/lib/rancher/rke2/agent/containerd/containerd.log
```

Show the kubelet logs:

```
less /var/lib/rancher/rke2/agent/logs/kubelet.log
```

More information, including how to read the logs from the Kubernetes Pods or from the containers can be found in the [Logging section](https://docs.rke2.io/reference/logging) of RKE2 reference documentation.

### Run kubectl in the management cluster machines

On a management cluster control plane machine:

* the kubeconfig file is `/etc/rancher/rke2/rke2.yaml` and it can only be read by `root`
* the `kubectl` binary is at `/var/lib/rancher/rke2/bin/kubectl`

To be able to run `kubectl` as `node-admin` user, do the following:

```
cd
mkdir .kube
sudo cat /etc/rancher/rke2/rke2.yaml > .kube/config
sudo ln -s /var/lib/rancher/rke2/bin/kubectl /usr/local/bin/kubectl
```

Then you can list the pods and look at their status with:

```
kubectl get pods -A
```

Pods that do not show a `Running` or `Completed` status are good candidates for further inspection.


## Common issues

:::note Editor's note

Section status: section to be completed
:::

- invalid config files
  - invalid YAML
  - values do not comply with the schema
- invalid proxy configuration
- invalid OpenStack credentials
- under dimensioned OpenStack quotas
- lack of resources on the OpenStack infra (no compute node available, no floating ip available, …)
- bugs in the product:
  - timing issues

### When the pivot fails
In case you encounter issues during deployment (sometimes due to timeout, resource exhaustion, or any other reason) and want to run the deployment again, then follow the below steps to proceed.

- Run the below command again.

```shell
./bootstrap.sh environment-values/current-environment-values
```

- If you see the below error trace while running `bootstrap.sh` then it means that the pivot is in progress and you should wait for some time to let the pivot job finish. If the issue still persists, try to debug further as stated below.

```terminal
$ ./bootstrap.sh environment-values/current-environment-values
🔎 Validate input files

❌ The pivot job is in progress. Please wait for it to finish.
```

- If the pivot job is still in progress for long, check the state and logs of the pivot job pod with the below commands.

```terminal
# to get pivot job's pod
$ kubectl describe pod -n kube-job -l job-name=pivot-job-default

# To check logs
$ kubectl logs -n kube-job -l job-name=pivot-job-default
```

- The logs given above will show you several stages at which it might wait for long followed by timeout and then again spawning of pivot-job's pod. The stages are defined below with an explanation of potential errors:

  - The below step annotates the "cluster" customization with "pivot/started=true"

  ```terminal
  -- Signal that the pivot job has started. This is used in bootstrap.sh to prevent accidental re-runs
  ```

  - It will fail at the below stage if the `management-cluster-kubeconfig` secret is not present. Check secrets to debug.

  ```terminal
  -- Retrieve target cluster kubeconfig
  ```

  - It will fail/timeout at the below stage if all of the management cluster nodes and cluster customization do not come up and running. Check cluster customization and machines to troubleshoot further.
  
  ```terminal
  -- Wait for cluster and machines to be ready as it is a required condition to move
  ```

  - It will fail/timeout at the below stage if any Cluster API-specific customization raises an issue on the management cluster. Check the customization list on the management cluster by using `management-cluster-kubeconfig` file. Also, If you have reached this step, you can use "apply.sh" to have a look at the management cluster even before the pivot is completed successfully.

  ```terminal
  -- Wait for all Kustomizations related to Cluster API to be ready in management cluster
  ```

  - It will fail at the below stage if there is an issue while patching the suspend flag as true.

  ```terminal
  -- Suspend Kustomizations and HelmReleases in bootstrap cluster that relate to the management cluster
  ```

  - This is the actual stage where the pivot is done using `clusterctl move` command. It will fail here if there is an issue while running `clusterctl move` command.

  ```terminal
  -- Move cluster definitions from source to target cluster
  ```

  - It will fail here if there is an issue while running a patch command on the management cluster to enable the cluster unit.

  ```terminal
  -- Patch the management cluster sylva-units HelmRelease to enable the 'cluster' unit in the management cluster
  ```

  - It will fail at the below stages if there is an issue while adding annotations in the bootstrap cluster and management cluster.

  ```terminal
  -- Freeze reconciliation of current job Kustomization in source cluster as we're done
  -- Accelerate reconciliation of the sylva-units HelmRelease
  -- Signal to bootstrap.sh that the pivot job has ended
  ```

  - Here, the pivot job is completed.

  ```terminal
  -- All done
  ```

- Run the below command to see the progress of the deployment. If you do not see any progress here, dig to troubleshoot further.

```terminal
$ sylvactl watch --reconcile
```

- If you see the below error trace while running `bootstrap.sh` then it means that you have passed the bootstrap phase and you should now switch to use "apply.sh" for further operations.

```terminal
$ ./bootstrap.sh environment-values/current-environment-values
🔎 Validate input files

❌ The pivot job has already ran and moved resources to the management cluster. Please use apply.sh instead of bootstrap.sh

```

```shell
# Switch to apply.sh
./apply.sh environment-values/current-environment-values 
```


## Appendix

### bootstrap.sh log after a successful installation

Context:
- Sylva 1.0.0
- Installation on OpenStack (CAPO)
- Management cluster on a single control plane machine

```
ubuntu@bootstrap-vm:~/my-deployment/sylva-core$ ./bootstrap.sh environment-values/current-environment-values
🔎 Validate input files

🔃 Preparing bootstrap cluster
Creating kind cluster with following config:
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
networking:
  podSubnet: "10.244.0.0/16"
  serviceSubnet: "10.96.0.0/16"
nodes:
  - role: control-plane
    extraMounts:
      - hostPath: /home/ubuntu/my-deployment/sylva-core/tools/kind/registry.d/
        containerPath: /etc/containerd/registry.d
      - hostPath: /var/run/docker.sock
        containerPath: /var/run/docker.sock
containerdConfigPatchesJSON6902:
  - '[{"op": "remove", "path": "/plugins/io.containerd.grpc.v1.cri/registry/mirrors"}]'
  - '[{"op": "add", "path": "/plugins/io.containerd.grpc.v1.cri/registry/config_path", "value": "/etc/containerd/registry.d"}]'
Creating cluster "sylva" ...
 ✓ Ensuring node image (kindest/node:v1.25.3) 🖼
 ✓ Preparing nodes 📦
 ✓ Writing configuration 📜
 ✓ Starting control-plane 🕹️
 ✓ Installing CNI 🔌
 ✓ Installing StorageClass 💾
Set kubectl context to "kind-sylva"
You can now use your cluster with:

kubectl cluster-info --context kind-sylva

Not sure what to do next? 😅  Check out https://kind.sigs.k8s.io/docs/user/quick-start/

🔃 Install flux
namespace/flux-system created
resourcequota/critical-pods-flux-system created
customresourcedefinition.apiextensions.k8s.io/buckets.source.toolkit.fluxcd.io created
customresourcedefinition.apiextensions.k8s.io/gitrepositories.source.toolkit.fluxcd.io created
customresourcedefinition.apiextensions.k8s.io/helmcharts.source.toolkit.fluxcd.io created
customresourcedefinition.apiextensions.k8s.io/helmreleases.helm.toolkit.fluxcd.io created
customresourcedefinition.apiextensions.k8s.io/helmrepositories.source.toolkit.fluxcd.io created
customresourcedefinition.apiextensions.k8s.io/kustomizations.kustomize.toolkit.fluxcd.io created
customresourcedefinition.apiextensions.k8s.io/ocirepositories.source.toolkit.fluxcd.io created
serviceaccount/helm-controller created
serviceaccount/kustomize-controller created
serviceaccount/source-controller created
clusterrole.rbac.authorization.k8s.io/crd-controller-flux-system created
clusterrole.rbac.authorization.k8s.io/flux-edit-flux-system created
clusterrole.rbac.authorization.k8s.io/flux-view-flux-system created
clusterrolebinding.rbac.authorization.k8s.io/cluster-reconciler-flux-system created
clusterrolebinding.rbac.authorization.k8s.io/crd-controller-flux-system created
service/source-controller created
deployment.apps/helm-controller created
deployment.apps/kustomize-controller created
deployment.apps/source-controller created
networkpolicy.networking.k8s.io/allow-egress created
networkpolicy.networking.k8s.io/allow-scraping created
networkpolicy.networking.k8s.io/allow-webhooks created

⏳ Wait for Flux to be ready...
deployment.apps/helm-controller condition met
deployment.apps/kustomize-controller condition met
deployment.apps/source-controller condition met

🔎 Validate sylva-units values for management cluster
namespace/sylva-units-preview created
configmap/sylva-units-values created
secret/sylva-units-secrets created
helmrelease.helm.toolkit.fluxcd.io/sylva-units created
Warning: v1beta2 Kustomization is deprecated, upgrade to v1
helmrepository.source.toolkit.fluxcd.io/sylva-core created
force reconciliation of helmrelease sylva-units
  helmrelease.helm.toolkit.fluxcd.io/sylva-units annotated
Wait for Helm release to be ready
 ✓ HelmRepository/sylva-units-preview/sylva-core - Resource is ready
 ✓ HelmChart/sylva-units-preview/sylva-units-preview-sylva-units - Resource is ready
 ⠈⠱ HelmRelease/sylva-units-preview/sylva-units - Progressing - Reconciliation in progress
 ✓ All Done: resource HelmRelease/sylva-units-preview/sylva-units is ready!

🗑 Delete preview chart and namespace
helmrelease.helm.toolkit.fluxcd.io/sylva-units
helmrepository.source.toolkit.fluxcd.io/sylva-core
ocirepository.source.toolkit.fluxcd.io/sylva-core
namespace "sylva-units-preview" deleted

📜 Install sylva-units Helm release and associated resources
Warning: resource namespaces/default is missing the kubectl.kubernetes.io/last-applied-configuration annotation which is required by kubectl apply. kubectl apply should only be used on resources created declarat
ively by either kubectl create --save-config or kubectl apply. The missing annotation will be patched automatically.
namespace/default configured
configmap/sylva-units-values created
secret/sylva-units-secrets created
helmrelease.helm.toolkit.fluxcd.io/sylva-units created
Warning: v1beta2 Kustomization is deprecated, upgrade to v1
helmrepository.source.toolkit.fluxcd.io/sylva-core created

🎯 Trigger reconciliation of units
force reconciliation of helmrelease sylva-units
  helmrelease.helm.toolkit.fluxcd.io/sylva-units annotated

⏳ Wait for bootstrap units and management cluster to be ready
 ✓ HelmRepository/default/sylva-core - Resource is ready
 ✓ HelmRepository/default/unit-cert-manager - Resource is ready
 ✓ HelmRepository/default/unit-cluster - Resource is ready
 ✓ HelmRepository/default/unit-management-sylva-units - Resource is ready
 ✓ OCIRepository/default/sylva-core - Resource is ready
 ✓ HelmChart/default/default-cert-manager - Resource is ready
 ✓ HelmChart/default/default-cluster - Resource is ready
 ✓ HelmChart/default/default-sylva-units - Resource is ready
 ✓ HelmRelease/default/cert-manager - Resource is ready
 ✓ HelmRelease/default/cluster - Resource is ready
 ✓ HelmRelease/default/sylva-units - Resource is ready
 ✓ Kustomization/default/cert-manager - Resource is ready
 ✓ Kustomization/default/heat-operator - Resource is ready
 ✓ Kustomization/default/cabpr - Resource is ready
 ✓ Kustomization/default/capi - Resource is ready
 ✓ Kustomization/default/capo - Resource is ready
 ✓ Kustomization/default/capo-cluster-resources - Resource is ready
 ✓ Kustomization/default/cluster - Resource is ready
 ✓ Kustomization/default/management-cluster-flux - Resource is ready
 ✓ Kustomization/default/management-cluster-configs - Resource is ready
⠈⠱ Kustomization/default/management-sylva-units - Progressing - Reconciliation in progress
 ✓ All Done: resource Kustomization/default/management-sylva-units is ready!

⏳ Wait for units installed on management cluster to be ready
 ✓ HelmRepository/default/sylva-core - Resource is ready
 ✓ HelmRepository/default/unit-capi-rancher-import - Resource is ready
 ✓ HelmRepository/default/unit-cert-manager - Resource is ready
 ✓ HelmRepository/default/unit-cinder-csi - Resource is ready
 ✓ HelmRepository/default/unit-cis-operator - Resource is ready
 ✓ HelmRepository/default/unit-cis-operator-crd - Resource is ready
 ✓ HelmRepository/default/unit-cluster - Resource is ready
 ✓ HelmRepository/default/unit-external-secrets-operator - Resource is ready
 ✓ HelmRepository/default/unit-flux-webui - Resource is ready
 ✓ HelmRepository/default/unit-ingress-nginx - Resource is ready
 ✓ HelmRepository/default/unit-k8s-gateway - Resource is ready
 ✓ HelmRepository/default/unit-kyverno - Resource is ready
 ✓ HelmRepository/default/unit-local-path-provisioner - Resource is ready
 ✓ HelmRepository/default/unit-monitoring - Resource is ready
 ✓ HelmRepository/default/unit-monitoring-crd - Resource is ready
 ✓ HelmRepository/default/unit-postgres - Resource is ready
 ✓ HelmRepository/default/unit-rancher - Resource is ready
 ✓ HelmRepository/default/unit-vault-config-operator - Resource is ready
 ✓ HelmRepository/default/unit-vault-operator - Resource is ready
 ✓ OCIRepository/capi-rancher-import/capi-rancher-import-cattle-agent-kustomize - Resource is ready
 ✓ OCIRepository/default/sylva-core - Resource is ready
 ✓ HelmChart/default/default-capi-rancher-import - Resource is ready
 ✓ HelmChart/default/default-cert-manager - Resource is ready
 ✓ HelmChart/default/default-cinder-csi - Resource is ready
 ✓ HelmChart/default/default-cis-operator - Resource is ready
 ✓ HelmChart/default/default-cis-operator-crd - Resource is ready
 ✓ HelmChart/default/default-cluster - Resource is ready
 ✓ HelmChart/default/default-external-secrets-operator - Resource is ready
 ✓ HelmChart/default/default-flux-webui - Resource is ready
 ✓ HelmChart/default/default-ingress-nginx - Resource is ready
 ✓ HelmChart/default/default-k8s-gateway - Resource is ready
 ✓ HelmChart/default/default-kyverno - Resource is ready
 ✓ HelmChart/default/default-local-path-provisioner - Resource is ready
 ✓ HelmChart/default/default-monitoring - Resource is ready
 ✓ HelmChart/default/default-monitoring-crd - Resource is ready
 ✓ HelmChart/default/default-postgres - Resource is ready
 ✓ HelmChart/default/default-rancher - Resource is ready
 ✓ HelmChart/default/default-sylva-units - Resource is ready
 ✓ HelmChart/default/default-vault-config-operator - Resource is ready
 ✓ HelmChart/default/default-vault-operator - Resource is ready
 ✓ HelmRelease/default/capi-rancher-import - Resource is ready
 ✓ HelmRelease/default/cert-manager - Resource is ready
 ✓ HelmRelease/default/cinder-csi - Resource is ready
 ✓ HelmRelease/default/cis-operator - Resource is ready
 ✓ HelmRelease/default/cis-operator-crd - Resource is ready
 ✓ HelmRelease/default/cluster - Resource is ready
 ✓ HelmRelease/default/external-secrets-operator - Resource is ready
 ✓ HelmRelease/default/flux-webui - Resource is ready
 ✓ HelmRelease/default/ingress-nginx - Resource is ready
 ✓ HelmRelease/default/k8s-gateway - Resource is ready
 ✓ HelmRelease/default/kyverno - Resource is ready
 ✓ HelmRelease/default/local-path-provisioner - Resource is ready
 ✓ HelmRelease/default/monitoring - Resource is ready
 ✓ HelmRelease/default/monitoring-crd - Resource is ready
 ✓ HelmRelease/default/postgres - Resource is ready
 ✓ HelmRelease/default/rancher - Resource is ready
 ✓ HelmRelease/default/sylva-units - Resource is ready
 ✓ HelmRelease/default/vault-config-operator - Resource is ready
 ✓ HelmRelease/default/vault-operator - Resource is ready
 ✓ Kustomization/default/cert-manager - Resource is ready
 ✓ Kustomization/default/cis-operator-crd - Resource is ready
 ✓ Kustomization/default/coredns - Resource is ready
 ✓ Kustomization/default/external-secrets-operator - Resource is ready
 ✓ Kustomization/default/flux-system - Resource is ready
 ✓ Kustomization/default/heat-operator - Resource is ready
 ✓ Kustomization/default/ingress-nginx - Resource is ready
 ✓ Kustomization/default/k8s-gateway - Resource is ready
 ✓ Kustomization/default/kyverno - Resource is ready
 ✓ Kustomization/default/kyverno-policies - Resource is ready
 ✓ Kustomization/default/local-path-provisioner - Resource is ready
 ✓ Kustomization/default/monitoring-crd - Resource is ready
 ✓ Kustomization/default/namespace-defs - Resource is ready
 ✓ Kustomization/default/shared-workload-clusters-settings - Resource is ready
 ✓ Kustomization/default/sylva-ca - Resource is ready
 ✓ Kustomization/default/sylva-ca-certs - Resource is ready
 ✓ Kustomization/default/vault-config-operator - Resource is ready
 ✓ Kustomization/default/vault-operator - Resource is ready
 ✓ Kustomization/default/cabpr - Resource is ready
 ✓ Kustomization/default/capi - Resource is ready
 ✓ Kustomization/default/capo - Resource is ready
 ✓ Kustomization/default/capo-cluster-resources - Resource is ready
 ✓ Kustomization/default/cinder-csi - Resource is ready
 ✓ Kustomization/default/cis-operator - Resource is ready
 ✓ Kustomization/default/cis-operator-scan - Resource is ready
 ✓ Kustomization/default/cluster - Resource is ready
 ✓ Kustomization/default/monitoring - Resource is ready
 ✓ Kustomization/default/pause-cluster-reconciliation - Resource is ready
 ✓ Kustomization/default/postgres - Resource is ready
 ✓ Kustomization/default/vault - Resource is ready: Vault UI can be reached at https://vault.sylva (vault.sylva must resolve to xx.xx.xx.xx)
 ✓ Kustomization/default/vault-secrets - Resource is ready
 ✓ Kustomization/default/capi-providers-pivot-ready - Resource is ready
 ✓ Kustomization/default/eso-secret-stores - Resource is ready
 ✓ Kustomization/default/synchronize-secrets - Resource is ready
 ✓ Kustomization/default/keycloak - Resource is ready: Keycloak admin console can be reached at https://keycloak.sylva/admin/master/console, user 'admin', password in Vault at secret/keycloak (keycloak.sylva mus
t resolve to xx.xx.xx.xx)
 ✓ Kustomization/default/keycloak-legacy-operator - Resource is ready
 ✓ Kustomization/default/keycloak-resources - Resource is ready
 ✓ Kustomization/default/rancher - Resource is ready: Rancher UI can be reached at https://rancher.sylva (rancher.sylva must resolve to xx.xx.xx.xx)
 ✓ Kustomization/default/rancher-monitoring-clusterid-inject - Resource is ready
 ✓ Kustomization/default/vault-oidc - Resource is ready
 ✓ Kustomization/default/capi-rancher-import - Resource is ready
 ✓ Kustomization/default/cluster-creator-login - Resource is ready
 ✓ Kustomization/default/cluster-creator-policy - Resource is ready
 ✓ Kustomization/default/first-login-rancher - Resource is ready
 ✓ Kustomization/default/keycloak-add-client-scope - Resource is ready
 ✓ Kustomization/default/keycloak-oidc-external-secrets - Resource is ready
 ✓ Kustomization/default/rancher-keycloak-oidc-provider - Resource is ready
 ✓ Kustomization/default/flux-webui - Resource is ready: Flux Web UI can be reached at https://flux.sylva (flux.sylva must resolve to xx.xx.xx.xx)
 ✓ Kustomization/default/sylva-units-status - Resource is ready
 ✓ All Done!

✔ Sylva is ready, everything deployed in management cluster
   Management cluster nodes:
NAME                                     STATUS   ROLES                       AGE   VERSION
management-cluster-cp-a1d5893084-c52kq   Ready    control-plane,etcd,master   15m   v1.26.9+rke2r1

🌱 You can access following UIs
NAMESPACE       NAME                      CLASS    HOSTS            ADDRESS          PORTS     AGE
cattle-system   rancher                   nginx    rancher.sylva    xx.xx.xx.xx   80, 443   7m29s
flux-system     flux-webui-weave-gitops   nginx    flux.sylva       xx.xx.xx.xx   80, 443   104s
keycloak        keycloak-ingress          <none>   keycloak.sylva   xx.xx.xx.xx   80, 443   7m29s
vault           vault                     <none>   vault.sylva      xx.xx.xx.xx   80, 443   9m7s

🎉 All done
```
