---
title: Technical Overview
---

:::note
## Terminology

Sylva is using three main components:

#### Management Cluster

Intended to be deployed centrally and capable of spawning and managing Sylva workload clusters (aka downstream clusters), with a declarative approach.

#### Workload Cluster

The cluster that will host the CNF (Containerized Network Functions)

#### Units

Units that are applications (or components), mandatory or optional, coming from opensource projects that compose the Sylva clusters. Note that "units" is used here not to bring confusion with the Kustomize "components" terminology also used in Sylva.
:::

## Design

From a high-level view, Sylva comprises one management cluster managing multiple workload clusters. Those workload clusters will host applications, called Containerized Network Functions (CNF) in the Telecom industry.

<div style={{textAlign: 'center'}}>

![](./img/hld-1.png)

</div>

## Clusters

Sylva is using [ClusterAPI](https://cluster-api.sigs.k8s.io/) to manage the lifecycle of each cluster, including the management cluster. Thanks to ClusterAPI providers, Sylva can deploy multiple types of infrastructure providers and bootstrap providers. 

<div style={{textAlign: 'center'}}>

![](./img/hld-2.png)

</div>

## Lifecycle management

* Sylva is leveraging GitOps principles by having all the clusters defined in description files. 
* [FluxCD](https://fluxcd.io/flux/) is used to reconcile running clusters with their description. * Any update to those descriptions will trigger the update of the corresponding cluster. 

<div style={{textAlign: 'center'}}>

![](./img/hld-3.png)

</div>

## Sylva units

The Sylva stack is a collection of software pieces and configuration elements called **units**.

Different units will be used depending on the role of the cluster (management or workload cluster) and depending on the targeted technical environment for the cluster. For instance, some units will be needed for baremetal clusters, while others will be needed for OpenStack clusters.

Based on the values used to describe the Sylva clusters, a sylva-units Helm chart will create corresponding FluxCD resources, and dependencies between those resources.

FluxCD will then orchestrate the deployment of the corresponding units.

More details about the sylva-units can be found [here](https://gitlab.com/sylva-projects/sylva-core/-/blob/main/charts/sylva-units/README.md?ref_type=heads)
