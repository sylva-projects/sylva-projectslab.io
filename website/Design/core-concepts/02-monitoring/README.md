---
title: Monitoring Overview
---

This document provides a comprehensive overview of the monitoring strategies and tools implemented in our system.

## Monitoring Design

The main components of the Sylva monitoring stack is provided by the `rancher-monitoring` stack which is deployed in each cluster (management and workload).

From Rancher UI, you can [access different UIs](/docs/Operations/user-facing-services/access-uis) in each workload cluster:
- [Prometheus](https://prometheus.io/) to collect metrics (powered by the [Prometheus Operator](https://github.com/prometheus-operator/prometheus-operator) and the [Prometheus adapter](https://github.com/DirectXMan12/k8s-prometheus-adapter))
- [Alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/) to define and push alerts from metrics collected by Prometheus
- [Grafana](https://grafana.com/grafana/) to provide comprehensive dashboards

For more information about available features and how to interact with those components, please refer to the [Monitoring and Alerting](https://ranchermanager.docs.rancher.com/integrations-in-rancher/monitoring-and-alerting) section from the official Rancher documentation.

Additionally, [Thanos](https://thanos.io/) is installed in the management cluster and Prometheus in each cluster pushes metrics to it via `remote_write`. The metrics collected by Thanos can be displayed from Grafana (management cluster). 

Also, Thanos **needs** an object storage backend which could be provided through the [Minio](https://github.com/minio/minio) unit or through any external S3 backend.

:::note
MinIO is a high-performance, S3 compatible object store, open sourced under GNU AGPLv3 license.
:::

Prometheus in each cluster pulls its metrics from several exporters:

- [Node Exporter](https://github.com/prometheus/node_exporter): scrapes metrics from node OS (e.g. CPU, memory)
- [kube-state-metrics](https://github.com/kubernetes/kube-state-metrics): scrapes metrics from Kubernetes objects (e.g. pods, deployments)
- [Service monitor](https://github.com/prometheus-operator/prometheus-operator/blob/main/Documentation/user-guides/getting-started.md): used through the Prometheus Operator to collect additional Sylva Units metrics (e.g. Longhorn, Kepler)
- [SNMP exporter](https://github.com/prometheus/snmp_exporter): used from the management cluster to collect hardware metrics from node's BMC interfaces of Bare-Metal clusters.

![monitoring-design](img/monitoring-design.png)



## General information

`monitoring.platform_tag` should be changed in order reflect the platform name (e.g. "DEV_Vendor_Test")

```yaml
monitoring:
  platform_tag: Sylva
```

This value is then configured as an `external_label` and propagated via Alertmanager so this platform's  alerts can be differentiated from ones coming from another platform (e.g. "Prod_Vendor").

Each section links to detailed documentation that elaborates on the configurations, usage, and benefits of the respective monitoring tools. This structured approach ensures that system administrators have the necessary resources to effectively monitor and maintain the system's health and performance.

## Prometheus

Prometheus is configured to handle alerting rules that monitor system metrics and notify administrators of potential issues. Custom rules can be added to tailor alerting to specific needs, enhancing the system's responsiveness to operational anomalies.

## Grafana

Grafana is used for visualizing real-time data and metrics through dashboards. These dashboards are customizable and can be tailored to display critical information, aiding in quick decision-making and thorough system analysis.

## Alertmanager

Alertmanager manages alerts sent from Prometheus, including deduplication, grouping, and routing. It supports webhooks and can integrate with JIRA through JIRAlert to create configurable JIRA issues based on alerts, streamlining incident management processes.

## Thanos

Thanos acts as an aggregator of metrics from all clusters and it's deployed in the management cluster

## SNMP

SNMP (Simple Network Management Protocol) is utilized to monitor network devices and servers. It provides essential operational metrics and supports dynamic configuration to include new devices and hardware types, ensuring comprehensive coverage of all monitored components.
