---
title: Stack Overview
sidebar_position: 0
---

## Table of Contents

- [Security](#security)
- [Monitoring](#monitoring)
- [Logging](#logging)

## Security

Security is a key focus area in cluster management. It ensures the protection of data, resources, and the cluster itself against unauthorized access and threats. This section outlines best practices, tools, and configurations to secure your Sylva-managed clusters.

[Learn more about Security](/design/core-concepts/security)

## Monitoring

Monitoring is essential for maintaining cluster health and performance. Here, you’ll learn about the tools and techniques available to monitor your Sylva-managed clusters effectively. Proper monitoring ensures early issue detection and quick resolution.

[Learn more about Monitoring](/design/core-concepts/monitoring)

## Logging

Logging plays a critical role in providing visibility into cluster activities and system behavior. Sylva leverages tools like Fluent Bit, Fluentd, and Loki for log collection, processing, and storage. While logging supports monitoring, it focuses more on capturing application-specific and system-wide events. This separation helps in analyzing granular data (e.g., application logs) while also enabling system-level aggregation for broader insights.

[Learn more about Logging](/design/core-concepts/logging)
