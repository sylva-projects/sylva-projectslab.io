# Security Overview

This document provides a comprehensive overview of the security measures and tools implemented in our system. Each section focuses on a specific aspect of security, ensuring that all components are secure and compliant with industry standards.

## Certificate Management

In the realm of cloud-native applications and Kubernetes clusters, managing the security of communications between services is paramount. X509 certificates play a crucial role in securing these communications by enabling TLS/SSL, which provides confidentiality, integrity, and authentication. Effective management of these certificates, including their creation, distribution, renewal, and revocation, is essential to maintaining the security posture of any system. This document outlines the strategies employed by the Sylva clusters to manage X509 certificates, leveraging both internal and external Public Key Infrastructures (PKIs) to ensure robust security across all services.

**Internal PKI**: Sylva management cluster includes its own Root authority, managed by cert-manager. This setup allows for the issuance of X509 certificates for services within the management cluster, enhancing secure communication.

**External PKI**: For higher trust levels, external PKI can be used to import certificates signed by trusted authorities. This method simplifies the process as it avoids the need for importing CA certificates into browsers, assuming the CA is already trusted by the system.

[Certificate Management: Operationnal guide](/docs/Operations/security-operations/certificates)

## Identity and Access Management

Managed by Keycloak, this system provides comprehensive user management, single-sign-on capabilities, and identity brokering. It ensures that only authenticated and authorized users can access system resources, significantly enhancing security.

[IAM: Operationnal guide](/docs/Operations/security-operations/iam)

## Neuvector

Neuvector provides comprehensive security solutions tailored for Kubernetes environments, including vulnerability management, compliance checks, and runtime security. This tool is integral for maintaining the security posture of containerized applications.

[Neuvector: Operationnal guide](/docs/Operations/security-operations/neuvector)

## SBOM

A Software Bill of Materials (SBOM) is crucial for tracking components used in the software, including their dependencies. This helps in managing vulnerabilities and ensuring compliance with security standards, making the system's software supply chain transparent and secure.

[SBOM: Operationnal Guide](/docs/Operations/security-operations/sbom)

## Security Scans

High Security Grade clusters should utilize RKE2, which is hardened by default and meets the majority of Kubernetes CIS controls without modifications. RKE2 is designed with a focus on security and compliance, particularly within the U.S. Federal Government sector, by:

- Providing default settings and configuration options that enable clusters to meet the CIS Kubernetes Benchmark v1.6 with minimal intervention from operators.
- Ensuring compliance with FIPS 140-2.
- Conducting regular scans for CVEs within the Rancher build pipeline.

For a detailed list of security controls, please refer to [RKE2's CIS self-assessment guide](https://docs.rke2.io/security/cis_self_assessment16/).

When a high security grade is necessary, Sylva can deploy RKE2 clusters with a `cis_profile` flag set to either `cis-1.6` or `cis-1.23`, depending on the Kubernetes release. These profiles validate the system configuration against their respective CIS benchmark by ensuring that host-level requirements are met and by configuring runtime pod security policies and network policies. For more details, please see the [RKE2 security hardening guide](https://docs.rke2.io/security/hardening_guide/).

[Security Scans: Operationnal Guide](/docs/Operations/security-operations/scans)

## Vault

Vault plays a critical role in managing and securing access to tokens, passwords, certificates, and encryption keys. It ensures that sensitive data is stored securely and access is tightly controlled, providing a robust method to handle secrets within distributed systems.

[Vault: Operationnal Guide](/docs/Operations/security-operations/vault)
