# CNF Security Guidelines

## Scope of the Document

This document is intended for any partner wishing to develop Cloud-native Network Functions (CNFs) that can be deployed on the Sylva stack in a secure, efficient, and responsive manner. Developers are responsible for the application security of the CNF. Architectural components should be designed to minimize the number of microservices.

CNF refers to Cloud-native Network Functions essential for the Telco industry, but it also encompasses various IT workloads.

## CNF Guidelines

Sylva security is designed and developed using the four principles of cybersecurity: confidentiality, integrity, authenticity, and availability. A state-of-the-art application software design adheres to these principles. The following guidelines should be followed:

1. **Supply Chain Security**: 
   - Follow the [SLSA guidelines](https://slsa.dev/):
     - Secure software development using DevSecOps principles (source code integrity, SAST, DAST, etc.).
     - Implement a vulnerability management process for CNF dependencies: create SBOM, monitor vulnerabilities, and analyze them against CNF code.
     - Establish a patch management process: patch images when vulnerabilities are identified.
     - Adopt a zero-trust approach: test CNFs for vulnerabilities, even if upstream artifacts are claimed safe.
     - Ensure integrity and authenticity by signing CNF artifacts.

2. **Runtime Security Monitoring**: 
   - CNFs should not obstruct the CaaS infrastructure from monitoring runtime security events.

3. **Network Filtering**: 
   - Implement network filtering to expose only necessary interfaces and mitigate pivoting risks.

4. **Least Privilege Principle**: 
   - CNF applications must adhere to the principle of least privilege for both users and service accounts, enforcing strict access control. Exceptions may be granted with justification and risk exposure.

5. **Pod Admission Control**: 
   - CNFs must operate in an environment that enables pod admission control (K8s native Pod Security Admission).

6. **Privilege Isolation**: 
   - If high privileges are necessary, CNFs must be isolated on dedicated Kubernetes nodes (hardware or VM). Exceptions may be granted with justification.

7. **Use of Sylva Units**: 
   - If a function can be fulfilled by Sylva units, the application should not require escalated privileges. Exceptions may be granted with justification.

8. **Host Directory Access**: 
   - Avoid mounting host directories for device access unless absolutely necessary. Justification and risk assessment should be provided.

9. **Host Network Namespace**: 
   - CNFs should not use the host network namespace unless required. Justification and risk assessment should be provided.

10. **High Availability Compliance**: 
    - CNFs must comply with the High Availability deployment model.

11. **Elasticity Support**: 
    - CNFs should support elasticity with dynamic scaling up and down.

12. **Service Availability During Upgrades**: 
    - CNFs should maintain service availability during platform upgrades, node rolling upgrades, and node draining.

13. **Compatibility with Sylva Versions**: 
    - CNFs should not be tied to unsupported Sylva versions of Kubernetes or any of its components.