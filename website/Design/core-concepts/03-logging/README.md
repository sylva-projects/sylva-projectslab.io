# Logging Overview

This document provides a comprehensive overview of the logging strategies and tools implemented in our system.

Logging is provided using the `rancher-logging` stack, deployed in each cluster (management and workload) and **Loki** to centralize logs in the management cluster and to provide a logging dashboard through Grafana.

![Logging-design](img/Logging-design.png)

The logging stack relies on 3 main components:

## Fluentd

Fluentd is configured to handle log aggregation, parsing, and forwarding to various destinations. Custom configurations can be added to tailor the logging to specific needs, enhancing the system's responsiveness to operational insights.

[Fluentd: Operationnal guide](/docs/Operations/logging-monitoring-operations/fluentd)

## Fluent Bit

Fluent Bit is used for lightweight log processing, acting as a forwarder to Fluentd or directly to other logging backends like Loki. Its lightweight nature makes it suitable for edge environments where resource usage is a concern.

[Fluent Bit: Operationnal guide](/docs/Operations/logging-monitoring-operations/fluentbit)

## Loki

Loki is used for log aggregation and storage, allowing for efficient querying and analysis of log data. It integrates seamlessly with Grafana for visualizing logs and metrics in a unified dashboard.

[Loki: Operationnal guide](/docs/Operations/logging-monitoring-operations/loki)

[Loki: Developper perspective](/dev-zone/developer-perspectives/logging/loki)

---

Also, additional components involved in the logging design:

- `sylva-logging-flow` unit to configure filters to export logs to an external syslog server
- `logging-operator` to manage logging custom resources

---

:::note
For more information about available features and how to interact with these components, please refer to the [Logging](https://ranchermanager.docs.rancher.com/integrations-in-rancher/logging) section from the official Rancher documentation.
:::
