import {useEffect, useState} from 'react';

export default function App({tag}) {

  // Function to get last release name from Sylva Core (project ID: 42451983)
  // https://gitlab.com/api/v4/projects/42451983/releases
    
  const [data, setData] = useState("");
  const getData = async () => {
    try {
      const resp = await fetch(`https://gitlab.com/api/v4/projects/42451983/releases/${tag}`);
      const json = await resp.json();
      const url = json._links.self
      setData(url);
    } catch (err) {
      setData(err.message);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <a href={data}>Gitlab link and merge requests list</a>
  )
}
