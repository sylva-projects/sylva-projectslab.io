const branchName = process.env.UPSTREAM_BRANCH;
const isPreview = process.env.CI_PROJECT_NAME === 'doc-preview';
import {themes as prismThemes} from 'prismjs';

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Sylva',
  tagline: 'Open Source Production-Grade Telco Cloud Stack',
  url: 'https://sylva-projects.gitlab.io',
  baseUrl: (isPreview) ? '/doc-preview/' : '/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'GitLab', // Usually your GitHub org/user name.
  projectName: 'sylva', // Usually your repo name.
  //enable mermaid diagram
  markdown: {
    mermaid: true,
  },
  themes: ['@docusaurus/theme-mermaid', '@docusaurus/theme-live-codeblock'],

  // Here you can add highlighting preferences when using code blocks
  themeConfig: {
    prism: {
      defaultLanguage: 'bash',
      additionalLanguages: ['bash', 'shell-session'] // Restart when you add new : https://prismjs.com/#supported-languages
    },
    // used for preview
    ... ((!branchName) ? {} : 
    {
      announcementBar:
      {
        id: 'preview',
        content:
          `🔨 This is a preview version of a temporary doc branch <b>${branchName}</b>. For the official release, please visit the URL <a href="https://sylva-projects.gitlab.io">https://sylva-projects.gitlab.io</a>! 🔨`,
      },
    }),
    navbar: {
      logo: {
        alt: 'Sylva Logo',
        src: 'img/Sylva_Logo_color.png',
        srcDark: 'img/Sylva_Logo_white.png',
      },
      items: [
        {
          type: 'docSidebar',
          position: 'left',
          sidebarId: 'DesignSidebar',
          label: 'Architecture & Design',
          docsPluginId: 'Design',
        },
        {
          type: 'docSidebar',
          position: 'left',
          sidebarId: 'ReleasesSidebar',
          label: 'Releases',
          docsPluginId: 'Releases',
        },
       {
          type: 'docsVersion',
          position: 'left',
          label: 'Docs',
          docsPluginId: 'default',
          href: '/docs/category/installation',
        },
        {
          type: 'docSidebar',
          position: 'left',
          sidebarId: 'DevZoneSidebar',
          label: 'Developer Zone',
          docsPluginId: 'DevZone',
        },
        {to: 'blog', label: 'Articles', position: 'left'},
        // right-side navbar

        {
          type: 'docsVersionDropdown',
          position: 'right',
          docsPluginId: 'default',
          href: '/docs/category/installation',
        },
        {
          href: 'https://sylvaproject.org/',
          position: 'right',
          label: 'Official Website',
        },
        {
          href: 'https://gitlab.com/sylva-projects/sylva-projects.gitlab.io',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: '/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Slack',
              href: 'https://sylva-projects.slack.com',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/sylva-projects/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Sylva Project.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          id: 'default',
          path: 'docs',
          routeBasePath: '/docs', // Serve the docs at the site's root
          sidebarPath: require.resolve('./sidebars.js'),
          showLastUpdateAuthor: false,
          showLastUpdateTime: true,
          disableVersioning: false,
          includeCurrentVersion: true, //!isMasterBranch,
//          lastVersion: '1.1.1',
          versions: {
            current: {
              label: 'Sylva 1.3.0',
//              banner: 'unreleased',
            },
/*            "1.1.1": {
              label: 'Sylva 1.1.1',
              path: '1.1.1',
            }, */
          },
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/sylva-projects/sylva-projects.gitlab.io/-/edit/main/website/',
        },
        blog: {
          blogTitle: 'Sylva Project Articles',
          blogDescription: 'Sylva Project Articles',
          postsPerPage: 'ALL',
          blogSidebarTitle: 'All posts',
          blogSidebarCount: 'ALL',
          authorsMapPath: 'authors.yaml',
          editUrl:
          'https://gitlab.com/sylva-projects/sylva-projects.gitlab.io/-/edit/main/website/',
        },
        theme: {
          customCss: [
            require.resolve('./src/css/custom.css'),
          ]
        },
      },
    ],
  ],

  // Multi-doc instances configuration
  // https://docusaurus.io/docs/docs-multi-instance
  plugins: [
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      {
        // ... Your options.
        // `hashed` is recommended as long-term-cache of index file is possible.
        hashed: true,
        indexPages: true,
        docsRouteBasePath: ["/docs", "/design", "dev-zone"],
        docsDir: ["docs", "Design", "DevZone", ],
        language: ["en"],
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'Design',
        path: 'Design',
        routeBasePath: 'design',
        sidebarPath: require.resolve('./Design/sidebars.json'),
        showLastUpdateAuthor: false,
        showLastUpdateTime: true,
        editUrl:
        'https://gitlab.com/sylva-projects/sylva-projects.gitlab.io/-/edit/main/website/',
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'Releases',
        path: 'Releases',
        routeBasePath: 'releases',
        sidebarPath: require.resolve('./Releases/sidebars.json'),
        showLastUpdateAuthor: false,
        showLastUpdateTime: true,
        editUrl:
        'https://gitlab.com/sylva-projects/sylva-projects.gitlab.io/-/edit/main/website/',
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'DevZone',
        path: 'DevZone',
        routeBasePath: 'dev-zone',
        sidebarPath: require.resolve('./DevZone/sidebars.json'),
        // ... other options
        showLastUpdateAuthor: false,
        showLastUpdateTime: true,
        editUrl:
        'https://gitlab.com/sylva-projects/sylva-projects.gitlab.io/-/edit/main/website/',
        
      },
    ],
  ],
};
