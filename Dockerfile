FROM node:lts-alpine3.17

EXPOSE 3000

WORKDIR /app/website
COPY ./website/package.json /app/website/package.json
# website content should not be copied to the image
# it should be mounted to enable dynamic changes
# COPY ./website /app/website
# If needed, setup proxy using .yarnrc file
COPY .yarnrc /app/website/.yarnrc
RUN yarn install

CMD ["yarn", "start", "--host", "0.0.0.0"]
