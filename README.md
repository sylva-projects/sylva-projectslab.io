## Sylva documentation project

This project collects markdown file documentations related to the Sylva project, 
renders them using [docusaurus](https://docusaurus.io/) framework 
and hosts them using [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) 

The `main` branch of this project is built by the pipeline, 
and accessible on the URL http://sylva-projects.gitlab.io

Please note that this repo hosts only "non-versioned" content for Sylva (Design, News, etc).
The docs related to a specific Sylva release is located inside the `sylva-core` repo, 
in the `docs` folder under the corresponding branch.

## Project folder structure

Different folders of the project are organized as follows:

```
.
├── docker-compose.yml              # config for docker compose 
├── Dockerfile                      # used to build local image run by compose
├── .yarnrc-example                 # example file to create your own .yarnrc for proxy
├── README.md
└── website                         # website configuration and static content
     ├── docs                       # empty folder, real `docs` folder is in `sylva-core`
     ├── blog                       # folder to host the "News" section
     ├── DevZone                    # folder to host the "Dev Zone" section
     ├── GetStarted                 # folder to host the "Get Started" section
     ├── docusaurus.config.js       # main configuration file for website
     └── ...
```

Please note that the content inside `website` are not meant to be changed frequently.
They are copied inside the docker image, so any change inside this folder requires to rebuild the image.

## Versioning

Multiple versions of Sylva documentations are available, e.g. installation and operation guides are sometimes different across versions. 
They can be found using the dropdown list on the right side of the navigation bar.

The markdown files for different versions are organized according to [Docusaurus versioning guide](https://docusaurus.io/docs/versioning):

```
.
├── README.md
└── website
     ├── docusaurus.config.js       # main configuration file for website
     ├── blog
     ├── DevZone                    # Dev docs
     ├── GetStarted                 # General docs (same for all versions)
     ├── docs                       <-- Docs for the "latest" version
     ├── versioned_docs
     │      └── version-v1.1.1       <-- Docs for the "v1.1.1" version
     │           └── release-note.md
     │      └── version-vx.x.x       <-- other versions
     └── ...
```

## How to contribute to the documentation

1. Write the documentation using markdown syntax

Doc files are in `docs` folder.  
They should be written using markdown syntax. 
Some additional markdown features are available in Docusaurus.  
They are explained here https://docusaurus.io/docs/markdown-features

2. Build and start the local container

to build and test the documentation website locally

    docker compose up

This will build a docker image called `sylva-docusaurus`, install the dependencies and start a container named `sylva-local-preview` to render the local docs.

:warning: the "sylva-docusaurus" docker image has a large size (>2GB), because all the packages installed.

Some proxy configuration for `yarn` may be needed, to download/install packages from Internet repos.  

If you are behind a web proxy, the proxy address and port should be set in `.yarnrc` file. (an example file is provided as `.yarnrc-example`)

3. Preview and edit the docs

The local server is accessible at http://localhost:3000.
It provides the **hot** preview for all markdown files in the `website` folder, i.e.
The changes in the markdown file are rendered immediately by the server without restart.

Note: To add new document categories in addition to `docs`, `GetStarted`, etc. 
You need also to modify `docusaurus.config.js`

4. Clean up

If you want stop the container, you can run

    docker compose rm

or, since the container has a static name

    docker rm sylva-local-preview

If you have modified anything in `website/package.json`, and you want to rebuild the image

    docker compose up --build

If you want to remove everything and start over

    docker rm sylva-local-preview
    docker rmi sylva-docusaurus
